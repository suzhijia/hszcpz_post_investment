import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import './static/style/index.sass'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { ConfigProvider } from 'antd'
import locale from 'antd/es/locale/zh_CN'
import { createStore} from 'redux'
import { Provider } from 'react-redux'
let store = createStore((state ={}, type) => state)

ReactDOM.render(<Provider store={store}><ConfigProvider locale={locale}><App /></ConfigProvider></Provider>, document.getElementById('root'))

serviceWorker.register()
