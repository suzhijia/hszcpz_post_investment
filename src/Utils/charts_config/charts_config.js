export const chartsColors = [
    '#2c81ca',
    '#f56e64',
    '#ffcb2e',
    '#bf4cd4',
    '#6ec7e7',
    '#f3a39e',
    '#fae29a',
    '#d5a3e0',]
export const areaColors = [
    'rgba(44, 129, 202, 0.3)',
    'rgba(245, 110, 100, 0.3)',
    'rgba(255, 203, 46, 0.3)',
    'rgba(191, 76, 212, 0.3)',
    'rgba(110, 199, 231, 0.3)',
    'rgba(243, 163, 158, 0.3)',
    'rgba(250, 226, 154, 0.3)',
    'rgba(213, 163, 224, 0.3)']

export let commonOptions = {
    tooltip: {
        trigger: 'axis',
        formatter:  (value) => {
            if (value.length > 1) {
                return `${value[1].name}<br />
                持仓信息：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%<br />
                ${value[1].seriesName}：${value[1].marker}${parseFloat(value[1].value).toFixed(2)}
                `
            } else {
                return `${value[0].name}<br />
                持仓信息：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%`
            }

        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 88,
        y: 100
    },

    legend: {
        x: 'center',
        icon: 'line',
        bottom: -2,
        data: []
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        },
    }],
    yAxis: [
        {
            name: '仓位(%)',
            type: 'value',
            nameLocation: 'end',
            splitNumber: 5,
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            color: '#ccc',
            axisLabel: {
                show: true,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: '{value} '
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                lineStyle: {
                    type: 'dotted',
                    color: '#ccc'
                }
            }
        },
        {
            name: '复权累计净值',
            nameLocation: 'end',
            max: 3,
            interval: 3 / 5,
            type: 'value',
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            color: '#ccc',
            axisLabel: {
                show: true,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: value => {
                    return parseFloat(value).toFixed(2)
                }
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                lineStyle: {
                    type: 'dotted',
                    color: '#ccc'
                }
            }
        }
    ],
    dataZoom: [
        {
            type: 'slider',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 24
        }
    ],
    series: [
        {
            type: 'bar',
            data:  [],
            barMaxWidth: '30px',
            itemStyle: {
                normal: {
                    color: chartsColors[0],
                    barBorderRadius: [1, 1, 1, 1],
                }
            }
        },
        {
            name:'',
            type: 'line',
            data: [],
            symbol: false,
            symbolSize: 0,
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    shadowColor: 'rgba(0, 0, 0, 0.2)',
                    shadowOffsetY: 2,
                    shadowOffsetX: 2,
                    shadowBlur: 5
                }
            },
            itemStyle: {
                normal: {
                    color: chartsColors[1],
                }
            },
        }
    ]
}

export let chartsOptions = {
    title: {},
    tooltips:{
        show: true,
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },

    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68,
        containLabel: true
    },

    legend: [],

    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#019afa',
                width: 2
            }
        },
        splitLine: {
            show: true,
            lineStyle: {
                type: 'dotted',
                color: '#0d9ef9'
            }
        },
        axisTick: {
            show: false
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        }
    }],

    yAxis: [
        {
            type: 'value',
            nameLocation: 'end',
            // min: -100,
            // max: 100,
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                show: false,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: '{value} '
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    type: 'dotted',
                    color: '#0d9ef9'
                }
            }
        }
    ],

    dataZoom: [],

    series: []
}

export let linesOptions =  {
    title: {
        top: '0',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        formatter: (value) => {
            let itemDesc = `${value[0].name}<br />`
            value.forEach(item => {
                itemDesc += `${item.marker}${item.seriesName}：${parseFloat(item.value).toFixed(2)}%<br />`
            })

            return itemDesc
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    legend: {
        x: 'center',
        icon: 'line',
        bottom: 0,
        data: []
    },
    dataZoom: [{
            type: 'slider',
            show: true,
            height: 30,
            left: '70px',
            right: '70px',
            bottom: '5%',
            start: 0,
            end: 100,
            textStyle: {
                color: '#999',
                fontSize: 11,
            },
        }

    ],
    grid: {
        right: '46px',
        bottom: '15%',
        left: '46px',
        top: '80px',
        containLabel: false
    },
    xAxis: [{
        type: 'category',
        data: [],
        nameTextStyle: {
            color: '#999'
        },
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            show: true,
            position: 'bottom',
            inside: false
        },
        axisLabel: {
            show: true,
            textStyle: {
                color: "#666",
                fontSize: 12,
            }
        },
    }],
    yAxis: [{
        type: 'value',
        name: '累计收益率（%）',
        nameTextStyle: {
            color: '#999'
        },
        position: 'left',
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
            }
        },
        splitLine: {
            lineStyle: {
                type: 'dotted',
                color: "#ccc",
            }

        },
        axisLabel: {
            color: '#666',
            fontSize: 12,
        }
    }, ],
    series: []
}

export let areasOptions = {
    title: {
        top: '0',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        formatter: (value) => {
            return `${value[0].name}<br />${value[0].seriesName}：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%`
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    legend: {
        x: 'center',
        icon: 'line',
        bottom: 0,
        data: []
    },
    dataZoom: [{
            type: 'slider',
            show: true,
            height: 30,
            left: '70px',
            right: '70px',
            bottom: '5%',
            start: 0,
            end: 100,
            textStyle: {
                color: '#999',
                fontSize: 11,
            },
        }
    ],
    grid: {
        right: '46px',
        bottom: '15%',
        left: '46px',
        top: '80px',
        containLabel: false
    },
    xAxis: [{
        type: 'category',
        data: [],
        nameTextStyle: {
            color: '#999'
        },
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            show: true,
        },
        axisLabel: {
            show: true,
            textStyle: {
                color: "#666",
                fontSize: 12,
            }
        },
    }],
    yAxis: [{
        type: 'value',
        name: '动态回撤（%）',
        nameTextStyle: {
            color: '#999'
        },
        position: 'left',
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                type: 'dotted'
            }
        },
        splitLine: {
            lineStyle: {
                type: 'dotted',
                color: "#ccc",
            }

        },
        axisLabel: {
            color: '#666',
            fontSize: 12,
        }
    }, ],
    series: []
}

export let barsOptions = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        },
    }],
    yAxis: [{
        name: '频数(%)',
        nameLocation: 'end',
        nameTextStyle: {
            color: '#999',
            align: 'left'
        },
        color: '#ccc',
        axisLabel: {
            show: true,
            typeStyle: {
                color: '#333'
            },
            color: '#999'
        },
        axisLine: {
            show: false,
            lineStyle: {
                color: '#666'
            }
        },
        splitLine: {
            lineStyle: {
                type: 'dotted',
                color: '#ccc'
            }
        }
    }],
    dataZoom: [
        {
            type: '',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 0,
            show: false
        }
    ],
    series: []
}

export let correlationsOption = {
    tooltip: {
        trigger: 'axis',
        formatter: (a) => {
            return `${a[0].name}<br />${a[0].marker}${parseFloat(a[0].value).toFixed(2)}%`
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            onZero: false,
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            alignWithLabel: false,
            interval: 0
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            lineHeight: 20,
            textStyle: {
                fontSize: 14
            },
            formatter: (a) => {
                return  `{a|${a.replace('-', '\n')}}`
            },
            rich: {
                a: {
                    fontSize: 13,
                    lineHeight: 20
                }
            }
        },
    }],
    yAxis: [{
        name: '相关性（%）',
        nameLocation: 'end',
        nameTextStyle: {
            color: '#999',
            align: 'left'
        },
        color: '#ccc',
        axisLabel: {
            show: true,
            typeStyle: {
                color: '#333'
            },
            color: '#999'
        },
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        splitLine: {
            number: 10,
            lineStyle: {
                type: 'dotted',
                color: '#ccc'
            }
        }
    }],
    dataZoom: [
        {
            type: '',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 0,
            show: false
        }
    ],
    series: []
}

export let radarOption = {
    title: {
        text: ''
    },
    tooltip: {},
    legend: {
        data: []
    },
    radar: {
        indicator: [],
        center: ['50%', '50%'],
        name: {
            textStyle: {
                color: '#666'
            }
        }
    },
    series: []
}
