export const neutralOptions = [
    {
        value: 'n_basis_cost',
        label: '年化基差成本'
    },
    {
        value: '股指涨跌幅',
        label: '股指涨跌幅',
        children: [
            {
                value: 'n_sse50_change',
                label: '上证50涨跌幅'
            },
            {
                value: 'n_hs300_change',
                label: '沪深300涨跌幅'
            },
            {
                value: 'n_csi500_change',
                label: '中证500涨跌幅'
            },
            {
                value: 'n_csi800_change',
                label: '中证800涨跌幅'
            },
            {
                value: 'n_sse_change',
                label: '上证指数涨跌幅'
            },
            {
                value: 'n_gem_change',
                label: '创业板指数涨跌幅'
            }
        ]
    },
    {
        value: 'n_ls_diff',
        label: '大小市值涨跌幅之差'
    },
    {
        value: 'n_sse_volatility',
        label: '上证指数波动率'
    },
    {
        value: 'n_sse_liquidity',
        label: '上证指数流动性'
    },
]

export const options = [
    {
        value: '到期收益率',
        label: '到期收益率',
        children: [
            {
                value: 'n_long_yield',
                label: '长债收益率'
            },
            {
                value: 'n_base_rate',
                label: '货币市场基准利率'
            },
            {
                value: 'n_base_rate_volatility',
                label: '货币市场基准利率波动'
            },
            {
                value: 'n_term_spread',
                label: '期限利差'
            },
            {
                value: 'n_long_credit_spread',
                label: '长期信用利差'
            }
        ]
    },
    {
        value: '宏观经济',
        label: '宏观经济',
        children: [
            {
                value: 'n_IVA_yoy',
                label: '工业增加值当月同比'
            },
            {
                value: 'n_PMI',
                label: 'PMI'
            }
        ]
    },
    {
        value: '通货膨胀',
        label: '通货膨胀',
        children: [
            {
                value: 'n_CPI_yoy',
                label: 'CPI当月同比'
            },
            {
                value: 'n_PMI_yoy',
                label: 'PPI当月同比'
            }
        ]
    },
    {
        value: '货币供应',
        label: '货币供应',
        children: [
            {
                value: 'n_M2_yoy',
                label: 'M2同比'
            },
            {
                value: 'n_M1_M2_diff',
                label: 'M1-M2增速差'
            }
        ]
    },
    {
        value: '实体融资需求',
        label: '实体融资需求',
        children: [
            {
                value: 'n_social_finance_yoy',
                label: '社融存量同比'
            }
        ]
    }
]

export const futureOptions = [
    {
        value: '商品期货',
        label: '商品期货',
        children: [
            {
                value: 'n_nhci_volatility',
                label: '南华商品指数'
            },
            {
                value: 'n_nhii_volatility',
                label: '南华工业品指数'
            },
            {
                value: 'n_nhai_volatility',
                label: '南华农产品指数'
            },
            {
                value: 'n_nhmi_volatility',
                label: '南华金属指数'
            },
            {
                value: 'n_nheci_volatility',
                label: '南华能化指数'
            },
            {
                value: 'n_nhnmi_volatility',
                label: '南华有色金属指数'
            },
            {
                value: 'n_nhbmi_volatility',
                label: '南华黑色指数'
            },
            {
                value: 'n_nhpmi_volatility',
                label: '南华贵金属指数'
            }
        ]
    },
    {
        value: '股指期货',
        label: '股指期货',
        children: [

            {
                value: 'n_ic_volatility',
                label: '中证500股指期货'
            },
            {
                value: 'n_ih_volatility',
                label: '上证50股指期货'
            },
            {
                value: 'n_if_volatility',
                label: '沪深300股指期货'
            }
        ]
    }
]

