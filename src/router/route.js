import Home from '../page/home'
import Details from '../page/details'
export default [
    {
        name: '首页',
        path: '/',
        component: Home
    },
    {
        name: '首页',
        path: '/home',
        component: Home
    },
    {
        name: '产品详情',
        path: '/details',
        component: Details
    }
]