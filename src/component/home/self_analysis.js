import React, { Component } from 'react'
import { Tabs } from 'antd'
import ScaleTab from './scale_tab'
import ScaleProfitTab from './scale_profit_tab'
import AllPloyTab from './all_ploy_tab'
import PloyProfitTab from './ploy_profit_tab'

export default class SelfAnalysis extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    render () {
        return (
            <Tabs tabPosition={`left`} defaultActiveKey={`1`} className={`home_vertical_tabs mt_20`} onTabClick={this.checkTab.bind(this)}>
                <Tabs.TabPane tab={`规模收益总览`} key={`1`} className={`home_vertical_tabs`}>
                    <ScaleTab />
                </Tabs.TabPane>

                <Tabs.TabPane tab={`规模收益表`} key={`2`} className={`home_vertical_tabs`}>
                    <ScaleProfitTab />
                </Tabs.TabPane>
                <Tabs.TabPane tab={`策略情况总览`} key={`3`} className={`home_vertical_tabs`}>
                    <AllPloyTab ref={`allPloy`} />
                </Tabs.TabPane>
                <Tabs.TabPane tab={`策略收益表`} key={`4`} className={`home_vertical_tabs`}>
                    <PloyProfitTab />
                </Tabs.TabPane>
            </Tabs>
        )
    }

    checkTab (v) {
        if (v === '3' && this.refs.allPloy) {
            this.refs.allPloy.setState({ showtabs: true, showtables: false })
        }
    }
}
