import React, { Component } from 'react'
import { Radio, Table, DatePicker, message, Spin} from 'antd'
import HeaderTitle from '../common_title/common_header_title'
import HomeStyle from '../../page/home/home.module.sass'
import locale from 'antd/es/date-picker/locale/zh_CN'
import moment from 'moment'
import Title_1 from '../../static/img/title_1.png'
import Title_2 from '../../static/img/title_2.png'
import Title_3 from '../../static/img/title_3.png'
import Title_4 from '../../static/img/title_4.png'
import Title_5 from '../../static/img/title_5.png'
import Guding from '../../static/img/guding.png'
import Gupiao from '../../static/img/gupiao.png'
import Jiazhi from '../../static/img/jiazhi.png'
import Qihuo from '../../static/img/qihuo.png'
import Shichang from '../../static/img/shichang.png'
import { _getAllPloy, _getInitDate, _getAllPloyRate,
    _getAllPloyThrowStrategy, _getAllPloyRateMonth,
    _getAllPloyCapital, _getAllPloyCapitalMonth, _getAllPloyMonitor} from '../../apis/home'


export default class ScaleTab extends Component {
    constructor (props) {
        super(props)
        this.state = {
            radioValue: 'all',
            tableTitle:'权益中性',
            showtabs: true,
            showtables: false,
            initDate: null,
            columnsOne: [
                {
                    title: '策略名称',
                    dataIndex: 'strategyNames',
                    key: '1',
                    render: (text) => text ? text : '--'
                },
                {
                    title: '总收益(万)',
                    dataIndex: 'sumBenefit',
                    key: '2',
                    render: (text) => text !== null ? isNaN(text) ? text : (text / 10000).toFixed(2) : '--'
                },
                {
                    title: '今年以来年化收益',
                    dataIndex: 'returnYear',
                    key: '3',
                    render: (text) => text ? text : '--'
                },
                {
                    title: '市场前1/4水平年化收益(今年以来)',
                    dataIndex: 'marketReturnThisYear',
                    key: '4',
                    render: (text) => text ? text : '--'
                },
                {
                    title: '市场前1/4水平年化收益(近一年)',
                    dataIndex: 'marketReturnYear',
                    key: '5',
                    render: (text) => text ? text : '--'
                },
                {
                    title: '排名',
                    dataIndex: 'ranking',
                    key: '6',
                    render: (text) => text ? text : '--'
                },

            ],
            columnsTwoLeft: [
                {
                    title: '自营',
                    dataIndex: 'strategyName',
                    width:'110px',
                    key: '1',
                    render: text => text !== null ?  text :  '--'
                }
            ],
            columnsTwoRight: [
                {
                    title: '自营',
                    dataIndex: 'strategyName',
                    width:'150px',
                    key: '1',
                    render: text => text !== null ?  text :  '--'
                }
            ],
            columnsThree: [
                {
                    title: '策略名称',
                    dataIndex: 'strategyName',
                    width:'120px',
                    textWrap: 'word-break',
                    key: '1',
                    render: text => text !== null ?  text :  '--'
                },
                {
                    title: '1月',
                    dataIndex: '1',
                    key: '2',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '2月',
                    dataIndex: '2',
                    key: '3',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '3月',
                    dataIndex: '3',
                    key: '4',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '4月',
                    dataIndex: '4',
                    key: '5',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '5月',
                    dataIndex: '5',
                    key: '6',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '6月',
                    dataIndex: '6',
                    key: '7',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '7月',
                    dataIndex: '7',
                    key: '8',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '8月',
                    dataIndex: '8',
                    key: '9',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '9月',
                    dataIndex: '9',
                    key: '10',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '10月',
                    dataIndex: '10',
                    key: '11',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '11月',
                    dataIndex: '11',
                    key: '12',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '12月',
                    dataIndex: '12',
                    key: '13',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '总计',
                    dataIndex: 'sumBenefitWeek',
                    key: '14',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                }
            ],
            columnsFour: [
                {
                    title: '策略名称',
                    dataIndex: 'strategyName',
                    width:'120px',
                    key: '1',
                    render: text => text !== null ?  text :  '--'
                },
                {
                    title: '1月',
                    dataIndex: '1',
                    key: '2',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '2月',
                    dataIndex: '2',
                    key: '3',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '3月',
                    dataIndex: '3',
                    key: '4',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '4月',
                    dataIndex: '4',
                    key: '5',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '5月',
                    dataIndex: '5',
                    key: '6',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '6月',
                    dataIndex: '6',
                    key: '7',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '7月',
                    dataIndex: '7',
                    key: '8',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '8月',
                    dataIndex: '8',
                    key: '9',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '9月',
                    dataIndex: '9',
                    key: '10',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '10月',
                    dataIndex: '10',
                    key: '11',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '11月',
                    dataIndex: '11',
                    key: '12',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '12月',
                    dataIndex: '12',
                    key: '13',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '总计',
                    dataIndex: 'sumEquity',
                    key: '14',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                }
            ],
            columnsFive: [
                {
                    title: '投顾',
                    dataIndex: 'orgName',
                    key: '1',
                    render: text => text !== null ?  text : '--'
                },
                {
                    title: '产品',
                    dataIndex: 'productName',
                    key: '2',
                    render: text => text !== null ?  text : '--'
                },
                {
                    title: '策略',
                    dataIndex: 'strategyName',
                    key: '3',
                    render: text => text !== null ?  text : '--'
                },
                {
                    title: '已配金额',
                    dataIndex: 'allocatedAmount',
                    key: '4',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '自营穿透金额',
                    dataIndex: 'sumEquity',
                    key: '5',
                    render: text => text !== null ? isNaN(text) ? text : (text).toFixed(2) : '--'
                },
                {
                    title: '当前比例',
                    dataIndex: 'currentScale',
                    key: '6',
                    render: text => text !== null ? isNaN(text) ? text : (text * 100).toFixed(2) + '%' : '--'
                },
                {
                    title: '自营穿透比例',
                    dataIndex: 'ratio',
                    key: '7',
                    render: text => text !== null ? isNaN(text) ? text : (text * 100).toFixed(2) + '%' : '--'
                },
                {
                    title: '初次申购日期',
                    dataIndex: 'firstDate',
                    key: '8',
                    render: text => text !== null ?  text : '--'
                },
                {
                    title: '净值日期',
                    dataIndex: 'tDate',
                    key: '9',
                    render: text => text !== null ?  text : '--'
                },
            ],
            dataSourceOne: [],
            dataSourceTwo: [],
            dataSourceThree: [],
            dataSourceFour: [],
            dataSourceFive: [],
            dataSourceSix: [],
            parentArr: [],
            loading: true,
            monitorSize: 10,
            monitorNo: 1
        }
    }

    radioChange = e => {
        this.setState({
            radioValue: e.target.value,
        }, async () => {
            await this._initTable()
        })
    }

    // 时间更改
    async dateChange(date, s) {
        if (!s) {
            await this.setState({initDate: null})
            return
        }
        await this.setState({initDate: moment(s)}, async () => {
            await this._initTable()
        })
      }

    // 自营分析—默认时间
    async _getInitDate () {
        let params = {
            token: this.state.taken,
        }
        const datas = await _getInitDate(params)
        if (datas) {
            await this.setState({initDate: moment(datas.initTime), initDateArr: datas.allTime})
        }
    }
    // 切换表格时间
    changeTable(title) {
        this.setState({
            tableTitle: title,
            showtabs: false,
            showtables: true
        }, async () => {
            await this._initTable()
        })
    }

    async componentDidMount() {
        await this._getInitDate()
        await this._initData({})
    }

    async _initTable () {
        this.setState({
            throwLoading: true,
            monitorLoading: true,
            columnsOneLoading: true,
            columnsTwoLoading: true,
            columnsThreeLoading: true,
            columnsFourLoading: true,
            columnsFiveLoading: true}, async () => {
            await this._getAllPloyThrowStrategy({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue
            })
            await this._getAllPloyRate({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue
            })
            await this._getAllPloyRateMonth({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue
            })
            await this._getAllPloyCapital({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue
            })
            await this._getAllPloyCapitalMonth({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue
            })
            await this._getAllPloyMonitor({
                t_date: moment(this.state.initDate).format('YYYY-MM-DD'),
                category_name: this.state.tableTitle,
                fund_classify: this.state.radioValue,
                page_no: this.state.monitorNo,
                page_size: this.state.monitorSize
            })
        })
    }
    // 投出策略穿透清单
    async _getAllPloyThrowStrategy (params) {
        params = { ...this.state.throwParams, ...params}
        this.setState({throwParams: params}, async () => {
            const data = await _getAllPloyThrowStrategy(params)
            if (data && data.status && data.result){
                data.result.map((item, index) => item.key = index)
                this.setState({dataSourceOne: data.result, throwLoading: false})
            } else {
                this.setState({dataSourceOne: [], throwLoading: false})
            }
        })
    }

    // 自营策略收益分布
    async _getAllPloyRate (params) {
        params = {...this.state.ployRateParams, ...params}
        this.setState({ployRateParams: params}, async () => {
            const data = await _getAllPloyRate(params)
            if (data && data.status && data.result) {
                const nameList = data.result.namelist, resList = data.result.reslist, columns = this.state.columnsTwoLeft
                let columnsArr = nameList.filter(item => item !== 'strategyName' && item !== 'sumBenefitYear')
                resList.forEach((item, index) => item.key = index)
                columnsArr.forEach((item, index) => {
                    columns.push({
                        title: item,
                        dataIndex: item,
                        key: index + 2,
                        render: text => text !== null ? isNaN(text) ? text : text.toFixed(2) : '--'
                    })
                })
                columns.push({
                    title: '总计',
                    dataIndex: 'sumBenefitYear',
                    key: columnsArr.length + 2,
                    render: text => text !== null ? isNaN(text) ? text : text.toFixed(2) : '--'
                })

                this.setState({columnsTwoLeft: columns, dataSourceTwo: resList, columnsOneLoading: false })
            } else {
                this.setState({dataSourceTwo: [], columnsOneLoading: false})
            }
        })
    }

    // 自营策略收益分布（按月）
    async _getAllPloyRateMonth (params) {
        params = { ...this.state.rateMonthParams, ...params }
        this.setState({rateMonthParams: params}, async () => {
            const data = await _getAllPloyRateMonth(params)

            if (data && data.status && data.result) {
                let resList = data.result.reslist
                resList.forEach((item, index) => item.key = index)
                this.setState({dataSourceFour: resList,
                    columnsThreeLoading: false})
            } else {
                this.setState({dataSourceFour: [], columnsThreeLoading: false})
            }
        })
    }

    // 自营策略资金分布
    async _getAllPloyCapital (params) {
        const data = await _getAllPloyCapital(params)

        if (data && data.status && data.result) {
            const nameList = data.result.namelist, resList = data.result.reslist, columns = this.state.columnsTwoRight
            let columnsArr = nameList.filter(item => item !== 'strategyName' && item !== 'sumEquity')
            resList.forEach((item, index) => item.key = index)
            columnsArr.forEach((item, index) => {
                columns.push({
                    title: item,
                    dataIndex: item,
                    key: index + 2,
                    render: text => text !== null ? isNaN(text) ? text : text.toFixed(2) : '--'
                })
            })
            columns.push({
                title: '总计',
                dataIndex: 'sumEquity',
                key: columnsArr.length + 2,
                render: text => text !== null ? isNaN(text) ? text : text.toFixed(2) : '--'
            })

            this.setState({columnsTwoRight: columns, dataSourceThree: resList, columnsTwoLoading: false})
        } else {
            this.setState({dataSourceThree: [], columnsTwoLoading: false})
        }
    }

    // 自营策略资金分布（按月）
    async _getAllPloyCapitalMonth (params) {
        const data = await _getAllPloyCapitalMonth(params)
        if (data && data.status && data.result) {
            let resList = data.result.reslist
            resList.forEach((item, index) => item.key = index)
            this.setState({dataSourceFive: resList, columnsFourLoading: false})
        } else {
            this.setState({dataSourceFive: [], columnsFourLoading: false})
        }
    }
    // 投顾监控表
    async _getAllPloyMonitor (params) {
        const data = await _getAllPloyMonitor(params)
        console.log(data)
        if (data && data.status && data.result) {
            const list = data.result
            list.map((item, index) => item.key = index)
            this.setState({dataSourceSix: list, monitorTotal: data.sumCount, monitorLoading: false})
        } else {
            this.setState({dataSourceSix: [], monitorLoading: false})
        }
    }

    //  初始化查询
    async _initData (params) {
        const res = await _getAllPloy(params)
        message.destroy()
        if (res && res.result && res.status) {
            console.log(res.result,'res')

            this.setState({
                parentArr: res.result,
                loading: false
            })
            message.success(res.desc)
        }

    }

    // 统计周期时间选择过滤
    _disableDateArr (current) {
        if (this.state.initDateArr) {
            const selectArr = this.state.initDateArr
            return selectArr.indexOf(moment(current).format('YYYY-MM-DD')) < 0
        }
    }


    render () {
        return (
            <Spin spinning={this.state.loading} tip={'Loading...'}>
                <div style={{minHeight: '300px', width: '100%'}}>

                {this.state.showtabs ?
                        this.state.parentArr.map((item, index) => <div key={index}>
                            <div onClick={() => { this.changeTable(item.categoryName) }} className={`container_bg_fff 
                            ${(item.maxDrawdown * 100) > 5 && item.ratioConcentration > 10 ? HomeStyle.title_tags_red : ''} 
                            ${(item.maxDrawdown * 100) > 5 || item.ratioConcentration > 10 ? HomeStyle.title_tags_yellow : ''} 
                            ${(item.maxDrawdown * 100) <= 5 && item.ratioConcentration <= 10 ? HomeStyle.title_tags_grey : ''} 
                            ${HomeStyle.title_content_wrapper} mb_20`} style={{fontSize: '20px',color:'rgb(54,171,215)',borderRadius:'5px',}}>
                                <div style={{display: 'flex',justifyContent:'space-between'}}>
                                    <div style={{display: 'flex',justifyContent:'space-between'}}>
                                        <h2 className={`${HomeStyle.icon_content}`}>
                                            {item.categoryName === '' || item.categoryName === '权益多头' ?<img src={Title_1} alt={`权益中性`} /> : ''}
                                            {item.categoryName === '权益中性' ?<img src={Title_2} alt={`权益中性`} /> : ''}
                                            {item.categoryName === '期货' ?<img src={Title_3} alt={`期货`} /> : ''}
                                            {item.categoryName === '期权' ?<img src={Title_4} alt={`期权`} /> : ''}
                                            {item.categoryName === '现金' ?<img src={Title_5} alt={`现金`} /> : ''}
                                            {item.categoryName === '固定收益' ?<img src={Guding} alt={`固定收益`} /> : ''}
                                            {item.categoryName === '市场中性' ?<img src={Shichang} alt={`市场中性`} /> : ''}
                                            {item.categoryName === '相对价值' ?<img src={Jiazhi} alt={`相对价值`} /> : ''}
                                            {item.categoryName === '管理期货' ?<img src={Qihuo} alt={`管理期货`} /> : ''}
                                            {item.categoryName === '股票策略' ?<img src={Qihuo} alt={`股票策略`} /> : ''}
                                        </h2>
                                        <div style={{paddingLeft:'20px'}}>
                                            <h5>{item.categoryName ? item.categoryName : '--'}</h5>
                                            <h6>统计时间: {moment(item.getDate).format('YYYY-MM-DD')}</h6>
                                        </div>
                                    </div>
                                    <div>
                                        <h5>{item.managementEquity !== null ? (item.managementEquity / 100000000).toFixed(2): '--'}亿</h5>
                                        <h6>管理规模</h6>
                                    </div>
                                    <div>
                                        <h5>{item.earningEquity !== null ? (item.earningEquity / 100000000).toFixed(2): '--'}亿</h5>
                                        <h6>自营规模</h6>
                                    </div>
                                    <div>
                                        <h5 className={`color_number_red`}>{item.currentProfit !== null ? (item.currentProfit / 10000).toFixed(2): '--'}万</h5>
                                        <h6>当前收益</h6>
                                    </div>
                                    <div>
                                        <h5 className={`color_number_red`}>{item.earningProfit !== null ? (item.earningProfit / 10000).toFixed(2): '--'}万</h5>
                                        <h6>自营收益</h6>
                                    </div>
                                    <div>
                                        <h5>{item.configProducts !== null ? item.configProducts: '--'}</h5>
                                        <h6>配置产品数</h6>
                                    </div>
                                    <div>
                                        <h5>{item.tossStrategies !== null ? item.tossStrategies: '--'}</h5>
                                        <h6>投出策略数量</h6>
                                    </div>

                                </div>
                                <h6 className={`${HomeStyle.title_h6_grey}`}>
                                    最大回撤：{(item.maxDrawdown * 100).toFixed(2)}%，&nbsp; 投顾集中度：{item.ratioConcentration}
                                </h6>

                            </div>


                    </div>
                ) : null}

                {this.state.showtables ? (
                    <div>
                        <div className={`container_bg_fff ${HomeStyle.title_content_wrapper}`} style={{fontSize: '20px',color:'rgb(54,171,215)'}}>
                            {this.state.tableTitle}
                        </div>
                        <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_2`}>
                            <Radio.Group onChange={this.radioChange} value={this.state.radioValue}>
                                <Radio value={`all`}>全部资金</Radio>
                                <Radio value={`proprietary`}>自主管理</Radio>
                                <Radio value={`infmanagement`}>资管跟投</Radio>
                            </Radio.Group>
                            <span> 统计时间 : </span>
                            <DatePicker
                                showToday={false}
                                locale={locale}
                                disabledDate={this._disableDateArr.bind(this)}
                                value={this.state.initDate}
                                placeholder={`选择统计日期`}
                                onChange={this.dateChange.bind(this)}
                                locale={locale}/>

                        </div>
                        <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                            <HeaderTitle title={`投出策略穿透清单`} iconStatus={true}>

                            </HeaderTitle>
                            <Table loading={this.state.throwLoading}
                                   columns={this.state.columnsOne} dataSource={this.state.dataSourceOne}/>
                        </div>
                        <div className={`mt_20 container_flex_wrapper`}>
                            <div className={`flex_left_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                                <HeaderTitle title={`自营策略收益分布`} iconStatus={true} />
                                <Table scroll={{x: 'auto', y: '600'}}
                                       loading={this.state.columnsOneLoading}
                                       columns={this.state.columnsTwoLeft} dataSource={this.state.dataSourceTwo}/>
                            </div>
                            <div className={`flex_right_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                                <HeaderTitle title={`自营策略资金分布`} iconStatus={true} />
                                <Table
                                    loading={this.state.columnsTwoLoading}
                                    scroll={{x: 'auto', y: '600'}} columns={this.state.columnsTwoRight} dataSource={this.state.dataSourceThree}/>
                            </div>
                        </div>

                        <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                            <HeaderTitle title={`自营策略收益分布(按月)`} iconStatus={true}></HeaderTitle>
                            <Table
                                loading={this.state.columnsThreeLoading}
                                columns={this.state.columnsThree} dataSource={this.state.dataSourceFour}/>
                        </div>
                        <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                            <HeaderTitle title={`自营策略资金分布(按月)`} iconStatus={true}>

                            </HeaderTitle>
                            <Table
                                loading={this.state.columnsFourLoading}
                                columns={this.state.columnsFour} dataSource={this.state.dataSourceFive}/>
                        </div>
                        <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                            <HeaderTitle title={`投顾监控表`} iconStatus={true}>

                            </HeaderTitle>
                            <Table
                                loading={this.state.monitorLoading}
                                pagination={{
                                    total: this.state.monitorTotal,
                                    showQuickJumper: true,
                                    showSizeChanger: true,
                                    pageSizeOptions: ['10', '20', '50', '100'],
                                    current: this.state.monitorNo,
                                    pageSize: this.state.monitorSize
                                }}
                                columns={this.state.columnsFive} dataSource={this.state.dataSourceSix}/>
                        </div>
                    </div>
                ) : null}

































            </div>
            </Spin>)
    }
}
