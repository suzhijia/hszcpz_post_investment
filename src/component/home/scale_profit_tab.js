import React, { Component } from 'react'
import { Radio, Table, message } from 'antd'
import { DownloadOutlined } from '@ant-design/icons'
import HeaderTitle from '../common_title/common_header_title'
import NoneTitle from '../common_title/none_border_header'
import HomeStyle from '../../page/home/home.module.sass'
import '../../static/style/home/home.sass'
import { _getScaleList, _getScaleRateList } from '../../apis/home'
import iscroll from 'iscroll'
import moment from "moment";

export default class ScaleTab extends Component {
    constructor (props) {
        super(props)
        this.state = {
            radioValue: 'all',
            scaleRateList: true,
            scaleList: true,
            dataSourceOne:[],
            dataSourceTwo:[],
            rate_page_no: 1,
            rate_page_size: 10,
            scale_page_no: 1,
            scale_page_size: 10,
            sumRateCount: 0,
            sumScaleCount: 0,
            columnsOne: [
                {
                    title: '日期',
                    dataIndex: 'date',
                    width: 130,
                    key: '1'
                }
            ],
            columnsTwo: [
                {
                    title: '日期',
                    dataIndex: 'date',
                    width:'120px',
                    key: '1'
                }
            ]
        }
    }

    radioChange = e => {
        this.setState({ radioValue: e.target.value, scaleRateList: true, scaleList: true }, async () => {
            await this._initData()
        })
    }

    async componentDidMount() {
        await this._initData()
    }

    async _initData (params) {
        await this._initRateList({
            t_date: '',
            fund_classify: this.state.radioValue,
            page_no: this.state.rate_page_no,
            page_size: this.state.rate_page_size,
            ...params
        })
        await this._initScaleData({
            t_date: '',
            fund_classify: this.state.radioValue,
            page_no: this.state.scale_page_no,
            page_size: this.state.scale_page_size,
            ...params
        })
    }

    // 规模总览-收益总览
    async _initRateList (params) {
        params = {...this.state.rateParams, ...params}
        await this.setState({ scaleRateList: true, rateParams: params})
        const rateList = await _getScaleRateList(params)
        message.destroy()
        if (rateList && rateList.status) {
            console.log(rateList, '收益调整')
            const result = rateList.result
            const columnsTwo = this.state.columnsTwo
            const keys = Object.keys(result.data[0])
            keys.forEach((item, index) => {
                if (item !== 'date') {
                    if (item.indexOf('比例') !== -1) {
                        columnsTwo.push({
                            title: item ? item : '--',
                            dataIndex: item,
                            width: 120,
                            key: index + 3,
                            render:text  => text ? isNaN(text) ? text : (text * 100).toFixed(2) + '%' : '--'
                        })
                    } else {
                        columnsTwo.push({
                            title: item ? item : '--',
                            dataIndex: item,
                            width: 120,
                            key: index + 3,
                            render:text  => text ? isNaN(text) ? text : (text / 10000).toFixed(2) : '--'
                        })
                    }
                }
            })

            let arr = result.data.map((item,index) => {
                    const key = Object.keys(item)
                    const tempArr = {}
                    key.map(it  => {
                        tempArr['key'] = index
                        tempArr[it] = item[it]
                    })
                    return  tempArr
                }
            )

            this.setState({dataSourceTwo: arr, columnsTwo: columnsTwo, scaleRateList: false, sumRateCount: result.sum_count }, () => {
                new iscroll('#scaleRateTable .ant-table-body', {
                    scrollbars: 'custom',
                    mouseWheel: true,
                    preventDefault: false,
                    scrollX: true,
                    scrollY: false,
                    interactiveScrollbars: true,
                    shrinkScrollbars: false,
                    bounce: false
                })
            })
        }
    }
    //  规模总览-规模列表
    async _initScaleData (params) {
        params = {...this.state.scaleParams, ...params}
        await this.setState({ scaleList: true, scaleParams: params })
        const res = await _getScaleList(params)
        message.destroy()

        if (res && res.hasOwnProperty('data')) {
            const columns = this.state.columnsOne
            const keys = Object.keys(res.data[0])

            keys.forEach((item, index) => {
                if (item !== 'date') {
                    if (item.indexOf('比例') !== -1) {
                        columns.push({
                            title: item ? item : '--',
                            dataIndex: item,
                            width: 120,
                            key: index + 3,
                            render:text  => text ? isNaN(text) ? text : (text * 100).toFixed(2) + '%' : '--'
                        })
                    } else {
                        columns.push({
                            title: item ? item : '--',
                            dataIndex: item,
                            width: 120,
                            key: index + 3,
                            render:text  => text ? isNaN(text) ? text : (text / 10000).toFixed(2) : '--'
                        })
                    }
                }
            })
            let arr = res.data.map((item,index) => {
                const key = Object.keys(item)
                const tempArr = {}
                    key.map(it  => {
                        tempArr['key'] = index
                        tempArr[it] = item[it]
                    })
                    return  tempArr
                }
            )
            this.setState({ dataSourceOne: arr, columnsOne: columns, scaleList: false, sumScaleCount: res.sum_count }, () => {
                new iscroll('#scaleListTable .ant-table-body', {
                    scrollbars: 'custom',
                    mouseWheel: true,
                    preventDefault: false,
                    scrollX: true,
                    scrollY: false,
                    interactiveScrollbars: true,
                    shrinkScrollbars: false,
                    bounce: false
                })
            })
            message.success(res.desc || '加载成功')
        }
    }

    // 规模总览-收益表格更改
    rateChange (pagination, filter, sorter, e) {
        const { pageSize, current } = pagination
        this.setState({rate_page_size: pageSize, rate_page_no: current}, async () => {
            await this._initRateList({page_no: this.state.rate_page_no, page_size: this.state.rate_page_size})
        })
    }

    // 规模总览-规模表格更改
    scaleChange (pagination, filter, sorter, e) {
        const { pageSize, current } = pagination
        this.setState({scale_page_size: pageSize, scale_page_no: current}, async () => {
            await this._initScaleData({page_no: this.state.scale_page_no, page_size: this.state.scale_page_size})
        })
    }

    render () {
        return (<div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper}`}>
                <Radio.Group onChange={this.radioChange} value={this.state.radioValue}>
                    <Radio value={`all`}>全部资金</Radio>
                    <Radio value={`proprietary`}>自主管理</Radio>
                    <Radio value={`infmanagement`}>资管跟投</Radio>
                </Radio.Group>
            </div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                <NoneTitle title={`规模汇总表`} iconStatus={true} className={`position_relative `}>
                    <div className={`float_right`}>
                        <div className="icons-list">
                            <DownloadOutlined />
                        </div>
                    </div>
                </NoneTitle>
                <div  className={`position_relative mt_30`}>
                    <div className={`position_absolute table_total_position`}>
                        共 <span className={`color_number_ff37`}>{this.state.sumScaleCount}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
                        <span className={`pl_10`}>规模单位：万元</span>
                    </div>
                    <Table
                        className={`table_border_top table_rowspan_content`}
                        id={`scaleListTable`}
                        loading={this.state.scaleList}
                        columns={this.state.columnsOne}
                        dataSource={this.state.dataSourceOne}
                        onChange={this.scaleChange.bind(this)}
                        pagination={{
                            total: this.state.sumCount,
                            showSizeChanger: true,
                            pageSize: this.state.scale_page_size,
                            current: this.state.scale_page_no,
                            pageSizeOptions: ['10', '20', '50', '100'],
                        }}
                        scroll={{x: 1200}}
                    />
                </div>
            </div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                <NoneTitle title={`收益汇总表`} iconStatus={true}>
                    <div className={`float_right`}>
                        <div className="icons-list">
                            <DownloadOutlined />
                        </div>
                    </div>
                </NoneTitle>
                <div  className={`position_relative mt_30`}>
                    <div className={`position_absolute table_total_position`}>
                        共 <span className={`color_number_ff37`}>{this.state.sumRateCount}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
                        <span className={`pl_10`}>收益单位：万元</span>
                    </div>
                    <Table
                        className={`table_border_top table_rowspan_content`}
                        id={`scaleRateTable`}
                        loading={this.state.scaleRateList}
                        columns={this.state.columnsTwo}
                        dataSource={this.state.dataSourceTwo}
                        onChange={this.rateChange.bind(this)}
                        pagination={{
                            showSizeChanger: true,
                            pageSize: this.state.rate_page_size,
                            current: this.state.rate_page_no,
                            pageSizeOptions: ['10', '20', '50', '100'],
                        }}
                        scroll={{x: 1200}}
                    />
                </div>
            </div>

        </div>)
    }
}
