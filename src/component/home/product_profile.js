import React, { Component } from 'react'
import NoneTitle from '../common_title/none_border_header'
import HomeStyle from '../../page/home/home.module.sass'
import { Button, Input } from 'antd'
import ProfileTable from '../common_table/home/profile_table'
import { _getProfileInit, _getProfileTable } from '../../apis/home'
import pro_01 from '../../static/img/pro_01.svg'
import pro_02 from '../../static/img/pro_02.svg'
import pro_03 from '../../static/img/pro_03.svg'
import pro_04 from '../../static/img/pro_04.svg'
import moment from 'moment'
import url from 'url'


export default class Profile extends Component {
    constructor (props) {
        super(props)
        this.state = {
            initInfo: null
        }
    }

    render () {
        return (
        <div className={`home_profile`}>
            <h2 className={`${HomeStyle.profile_title}`}>总体产品情况
                {/* <span className={`${HomeStyle.date_time}`}>数据统计时间：{this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : '--'}</span> */}
                <span className={`${HomeStyle.date_time}`}>管理产品数量：{this.state.initInfo ? this.state.initInfo.fundNum ? isNaN(this.state.initInfo.fundNum) ? '--' :  this.state.initInfo.fundNum : '--' : '--'}</span>
                <span className={`${HomeStyle.date_time}`}>投出产品数量：{this.state.initInfo ? isNaN(this.state.initInfo.thFundNum) ? '--' : this.state.initInfo.thFundNum ? this.state.initInfo.thFundNum : '--' : '--'}</span>
            </h2>
            <ul className={`display_flex ${HomeStyle.flex_container}`}>
                <li className={`display_flex ${HomeStyle.flex_item}`}>
                    <div>
                        <h2>{this.state.initInfo ? isNaN(this.state.initInfo.managementScale) ? '--' : parseFloat(this.state.initInfo.managementScale).toFixed(2) : '--'}<em style={{ fontSize: 14, fontWeight: 'normal', marginLeft: 10}}>亿元</em></h2>
                        <p className={`${HomeStyle.item_title}`}>管理规模</p>
                    </div>
                    <div className={HomeStyle.icon_group}>
                            {/* <span className={`iconfont iconbennianduguimobianhua ${HomeStyle.icon_size}`}></span> */}
                        <img src={pro_01} className={HomeStyle.icon_size} />
                        <div className={`${HomeStyle.desc_date}`}>{this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : '--'}</div>
                    </div>
                    </li>
                    <li className={`display_flex ${HomeStyle.flex_item}`}>
                    <div>
                        <h2>{this.state.initInfo ? isNaN(this.state.initInfo.selfManager) ? '--' : parseFloat(this.state.initInfo.selfManager).toFixed(2) : '--'}<em style={{ fontSize: 14, fontWeight: 'normal', marginLeft: 10}}>亿元</em></h2>
                        <p className={`${HomeStyle.item_title}`}>本年度规模变化</p>
                    </div>
                    <div className={HomeStyle.icon_group}>
                        <img src={pro_02} className={HomeStyle.icon_size} />
                        <div className={`${HomeStyle.desc_date}`}>{this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : '--'}</div>
                    </div>
                    </li>
                    <li className={`display_flex ${HomeStyle.flex_item}`}>
                    <div>
                        <h2>{this.state.initInfo ? isNaN(this.state.initInfo.yearTotalRevenue) ? '--' : parseFloat(this.state.initInfo.yearTotalRevenue).toFixed(2) : '--'}<em style={{ fontSize: 14, fontWeight: 'normal', marginLeft: 10}}>万元</em></h2>
                        <p className={`${HomeStyle.item_title}`}>本年度总收益</p>
                        </div>
                        <div className={HomeStyle.icon_group}>
                            {/* <span className={`iconfont iconguanliguimo ${HomeStyle.icon_size}`}></span> */}
                            <img src={pro_03} className={HomeStyle.icon_size} />
                            <div className={`${HomeStyle.desc_date}`}>{this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : '--'}</div>
                        </div>
                    </li>
                    <li className={`display_flex ${HomeStyle.flex_item}`}>
                    <div>
                        <h2>{this.state.initInfo ? isNaN(this.state.initInfo.yearSelfRevenue) ? '--' : parseFloat(this.state.initInfo.yearSelfRevenue * 100).toFixed(2) : '--'}<em style={{ fontSize: 14, fontWeight: 'normal', marginLeft: 10}}>%</em></h2>
                        <p className={`${HomeStyle.item_title}`}>本年度收益率</p>
                        </div>
                        <div className={HomeStyle.icon_group}>
                            {/* <span className={`iconfont iconxiaoxi1 ${HomeStyle.icon_size}`}></span> */}
                            <img src={pro_04} className={HomeStyle.icon_size} />
                        <div className={`${HomeStyle.desc_date}`}>{this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : '--'}</div>
                        </div>
                </li>
            </ul>
                    {/* <dl className={`display_flex ${HomeStyle.flex_item}`}>
                        <dd>
                            <strong>{this.state.initInfo ? isNaN(this.state.initInfo.managementScale) ? '--' : parseFloat(this.state.initInfo.managementScale).toFixed(2) : '--'}<em>亿元</em></strong>
                            <span>管理规模</span>
                        </dd>
                        <dd>
                            <strong>
                            {this.state.initInfo ? isNaN(this.state.initInfo.selfManager) ? '--' : parseFloat(this.state.initInfo.selfManager).toFixed(2) : '--'}<em>亿元</em></strong>
                            <span>本年度规模变化</span>
                        </dd>
                    </dl>

                    <dl className={`display_flex ${HomeStyle.flex_item}`}>
                        <dd>
                            <strong>{this.state.initInfo ? this.state.initInfo.fundNum ? isNaN(this.state.initInfo.fundNum) ? '--' :  this.state.initInfo.fundNum : '--' : '--'}</strong>
                            <span>管理产品数量</span>
                        </dd>
                        <dd>
                        {this.state.initInfo ? isNaN(this.state.initInfo.thFundNum) ? '--' : this.state.initInfo.thFundNum ? this.state.initInfo.thFundNum : '--' : '--'}</strong>
                            <span>投出产品数量</span>
                        </dd>
                    </dl>

                    <dl className={`display_flex ${HomeStyle.flex_item}`}> */}
                        {/* <dd>
                            <strong>
                            {this.state.initInfo ? isNaN(this.state.initInfo.yearTotalRevenue) ? '--' : parseFloat(this.state.initInfo.yearTotalRevenue).toFixed(2) : '--'}<em>万元</em></strong>
                            <span>本年度总收益</span>
                        </dd>
                        <dd>
                            <strong>{this.state.initInfo ? isNaN(this.state.initInfo.yearSelfRevenue) ? '--' : parseFloat(this.state.initInfo.yearSelfRevenue * 100).toFixed(2) : '--'}<em>%</em></strong>
                            <span>本年度收益率</span>
                        </dd>
                    </dl> */}
            {/* </div> */}

            <div className={`mt_20 container_bg_fff ${HomeStyle.home_content_wrapper}`}>
                <NoneTitle title={`产品浏览`} iconStatus={true}>
                    <div className={`float_right`}>
                        <Input placeholder={`产品名称/投资策略`} onPressEnter={this.searchType.bind(this)} onChange={this.setType.bind(this)} className={`${HomeStyle.profile_search}`} />
                        <Button type={`primary`} onClick={this.searchType.bind(this)}>搜索</Button>
                    </div>
                </NoneTitle>
                <ProfileTable _initDate={this._getProfileList.bind(this)} ref={`profileTable`} _stateDate={this.state.initInfo ? moment(this.state.initInfo.getDate).format('YYYY-MM-DD') : null} />
            </div>
            
        </div>)
    }

    async componentDidMount () {
        const { query } = url.parse(window.location.href, true)
        await this._getInit({fund_id: query.id})
    }

    // 投资策略 更改
    setType (e) {
        this.setState({strategyType: e.currentTarget.value})
    }

    //
    async searchType () {
        this.refs.profileTable._initData({strategy_type: this.state.strategyType})
    }

    async _getInit (params) {
        const data = await _getProfileInit(params)
        if (data) {
            this.setState({initInfo: data})
        }
    }

    async _getProfileList (params) {
        const data = await _getProfileTable(params)
        if (data) {
            return data
        }
    }

}
