import React, { Component } from 'react'
import { Button, Input, Table, Form, Modal, Upload, message } from 'antd'
import { CheckCircleOutlined, FileSearchOutlined , DeleteOutlined, ArrowDownOutlined } from '@ant-design/icons'
import HeaderTitle from '../common_title/common_header_title'
import HomeStyle from '../../page/home/home.module.sass'
import locale from 'antd/es/date-picker/locale/zh_CN'
import axios from '../../Utils/axios'


    const layout = {
        labelCol: {
            span: 5,
        },
        wrapperCol: {
            span: 19,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 5,
            span: 19,
        },
    };

    const props = {
        // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange({ file, fileList }) {
            if (file.status !== 'uploading') {
            console.log(file, fileList);
            }
        },
        defaultFileList: [

        ],
    };
export default class ReportManage extends Component {
    formRef = React.createRef();
    formRefModal = React.createRef();
    formRefModal2 = React.createRef();
    constructor (props) {
        super(props)
        this.state = {
            name:'',
            man:'',
            tableData:[],
            editData:{},
            deleteData:{},
            downloadData:{},
            showEditModal: false,
            upload: false,
            uploadParams:'block',
            realUpLoad:'none',
            columns: [
                {
                    title: '报告名称',
                    dataIndex: 'name',
                    key: '1',
                    align:'center'
                },
                {
                    title: '简介',
                    dataIndex: 'desc',
                    key: '2',
                    align:'center'
                },
                {
                    title: '创建人',
                    dataIndex: 'man',
                    key: '3',
                    align:'center'
                },
                {
                    title: '创建时间',
                    dataIndex: 'date',
                    key: '4',
                    align:'center'
                },
                {
                    title: '操作',
                    dataIndex: 'strategyType',
                    key: '5',
                    width: 260,
                    align:'center',
                    render: (t, r) => {
                        return [
                            <Button type='link' key={'1'} onClick={this.edit.bind(this, r) }>
                                <FileSearchOutlined />
                            </Button>,
                            <Button type='link' key={'2'} onClick={this.delete.bind(this, r) }>
                                <DeleteOutlined />
                            </Button>,
                            <Button type='link'  key={'3'} onClick={this.download.bind(this, r) }>
                                <ArrowDownOutlined />
                            </Button>
                        ]
                    }
                },
            ]
        }
    }

    onReset = () => {
        this.formRef.current.resetFields();
    };

    upload() {
        this.setState({ upload: true })
    }
    cancelUpload() {
        this.setState({ upload: true })
    }

    handleCancel = e => {
        this.setState({
            upload: false,
        });
        setTimeout(() => {

            this.setState({ uploadParams:'block', realUpLoad:'none'})

        }, 1000)

    };
    editHandleCancel = e => {
        this.setState({
            showEditModal: false,
        });


    };

    realUpload () {
        this.setState({ uploadParams:'none', realUpLoad:'block'})
    }


    async componentDidMount() {
        await this._initData()


    }

    async _getName (e) {
        const values = e.target.value
        await this.setState({name: values})

    }
    async _getMan (e) {
        const values = e.target.value
        await this.setState({man: values})
    }
    //  初始化查询
    async _initData () {
        // const res = await axios({
        //     url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
        //     method: 'get',
        //     params: {
        //         name:this.state.name,
        //         man:this.state.man
        //     }
        // })
        //
        // message.destroy()
        // console.log(res,'res')
        // if (res.status) {
        //
        //     let arr = res.result.data.map(
        //         (item,index) => {
        //             return  {
        //                 key:index,
        //                 name: item.name,
        //                 man: item.man,
        //                 desc: item.desc,
        //                 date:item.date,
        //                 id:item.id,
        //                 url:item.url
        //             }
        //         }
        //     )
        //     this.setState({tableData: arr})
        //     message.success(res.desc)
        //
        // }
    }

    edit (item,r) {
        console.log(item,'item')
        console.log(r,'r')
        this.setState({editData: item, showEditModal:true})
        console.log(this.state.editData)

    }
    delete (item,r) {
        this.setState({deleteData: item})
    }
    download (item,r) {
        this.setState({downloadData: item})
    }



    render () {
        return (
            <div className={`container_bg_fff ${HomeStyle.title_content_wrapper} mt_20`} style={{fontSize: '20px',color:'blue'}}>

               <Form  ref={this.formRef} name="horizontal_login" layout="inline" >
                    <Form.Item
                        label="报告名称 : "
                        name="name"
                    >
                        <Input onBlur={this._getName.bind(this)}  placeholder="请输入报告名称" />
                    </Form.Item>
                    <Form.Item
                        label="创建人 : "
                        name="man"
                    >
                        <Input onBlur={this._getMan.bind(this)}
                        placeholder="请输入创建人名称"
                        />
                    </Form.Item>
                    <Form.Item >
                        <Button onClick={() => {this._initData()}} type="primary" htmlType="submit" className={`mr_20`}>
                            搜索
                        </Button>
                        <Button type="primary" htmlType="button">
                            重置
                        </Button>

                    </Form.Item>
                </Form>

                <div>
                    <Button style={{color:'blue'}}  onClick={() => {this.upload()}}>
                       + 上传
                    </Button>
                </div>
                <div style={{margin:'20px'}}>

                </div>
                <Table  columns={this.state.columns} dataSource={this.state.tableData}/>

                <Modal
                    title="上传报告"
                    // width={`800px`}
                    centered
                    visible={this.state.upload}
                    footer={[

                    ]}
                    onCancel={this.handleCancel}

                >
                     <div style={{display:this.state.uploadParams}}>
                         <div style={{display:'flex',justifyContent:'center'}}>
                            <Form {...layout} ref={this.formRefModal} name="horizontal_login"  style={{width:'408px'}}>
                                <Form.Item
                                    label="报告名称 : "
                                    name="username"
                                >
                                    <Input  placeholder="请输入报告名称" />
                                </Form.Item>
                                <Form.Item
                                    label="报告简介 : "
                                    name="password"
                                >
                                    <Input.TextArea />
                                </Form.Item>
                                <Form.Item
                                    label="上传附件 : "
                                    name="username"
                                >
                                    <Upload {...props}>
                                        <Button type="link">
                                            添加附件
                                        </Button>
                                    </Upload>
                                    <div style={{fontSize:'12px'}}>
                                        文件不能超过10M
                                    </div>
                                </Form.Item>
                                <Form.Item {...tailLayout}>
                                    <Button type="primary" htmlType="submit" onClick={() => {this.realUpload()}}>
                                        上传
                                    </Button>


                                </Form.Item>
                            </Form>
                         </div>


                     </div>
                     <div style={{display:this.state.realUpLoad}}>
                        <div style={{display:'flex',justifyContent:'center',fontSize:'16px',marginBottom:'20px'}}>
                           <span style={{color:'green',marginRight:'10px'}}><CheckCircleOutlined /></span>
                           <span>报告上传成功</span>
                        </div>
                        <div style={{display:'flex',justifyContent:'center'}}>
                            <Form {...layout} ref={this.formRefModal2} name="horizontal_login"  >
                                <Form.Item
                                    label="报告名称 : "
                                    name="username"
                                >
                                    2018萍乡万方资产报告
                                </Form.Item>
                                <Form.Item
                                    label="创建人 : "
                                    name="password"
                                >
                                    林阳
                                </Form.Item>
                                <Form.Item
                                    label="创建时间 : "
                                    name="username"
                                >
                                    2020-03-27 19:00:00
                                </Form.Item>

                            </Form>

                        </div>
                     </div>
                </Modal>
                <Modal
                    title="报告详情"
                    // width={`800px`}
                    centered
                    visible={this.state.showEditModal}
                    footer={[

                    ]}
                    onCancel={this.editHandleCancel}

                >
                     <div >
                         <div style={{display:'flex',justifyContent:'center'}}>
                            <Form {...layout} ref={this.formRefModal} name="horizontal_login"  style={{width:'408px'}}>
                                <Form.Item
                                    label="报告编号 : "
                                    name="username"
                                >
                                    {this.state.editData.id}
                                </Form.Item>
                                <Form.Item
                                    label="报告名称 : "
                                    name="username"
                                >
                                    {this.state.editData.name}
                                </Form.Item>
                                <Form.Item
                                    label="报告人 : "
                                    name="username"
                                >
                                    {this.state.editData.man}
                                </Form.Item>
                                <Form.Item
                                    label="报告时间 : "
                                    name="username"
                                >
                                    {this.state.editData.date}
                                </Form.Item>
                                <Form.Item
                                    label="报告简介 : "
                                    name="password"
                                >
                                    <Input.TextArea value={this.state.editData.desc}/>
                                </Form.Item>
                                <Form.Item
                                    label="报告附件 : "
                                    name="username"
                                >
                                    <span>{this.state.editData.url}</span>
                                    <span style={{marginLeft:'10px',color:'blue'}}>
                                        下载
                                    </span>
                                </Form.Item>

                            </Form>
                         </div>


                     </div>

                </Modal>






            </div>
        )
    }
}
