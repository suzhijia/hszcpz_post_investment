import React, { Component } from 'react'
import HeaderTitle from '../common_title/common_header_title'
import HomeStyle from '../../page/home/home.module.sass'
import CommonCharts from '../common_chart'
import {Radio, Spin, DatePicker} from 'antd'
import echarts from 'echarts'
import moment from 'moment'
import {areaColors, areasOptions, chartsColors} from '../../Utils/charts_config/charts_config'
import { _getCapitalScale, _getCategoryScale, _getCapitalProfit, _getCategoryProfit, _getInitDate, _getAllprofitChange,
    _getScaleDistribution, _getProductCount,  _getStrategyCount} from '../../apis/home'
import locale from "antd/lib/time-picker/locale/zh_CN";
export default class ScaleTab extends Component {
    constructor (props) {
        super(props)
        this.state = {
            scaleLeftChart: false,
            scaleRightChart: false,
            rateLeftChart: false,
            rateRightChart: false,
            scaleChangeChart: false,
            rateChangeChart: false,
            rateLeftChart2: false,
            rateRightChart2: false,
            radioValue: 'all',
            taken:'a26cf44e-1f5e-477c-a45a-cff762937362',
            totalOne:'',  // 总规模
            totalTwo:'',  // 总收益,
            initDate: null,  // 默认时间
            initDateArr: []
        }
        this.echarts1 = '';
        this.echarts2 = '';
        this.echarts3 = '';
        this.echarts4 = '';
        this.echarts5 = '';
        this.echarts6 = '';
        this.echarts7 = '';
        this.echarts8 = '';
        this.options1 = {
            color: chartsColors,
            tooltip: {
                trigger: 'item',
                formatter: function(params) {

                    return params['data']['name']+': '+Number(params['data']['value']/100000000).toFixed(2) +'亿元'+ ' (' +params['percent'] + '%)'
                }
            },
            // legend: {
            //     orient: 'vertical',
            //     // top: 'middle',
            //     bottom: 2,
            //     right: 5,
            //     data: []
            //     // data: ['资管跟投', '自主管理']
            // },
            series: [
                {
                    type: 'pie',
                    left:'left',
                    radius: ['50%', '70%'],
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    data: [],
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        padding: [6, 5, 6, 5],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '12',
                            fontWeight: 'bold'
                        }
                    },
                }
            ]
        };
        this.options2 = {
            color: chartsColors,
            tooltip: {
                trigger: 'item',
                formatter: function(params) {

                    return params['data']['name']+': '+ Number(params['data']['value']/100000000).toFixed(2)+'亿元'+ ' (' +params['percent'] + '%)'
                }
            },
            // legend: {
            //     orient: 'vertical',
            //     // top: 'middle',
            //     bottom: 2,
            //     right: 5,
            //     data: []
            //     // data: ['期货', '期权','权益多头','市场中性','现金']
            // },
            series: [
                {
                    type: 'pie',
                    left:'left',
                    radius: ['50%', '70%'],
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    data: [],
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        padding: [6, 5, 6, 5],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '12',
                            fontWeight: 'bold'
                        }
                    },
                }
            ]
        };
        this.options3 = {
            color: chartsColors,
            tooltip: {
                trigger: 'item',
                formatter: function(params) {

                    return params['data']['name']+': '+ Number(params['data']['value']/10000).toFixed(2)+'万元'+ ' (' +params['percent'] + '%)'
                }
            },
            // legend: {
            //     orient: 'vertical',
            //     // top: 'middle',
            //     bottom: 2,
            //     right: 5,
            //     data: []
            //     // data: ['资管跟投', '自主管理']
            // },
            series: [
                {
                    type: 'pie',
                    left:'left',
                    radius: ['50%', '70%'],
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    data: [
                        // {
                        //     value: 1548,
                        //     name: '资管跟投',
                        // },
                        // {value: 535, name: '自主管理'},
                    ],
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        padding: [6, 5, 6, 5],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '12',
                            fontWeight: 'bold'
                        }
                    },
                }
            ]
        };
        this.options4 = {
            color: chartsColors,
            tooltip: {
                trigger: 'item',
                formatter: function(params) {

                    return params['data']['name']+': '+ Number(params['data']['value']/10000).toFixed(2)+'万元'+ ' (' +params['percent'] + '%)'
                }
            },
            // legend: {
            //     orient: 'vertical',
            //     // top: 'middle',
            //     bottom: 2,
            //     right: 5,
            //     data: []
            // },
            series: [
                {
                    type: 'pie',
                    left:'left',
                    radius: ['50%', '70%'],
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    data: [],
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        padding: [6, 5, 6, 5],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '12',
                            fontWeight: 'bold'
                        }
                    },
                }
            ]
        };
        this.options5 = {
            color: chartsColors,
            title: {
                text: '近一年总规模与资产规模按月变化',
                textStyle:{
                    fontSize :12,
                    fontWeight:'normal',
                    lineHeight:30

                }
            },
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    var  str  = '占比 (%) ' + params[0].axisValue + '<br/>';
                    params.forEach(function(item,index){
                            str+= item.marker + item.seriesName+':' + Number(item.value).toFixed(2) + '%' + '<br/>';

                    })

                    return    str

                }

            },
            legend: {
                bottom:10,
                left:'center',
                // data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                data:[]
            },

            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis:
                {
                    type: 'category',
                    boundaryGap: false,
                    // data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                    data:[]
                }
            ,
            yAxis: {
                type: 'value',
                name:'占比(%)',
                axisLine:{
                    show:false
                },
                 splitLine:{show:true}
            },
            series: [
                // {
                //     name: '邮件营销',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [120, 132, 101, 134, 90, 230, 210]
                // },
                // {
                //     name: '联盟广告',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [220, 182, 191, 234, 290, 330, 310]
                // },
                // {
                //     name: '视频广告',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [150, 232, 201, 154, 190, 330, 410]
                // },
                // {
                //     name: '直接访问',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [320, 332, 301, 334, 390, 330, 320]
                // },
                // {
                //     name: '搜索引擎',
                //     type: 'line',
                //     stack: '总量',

                //     areaStyle: {},
                //     data: [820, 932, 901, 934, 1290, 1330, 1320]
                // }
            ]
        };
        this.options6 =  {
            color: chartsColors,
            title: {
                text: '近一年总收益与各类资产收益按月变化',
                textStyle:{
                    fontSize :12,
                    fontWeight:'normal',
                    lineHeight:30

                }
            },
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    var  str  = '收益 (万元) ' + params[0].axisValue + '<br/>';
                    params.forEach(function(item,index){
                            str+= item.marker + item.seriesName+':' + item.value+ '<br/>';

                    })

                    return    str

                }

            },
            legend: {
                bottom:10,
                left:'center',
                // data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                data:[]
            },

            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis:
                {
                    type: 'category',
                    boundaryGap: false,
                    // data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                    data:[]
                }
            ,
            yAxis: {
                type: 'value',
                name:'收益(万元)',
                axisLine:{
                    show:false
                },
                 splitLine:{show:true}
            },
            series: [
                // {
                //     name: '邮件营销',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [120, 132, 101, 134, 90, 230, 210]
                // },
                // {
                //     name: '联盟广告',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [220, 182, 191, 234, 290, 330, 310]
                // },
                // {
                //     name: '视频广告',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [150, 232, 201, 154, 190, 330, 410]
                // },
                // {
                //     name: '直接访问',
                //     type: 'line',
                //     stack: '总量',
                //     areaStyle: {},
                //     data: [320, 332, 301, 334, 390, 330, 320]
                // },
                // {
                //     name: '搜索引擎',
                //     type: 'line',
                //     stack: '总量',

                //     areaStyle: {},
                //     data: [820, 932, 901, 934, 1290, 1330, 1320]
                // }
            ]
        };
        this.options7 = {
            color: chartsColors,
            title: {
                text: '自营资产投资产品按照投资结构汇总',
                textStyle:{
                    fontSize :12,
                    fontWeight:'normal',
                    lineHeight:30

                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                bottom:10,
                left:'center',
                data: ['直接访问', '邮件营销', '联盟广告',]
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis:
                {
                    type: 'category',
                    data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                }
            ,
            yAxis:
                {
                    type: 'value',
                    axisLine:{
                        show:false
                    },
                     splitLine:{show:true}
                }
            ,
            series: [
                {
                    name: '直接访问',
                    type: 'bar',
                     stack: '广告',
                    data: [320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name: '邮件营销',
                    type: 'bar',
                    stack: '广告',
                    data: [120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name: '联盟广告',
                    type: 'bar',
                    stack: '广告',
                    data: [220, 182, 191, 234, 290, 330, 310]
                },

            ]
        };
        this.options8 = {
            color: chartsColors,
            title: {
                text: '自营资产按照投资策略数量汇总',
                textStyle:{
                    fontSize :12,
                    fontWeight:'normal',
                    lineHeight:30

                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                bottom:10,
                left:'center',
                data: ['直接访问', '邮件营销', '联盟广告',]
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis:
                {
                    type: 'category',
                    data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                }
            ,
            yAxis:
                {
                    type: 'value',
                    axisLine:{
                        show:false
                    },
                     splitLine:{show:true}
                }
            ,
            series: [
                {
                    name: '直接访问',
                    type: 'bar',
                    stack: '广告',
                    data: [320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name: '邮件营销',
                    type: 'bar',
                    stack: '广告',
                    data: [120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name: '联盟广告',
                    type: 'bar',
                    stack: '广告',
                    data: [220, 182, 191, 234, 290, 330, 310]
                },

            ]
        };

    }


    async componentDidMount () {
        this.setState({
            scaleLeftChart: true,
            scaleRightChart: true,
            rateLeftChart: true,
            rateRightChart: true,
            scaleChangeChart: true,
            rateChangeChart: true,
            rateLeftChart2: true,
            rateRightChart2: true
        })
        await this._getInitDate()
    }

    async radioChage (e) {
        const value = e.target.value
        await this.setState({radioValue: value})
        // console.log(this.state.radioValue)
        await this._getCapitalScale()
        await this._getCategoryScale()
        await this._getCapitalProfit()
        await this._getCategoryProfit()
        await this._getScaleDistribution()
        await this._getAllprofitChange()
        await this._getProductCount()
        await this._getStrategyCount()
    }

    async changeData(data,s) {
        // console.log(s)
        if (!s) {
            await this.setState({initDate: null})
            return
        }
        await this.setState({initDate: moment(s)})
        // console.log(this.state.initDate,'initDate')
        await this._getCapitalScale()
        await this._getCategoryScale()
        await this._getCapitalProfit()
        await this._getCategoryProfit()
        await this._getScaleDistribution()
        await this._getAllprofitChange()
        await this._getProductCount()
        await this._getStrategyCount()
    }

    // 自营分析—默认时间
    async _getInitDate () {
        let params = {
            token: this.state.taken,
        }
        const datas = await _getInitDate(params)
        if (datas) {
            // console.log(datas)
            await this.setState({initDate: moment(datas.initTime), initDateArr: datas.allTime})
            this._getCapitalScale()
            this._getCategoryScale()
            this._getCapitalProfit()
            this._getCategoryProfit()
            this._getScaleDistribution()
            this._getAllprofitChange()
            this._getProductCount()
            this._getStrategyCount()

        }
    }

    // 自营分析—规模收益总览-自营资产总规模 按资金结构划分
    async _getCapitalScale () {
        await this.setState({scaleLeftChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const datas = await _getCapitalScale(params)
        if (datas.status&&datas.result) {
            await this.setState({totalOne:Number(datas.sum_equity/100000000).toFixed(2)})

            let data = datas.result
            // this.options1.legend.data = data.map((item,index) => {
            //     return item.c_fund_classify_1st

            // })
            this.options1.series[0].data = data.map((item,index) => {
                return {
                    name:item.c_fund_classify_1st,
                    value:item.sum_equity
                }

            })


        }
        this.refs.scaleLeftChart.setOpitions(this.options1);
        this.setState({scaleLeftChart: false})
    }

    // 自营分析—规模收益总览-自营资产总规模 按大类策略划分
    async _getCategoryScale () {
        await this.setState({scaleRightChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getCategoryScale(params)
        if (data && data.length > 0) {
            // console.log(data, '大类策略划分')
            // this.options2.legend.data = data.map((item,index) => {
            //     return item.c_category_name

            // })
            this.options2.series[0].data = data.map((item,index) => {
                return {
                    name:item.c_category_name ,
                    value:item.sum_equity
                }

            })

        }
        this.refs.scaleRightChart.setOpitions(this.options2)
        await this.setState({scaleRightChart: false})
    }

    // 自营分析—自营资产总收益 按资金结构划分
    async _getCapitalProfit () {
        await this.setState({rateLeftChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const datas = await _getCapitalProfit(params)
        if (datas.status&&datas.result) {
            await this.setState({totalTwo:Number(datas.sum_equity/10000).toFixed(2)})

            let  data = datas.result
            this.options3.series[0].data = data.map((item,index) => {
                return {
                    name:item.fund_classify,
                    value:item.benefit
                }

            })
            this.refs.rateLeftChart.setOpitions(this.options3)
            await this.setState({rateLeftChart: false})
        }
    }

    // 自营分析—自营资产总收益 按大类资产划分
    async _getCategoryProfit () {
        await this.setState({rateRightChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getCategoryProfit(params)
        if (data) {

            this.options4.series[0].data = data.map((item,index) => {
                return {
                    name:item.c_category_name,
                    value:item.benefit
                }

            })
            this.refs.rateRightChart.setOpitions(this.options4)
            this.setState({rateRightChart: false})
        }
    }

    // 自营分析—规模分布变化
    async _getScaleDistribution () {
        await this.setState({scaleChangeChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getScaleDistribution(params)
        if (data) {
            this.options5.legend.data = data.name_list
            this.options5.xAxis.data = data.x_axis
            let temparr = [];
            data.y_axis.forEach((item,index) => {
                let obj = {}
                obj.key = item.key
                obj.value = item.value.map((v,i) => {
                    return Number(v*100).toFixed(2)
                })
                temparr.push(obj)
            })
            this.options5.series = temparr.map((item,index) => {
                return {
                    name:item.key,
                    type: 'line',
                    stack: '总量',
                    areaStyle: {},
                    data:item.value
                }


            })
            this.refs.scaleChangeChart.setOpitions(this.options5)
            this.setState({scaleChangeChart: false})
        }
    }

    // 自营分析—总收益率变化
    async _getAllprofitChange () {
        await this.setState({rateChangeChart: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getAllprofitChange(params)
        if (data) {
            this.options6.legend.data = data.name_list
            this.options6.xAxis.data = data.x_axis
            let temparr = [];
            data.y_axis.forEach((item,index) => {
                let obj = {}
                obj.key = item.key
                // console.log(item,'item')
                obj.value = item.value.map((v,i) => {
                    return Number(v/10000).toFixed(2)
                })
                temparr.push(obj)
            })

            this.options6.series = temparr.map((item,index) => {
                return {
                    name:item.key,
                    type: 'line',
                    stack: '总量',
                    areaStyle: {},
                    data:item.value
                }


            })
            this.refs.rateChangeChart.setOpitions(this.options6)
            this.setState({rateChangeChart: false})
        }
    }

    // 自营分析—出产品数量变化
    async _getProductCount () {
        await this.setState({rateLeftChart2: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getProductCount(params)
        // this.options7.series = []
        if (data) {
            this.options7.legend.data = data.name_list
            this.options7.xAxis.data = data.x_axis
            this.options7.series = data.y_axis.map((item,index) => {
                return {
                    name:item.key,
                    type: 'bar',
                    stack: '广告',
                    data:item.value,
                    barMaxWidth :40
                }


            })
            this.refs.rateLeftChart2.setOpitions(this.options7)
            this.setState({rateLeftChart2: false})
        }
    }

    // 自营分析—投出策略数量变化
    async _getStrategyCount () {
        await this.setState({rateRightChart2: true})
        let params = {
            token: this.state.taken,
            t_date: this.state.initDate,
            fund_classify: this.state.radioValue
        }
        const data = await _getStrategyCount(params)
        // this.options8.series = []
        if (data) {
            // console.log(data, '投出策略数量变化')
            // return
            this.options8.legend.data = data.name_list
            this.options8.xAxis.data = data.x_axis
            this.options8.series = data.y_axis.map((item,index) => {
                return {
                    name:item.key,
                    type: 'bar',
                    stack: '广告',
                    barMaxWidth :40,
                    data:item.value
                }
            })
            this.refs.rateRightChart2.setOpitions(this.options8)
            this.setState({rateRightChart2: false})
        }
    }

    // 统计周期时间选择过滤
    _disableDateArr (current) {
        if (this.state.initDateArr) {
            const selectArr = this.state.initDateArr
            return selectArr.indexOf(moment(current).format('YYYY-MM-DD')) < 0
        }
    }




    render () {
        return (<div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper}`}>
                <Radio.Group value={this.state.radioValue} onChange={this.radioChage.bind(this)}>
                    <Radio value={`all`}>全部资金</Radio>
                    <Radio value={`proprietary`}>自主管理</Radio>
                    <Radio value={`infmanagement`}>资管跟投</Radio>
                </Radio.Group>
                <span className={`pl_10`}>统计日期：</span>
                {/* <DatePicker defaultValue={moment(this.state.initDate, 'YYYY-MM-DD')} onChange={this.changeData.bind(this)}/> */}
                <DatePicker
                    showToday={false}
                    locale={locale}
                    disabledDate={this._disableDateArr.bind(this)}
                    value={this.state.initDate}
                    placeholder={`选择统计日期`}
                    onChange={this.changeData.bind(this)} />
            </div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                <HeaderTitle title={`自营资产总规模 ${this.state.totalOne} 亿元`} iconStatus={true} />
                <div className={`clear-fix`}>
                    <div className={`float_left ${HomeStyle.home_chart_left}`}>
                        <h3 className={`${HomeStyle.home_chart_tit}`}>按资金结构划分</h3>
                        <div className={`${HomeStyle.home_chart_split}`}>
                            <Spin spinning={this.state.scaleLeftChart} tip={`Loading...`}>
                                <CommonCharts idName={`scaleLeftChart`} ref={`scaleLeftChart`} styles={{width: '100%', height: '400px'}} />
                            </Spin>
                        </div>
                    </div>
                    <div className={`float_right ${HomeStyle.home_chart_right}`}>
                        <h3 className={`${HomeStyle.home_chart_tit}`} tip={`Loading...`}>按大类策略划分</h3>
                        <Spin spinning={this.state.scaleRightChart} ><CommonCharts idName={`scaleRightChart`} ref={`scaleRightChart`} styles={{width: '100%', height: '400px'}} /></Spin>
                    </div>
                </div>
            </div>

            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper} mt_20`}>
                <HeaderTitle title={`自营资产总收益 ${this.state.totalTwo} 万元`} iconStatus={true} />
                <div className={`clear-fix`}>
                    <div className={`float_left ${HomeStyle.home_chart_left}`}>
                        <h3 className={`${HomeStyle.home_chart_tit}`} tip={`Loading...`}>按资金结构划分</h3>
                        <div className={`${HomeStyle.home_chart_split}`}>
                            <Spin spinning={this.state.rateLeftChart}><CommonCharts idName={`rateLeftChart`} ref={`rateLeftChart`} styles={{width: '100%', height: '400px'}} /></Spin>
                        </div>
                    </div>
                    <div className={`float_right ${HomeStyle.home_chart_right}`}>
                        <h3 className={`${HomeStyle.home_chart_tit}`} tip={`Loading...`}>按大类策略划分</h3>
                        <Spin spinning={this.state.rateRightChart}><CommonCharts idName={`rateRightChart`} ref={`rateRightChart`} styles={{width: '100%', height: '400px'}} /></Spin>
                    </div>
                </div>
            </div>

            <div className={`mt_20 container_flex_wrapper`}>
                <div className={`flex_left_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                    <HeaderTitle title={`规模分布变化`} iconStatus={true} />
                    <Spin spinning={this.state.scaleChangeChart} tip={`Loading...`}><CommonCharts idName={`scaleChangeChart`} ref={`scaleChangeChart`} styles={{width: '100%', height: '500px'}} /></Spin>
                </div>
                <div className={`flex_right_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                    <HeaderTitle title={`总收益变化`} iconStatus={true} />
                    <Spin spinning={this.state.rateChangeChart} tip={`Loading...`}><CommonCharts idName={`rateChangeChart`} ref={`rateChangeChart`} styles={{width: '100%', height: '500px'}} /></Spin>
                </div>
            </div>

            <div className={`mt_20 container_flex_wrapper mb_20`}>
                <div className={`flex_left_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                    <HeaderTitle title={`投出产品数量变化`} iconStatus={true} />
                    <Spin spinning={this.state.rateLeftChart2} tip={`Loading...`}><CommonCharts idName={`rateLeftChart2`} ref={`rateLeftChart2`} styles={{width: '100%', height: '500px'}} /></Spin>
                </div>
                <div className={`flex_right_container  ${HomeStyle.home_content_wrapper} container_bg_fff`}>
                    <HeaderTitle title={`投出策略数据变化`} iconStatus={true} />
                    <Spin spinning={this.state.rateRightChart2} tip={`Loading...`}><CommonCharts idName={`rateRightChart2`} ref={`rateRightChart2`} styles={{width: '100%', height: '500px'}} /></Spin>
                </div>
            </div>
        </div>)
    }


}
