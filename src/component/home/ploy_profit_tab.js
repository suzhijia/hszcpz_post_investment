import React, { Component } from 'react'
import {message, Table} from 'antd'
import { DownloadOutlined } from '@ant-design/icons'
import NoneTitle from '../common_title/none_border_header'
import HomeStyle from '../../page/home/home.module.sass'
import moment from 'moment'
import iscroll from 'iscroll'


import { _getPloTable } from '../../apis/home'



export default class PloyProfit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSourceOne:[],
            total: 0,
            page_size: 20,
            page_no: 1,
            columnsOne: [
                {
                    title: '策略名称',
                    dataIndex: 'c_strategy_name',
                    width:'120px',
                    key: '1',
                    ellipsis: true,
                    render: t => t ? t : '--'
                },
                {
                    title: '产品名称',
                    dataIndex: 'c_prod_name',
                    key: '2',
                    ellipsis: true,
                    render: t => t ? t : '--'
                },
                {
                    title: '年初规模（万）',
                    dataIndex: 'n_sum_equity_start',
                    key: '3',
                    width: 130,
                    render: t => t ? parseFloat(String(t / 10000)).toFixed(2) : '--'
                },
                {
                    title: '当前规模（万）',
                    dataIndex: 'n_sum_equity_now',
                    key: '4',
                    width: 130,
                    render: t => t ? parseFloat(String(t / 10000)).toFixed(2) : '--'
                },
                {
                    title: '本周收益（万）',
                    dataIndex: 'n_benefit_week',
                    key: '5',
                    width: 130,
                    render: t => t ? parseFloat(String(t / 10000)).toFixed(2) : '--'
                },
                {
                    title: '本月收益（万）',
                    dataIndex: 'n_benefit_month',
                    key: '6',
                    width: 130,
                    render: t => t ? parseFloat(String(t / 10000)).toFixed(2) : '--'
                },
                {
                    title: '本年收益（万）',
                    dataIndex: 'n_benefit_year',
                    key: '7',
                    width: 130,
                    render: t => t ? parseFloat(String(t / 10000)).toFixed(2) : '--'
                },
                {
                    title: '资金结构',
                    dataIndex: 'c_fund_classify_1st',
                    key: '8',
                    render: t => t ? t : '--'
                },
                {
                    title: '资金分组',
                    dataIndex: 'c_fund_classify_2th',
                    key: '9',
                    render: t => t ? t : '--'
                },
                {
                    title: '大类资产',
                    dataIndex: 'c_category_name',
                    key: '10',
                    render: t => t ? t : '--'
                },
                {
                    title: '策略类型',
                    dataIndex: 'c_strategy_type_name',
                    key: '11',
                    render: t => t ? t : '--'
                },
                {
                    title: '计算日期',
                    dataIndex: 't_date',
                    width:'120px',
                    key: '12',
                    render: t => t ? t : '--'
                },
                {
                    title: '单位净值',
                    dataIndex: 'fund_nav',
                    key: '13',
                    render: t => t ? parseFloat(t).toFixed(4) : '--'
                },
                {
                    title: '净值日期',
                    dataIndex: 'new_nav_date',
                    key: '14',
                    render: t => t ? t : '--'
                },
                {
                    title: '管理人',
                    dataIndex: 'c_fund_manager_org',
                    key: '15',
                    ellipsis: true,
                    render: t => t ? t : '--'
                },
            ],
            ployLoading: true
        }
    }

    async componentDidMount() {
        await this._getInit({ token:'a26cf44e-1f5e-477c-a45a-cff762937362'})
        const eleTable = document.getElementById('homePloyTable').querySelector('.ant-table-body')
        eleTable.style.overflow = 'hidden'
    }

    async _getInit (params) {
        const data = await _getPloTable(params)
        if (data) {
            // console.log(data, '策略收益表')
            let arr = data.map(
                (item,index) => {
                    return  {
                        key:index,
                        c_strategy_name: item.c_strategy_name,
                        c_prod_name: item.c_prod_name,
                        n_sum_equity_start: item.n_sum_equity_start,
                        n_sum_equity_now: item.n_sum_equity_now,
                        n_benefit_week: item.n_benefit_week,
                        n_benefit_month: item.n_benefit_month,
                        n_benefit_year: item.n_benefit_year,
                        c_fund_classify_1st: item.c_fund_classify_1st,
                        c_fund_classify_2th: item.c_fund_classify_2th,
                        c_category_name: item.c_category_name,
                        c_strategy_type_name: item.c_strategy_type_name,
                        t_date: item.t_date,
                        fund_nav: item.fund_nav,
                        new_nav_date: item.new_nav_date,
                        c_fund_manager_org: item.c_fund_manager_org,
                    }
                }
            )
            this.setState({dataSourceOne: arr, total:data.length, ployLoading: false}, () => {
                new iscroll('#homePloyTable .ant-table-body', {
                    scrollbars: 'custom',
                    mouseWheel: true,
                    preventDefault: false,
                    scrollX: true,
                    scrollY: false,
                    interactiveScrollbars: true,
                    shrinkScrollbars: false,
                    bounce: false
                })
            })
        }
    }


    downloadTable = () => {
        message.destroy()
        window.location.href = `${process.env.REACT_APP_ROOT_IP}:9990/hsth/earning/strategyRevenueDown?token=123`
        message.loading('下载中...', 3)
    }

    render () {
        return (<div>
            <div className={`container_bg_fff ${HomeStyle.home_content_wrapper}`}>
                <NoneTitle title={`策略收益表`} iconStatus={true}>
                    <div className={`float_right`}>
                        <div className="icons-list">
                            <DownloadOutlined onClick={this.downloadTable}/>
                        </div>
                    </div>
                </NoneTitle>
                <div className={`position_relative mt_30`}>
                    <div className={`position_absolute table_total_position`}>
                        共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment().format('YYYY-MM-DD')}</span>
                    </div>
                    <Table
                        id={`homePloyTable`}
                        className={`table_border_top`}
                        loading={this.state.ployLoading}
                        onChange={this.tableChange.bind(this)}
                        pagination={{
                            showQuickJumper: true,
                            showSizeChanger: true,
                            pageSize: this.state.page_size,
                            current: this.state.page_no,
                            pageSizeOptions: ['10', '20', '50', '100'],
                        }}
                        columns={this.state.columnsOne} dataSource={this.state.dataSourceOne} scroll={{x: 120 * 15}} />
                </div>
            </div>
        </div>)
    }

    tableChange (pagination, filter, sorter, e) {
        const { pageSize, current } = pagination
        this.setState({page_size: pageSize, page_no: current})
    }
}
