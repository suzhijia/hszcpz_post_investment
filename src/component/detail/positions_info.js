import React from 'react'
import { DatePicker, Spin, message } from 'antd'
import { _getholderStack, _getConcentration, _getIndustry, _getIndustrySetting,
    _getStaticDate, _getFutureHold, _getFutureIndustry, _getFuturePie, _getFutureBar } from '../../apis/details/hold_api'
import HeaderTitle from '../common_title/common_header_title'
import CommonCharts from '../common_chart/index'
import locale from 'antd/es/date-picker/locale/zh_CN'
import detailsStyle from './detail.module.sass'
import moment from 'moment'
import url from 'url'
import echarts from 'echarts'
import { commonOptions, chartsColors, areaColors, areasOptions, chartsOptions } from '../../Utils/charts_config/charts_config'

const { query } = url.parse(window.location.href, true)
export default class PositionsInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        holderLoading: false,
        stackLoading: false,
        industryLoading: false,
        stockSet: false,
        futureHolder: false,
        futureStack: false,
        futureIndustry: false,
        pieLoading: false,
        futureSet: false,
        stockStartDate: moment(new Date().getTime() - (365 * 24 * 60 * 60 * 1000)),
        stockEndDate: moment(new Date()),
        futureStartDate: moment(new Date().getTime() - (365 * 24 * 60 * 60 * 1000)),
        futureEndDate: moment(new Date()),
        stockCheckStatus: true,
        futureCheckStatus: true
    }
  }


  createDate (target) {
    return (
      <div className="float_right">
        <DatePicker value={this.state[`${target}StartDate`]} locale={locale} onChange={(moment, str) => { this.checkDateRender(moment, str,'holderBar', 'start', target)}} placeholder={'开始日期'} />
        <strong className={`split_span_date`}>~</strong>
        <DatePicker value={this.state[`${target}EndDate`]} locale={locale} onChange={(moment, str) => { this.checkDateRender(moment, str,'holderBar', 'end', target)}} placeholder={'结束日期'} />
      </div>
    )
  }

  createDatePicker () {
    return (
      <div className="float_right">
        统计日期：
        <DatePicker locale={locale} onChange={(moment, str) => { this.checkDateRender(moment, str,'holderBar', 'end')}} placeholder={'结束日期'} />
      </div>
    )
  }

  render() {
        return (
            <div>
            <div className={detailsStyle.positions_info}>
                <div className={detailsStyle.positions_height}></div>
                <div>
                    <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>股票多头</div>
                </div>
                <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                    <HeaderTitle iconStatus={true} title={`仓位总览`} children={this.createDate('stock')} />
                    <Spin spinning={this.state.holderLoading} tip={`Loading...`}>
                        <CommonCharts idName='holderBar' ref='holderBar' styles={{width: '100%', height: '500px'}} />
                    </Spin>
                </div>
                <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                    <HeaderTitle iconStatus={true} title={`板块配置`} />
                    <Spin spinning={this.state.stackLoading} tip={`Loading...`}>
                        <CommonCharts idName='holderStack' ref='holderStack' styles={{width: '100%', height: '500px'}}/>
                    </Spin>
                </div>
                <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                    <HeaderTitle iconStatus={true} title={`行业集中度`} />
                    <Spin spinning={this.state.industryLoading} tip={`Loading...`}>
                    <CommonCharts idName='industryStack' ref='industryStack' styles={{width: '100%', height: '500px'}} />
                    </Spin>
                </div>
                <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                    <HeaderTitle iconStatus={true} title={`行业配置`} children={this.createDatePicker()} />
                    <Spin spinning={this.state.stockSet}>
                    <CommonCharts idName='industrySet' ref='industrySet' styles={{width: '100%', height: '500px'}} />
                    </Spin>
                </div>

            </div>
                <div className={`mt_20`}>

                    <div className={`${detailsStyle.position_relative}`}>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqihuo'}></span>期货</div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg}`}>
                        <HeaderTitle iconStatus={true} title={`仓位总览`} children={this.createDate('future')} />
                        <Spin spinning={this.state.futureHolder} tip={`Loading...`}>
                            <CommonCharts idName='futureHolder' ref='futureHolder' styles={{width: '100%', height: '500px'}} />
                        </Spin>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`板块配置`} />
                        <Spin spinning={this.state.futureStack} tip={`Loading...`}>
                            <CommonCharts idName='futureStack' ref='futureStack' styles={{width: '100%', height: '500px'}}/>
                        </Spin>
                    </div>

                    <div className={`${detailsStyle.charts_item_bg} mt_20 clear-fix`}>
                        <HeaderTitle iconStatus={true} title={`资产配比`}>
                            <div className="float_right">
                                统计日期：
                                <DatePicker locale={locale} placeholder={'结束日期'} />
                            </div>
                        </HeaderTitle>
                        <div className={`float_left ${detailsStyle.future_left}`}>
                            <Spin spinning={this.state.pieLoading} tip={`Loading...`}>
                                <CommonCharts idName='futureIndustryPie' ref='futureIndustryPie' styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                        <div className={`float_right ${detailsStyle.future_right}`}>
                            <Spin spinning={this.state.futureIndustry} tip={`Loading...`}>
                                <CommonCharts idName='futureIndustry' ref='futureIndustry' styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    async componentDidMount () {

        this.setState({pieLoading: true}, async () => {
         // 股票多头，持仓信息
        await this._getHolder({
              token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
              fundId: query.id,
              startDate: moment(this.state.stockStartDate).format('YYYY-MM-DD'),
              endDate: moment(this.state.stockEndDate).format('YYYY-MM-DD')
          })
        // 股票多头，板块配置
        await this._getAllocation({
              token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
              fundId: query.id,
              beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
              endTime: moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000
          })
        // 股票多头，行业集中度
        await this._getIndustry({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            fundId: query.id,
            beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
            endTime:  moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000})
        // 股票多头，行业配置
        await this._getIndustrySet({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            c_fund_id: query.id,
            end_time: 1556553600000 / 1000
        })
        // 持仓，固定时间获取
        await this._getStatic({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            c_fund_id: query.id,
            type: 'industryConf'
        })

        // 持仓，期货模块-期货仓位总览
        await this._getFutureHold({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            fundId: query.id,
            startDate: moment(this.state.futureStartDate).format('YYYY-MM-DD'),
            endDate: moment(this.state.futureEndDate).format('YYYY-MM-DD')
        })

            //持仓，期货模块-板块配置
            await this._getFutureStack({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            fundId: query.id,
            beginTime: moment(moment(this.state.futureStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
            endTime: moment(moment(this.state.futureEndDate).format('YYYY-MM-DD')).valueOf() / 1000})

            // 持仓，期货模块-资产配比-饼图
            await this._getFuturePie({
                token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                fundId: query.id,
                tDate: (Date.parse(new Date())) / 1000
            })
        })

        // 持仓，期货模块-资产配比-柱状图
        await this._getFutureBar({
            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
            fundId: query.id,
            tDate: (Date.parse(new Date())) / 1000
        })
    }
    // 时间选择问题
    checkDateRender (mom, str, ele, target, parent) {
      if (str) {
        const currentVal = moment(moment(str).format('YYYY-MM-DD')).valueOf()
        if (target === 'start') {
            if (this.state[`${parent}EndDate`]) {
                const endValue = moment(moment(this.state[`${parent}EndDate`]).format('YYYY-MM-DD')).valueOf()
                if (currentVal > endValue) {
                    message.destroy()
                    message.info('开始时间不能大于结束时间')
                    return
                }
            }
            this.setState({[`${parent}StartDate`]: moment(str)}, async () => {
                if (this.state[`${parent}CheckStatus`]) {
                    if (parent === 'stock') {
                        // 股票多头，持仓信息
                        await this._getHolder({
                            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                            fundId: query.id,
                            startDate: moment(this.state.stockStartDate).format('YYYY-MM-DD'),
                            endDate: moment(this.state.stockEndDate).format('YYYY-MM-DD')
                        })
                        // 股票多头，板块配置
                        await this._getAllocation({
                            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                            fundId: query.id,
                            beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                            endTime: moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000
                        })
                        // 股票多头，行业集中度
                        await this._getIndustry({
                            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                            fundId: query.id,
                            beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                            endTime:  moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000})
                    } else {
                        // 持仓，期货模块-期货仓位总览
                        await this._getFutureHold({
                            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                            fundId: query.id,
                            startDate: moment(this.state.futureStartDate).format('YYYY-MM-DD'),
                            endDate: moment(this.state.futureEndDate).format('YYYY-MM-DD')
                        })

                        //持仓，期货模块-板块配置
                        await this._getFutureStack({
                            token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                            fundId: query.id,
                            beginTime: moment(moment(this.state.futureStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                            endTime: moment(moment(this.state.futureEndDate).format('YYYY-MM-DD')).valueOf() / 1000})
                    }
                }
            })
        } else {
            if (this.state[`${parent}StartDate`]) {
                const startValue = moment(moment(this.state[`${parent}StartDate`]).format('YYYY-MM-DD')).valueOf()
                if (startValue > currentVal) {
                    message.destroy()
                    message.info('开始时间不能大于结束时间')
                    return
                }
            }
            this.setState({[`${parent}EndDate`]: moment(str), [`${parent}CheckStatus`]: true}, async () => {
                if (parent === 'stock') {
                    // 股票多头，持仓信息
                    await this._getHolder({
                        token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                        fundId: query.id,
                        startDate: moment(this.state.stockStartDate).format('YYYY-MM-DD'),
                        endDate: moment(this.state.stockEndDate).format('YYYY-MM-DD')
                    })
                    // 股票多头，板块配置
                    await this._getAllocation({
                        token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                        fundId: query.id,
                        beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                        endTime: moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000
                    })
                    // 股票多头，行业集中度
                    await this._getIndustry({
                        token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                        fundId: query.id,
                        beginTime: moment(moment(this.state.stockStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                        endTime:  moment(moment(this.state.stockEndDate).format('YYYY-MM-DD')).valueOf() / 1000})
                } else {
                    // 持仓，期货模块-期货仓位总览
                    await this._getFutureHold({
                        token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                        fundId: query.id,
                        startDate: moment(this.state.futureStartDate).format('YYYY-MM-DD'),
                        endDate: moment(this.state.futureEndDate).format('YYYY-MM-DD')
                    })

                    //持仓，期货模块-板块配置
                    await this._getFutureStack({
                        token: 'dee27c9d-2c80-4cdb-b71d-c27ccfe5346f',
                        fundId: query.id,
                        beginTime: moment(moment(this.state.futureStartDate).format('YYYY-MM-DD')).valueOf() / 1000,
                        endTime: moment(moment(this.state.futureEndDate).format('YYYY-MM-DD')).valueOf() / 1000})
                }
            })
        }
      } else {
          if (target === 'start') {
              this.setState({[`${parent}StartDate`]: null, [`${parent}CheckStatus`]: false })
          } else {
              this.setState({[`${parent}EndDate`]: null, [`${parent}CheckStatus`]: false })
          }
      }
    }
    // 股票多头，仓位总览
  async _getHolder (params) {
      this.setState({holderLoading: true}, async () => {
          const result = await _getholderStack(params)
          let holderOptions = JSON.parse(JSON.stringify(commonOptions))

          if (result.status === 'success') {
              let resultArr = {
                  legend: [],
                  xDate: [],
                  lineData: [],
                  barData: [],
                  max: ''
              }
              let results = result.result

              resultArr.legend.push(result.nav_name)
              resultArr.max = result.nav_max
              results.forEach(item => {
                  resultArr.xDate.push(item.t_date_std)
                  resultArr.lineData.push(item.nav)
                  resultArr.barData.push(item.position_ratio)
              })

              holderOptions.legend.data = resultArr.legend
              holderOptions.xAxis[0].data = resultArr.xDate
              holderOptions.yAxis[0].max = 100
              holderOptions.yAxis[1].max = resultArr.max
              holderOptions.yAxis[1].interval = resultArr.max / 5

              if (resultArr.lineData.length > 0) {
                  holderOptions.series[0].data = resultArr.barData
                  holderOptions.series[1].data = resultArr.lineData
                  holderOptions.series[1].name = resultArr.legend[0]
              } else {
                  holderOptions.series = []
              }

              this.refs.holderBar.setOpitions(holderOptions)
          }

          this.setState({holderLoading: false})
      })
  }
    // 股票多头，板块配置
  async _getAllocation (params) {
      this.setState({stackLoading: true}, async () => {
          const result = await _getConcentration(params)
          if (result.result === 'success') {
              let datas = result.tectonic
              let xDate = Object.keys(datas)
              let returnData = {}
              let firstData = [],  secData = [], threeData = [], lastData = [], legend = []

              // eslint-disable-next-line no-unused-vars
              for (let item in datas) {
                  let keys = Object.keys(datas[item])
                  legend = keys
                  firstData.push(datas[item][keys[0]])
                  secData.push(datas[item][keys[1]])
                  threeData.push(datas[item][keys[2]])
                  lastData.push(datas[item][keys[3]])
              }

              returnData.xDate = xDate
              returnData.firstData = firstData
              returnData.secData = secData
              returnData.threeData = threeData
              returnData.lastData = lastData
              returnData.legend = legend

              let stackOptions = {
                  tooltip: {
                      trigger: 'axis',
                      axisPointer: {
                          type: 'line',
                          lineStyle: {
                              width: 60,
                              color: 'rgba(0, 0, 0, 0.1)'
                          },
                          z: -1
                      }
                  },
                  legend: {
                      icon: 'rect',
                      data: returnData.legend || [],
                      align: 'left',
                      bottom: 0,
                      color: '#eee',
                      show: false
                  },
                  grid: {
                      top: '50px',
                      right: '46px',
                      left: '46px',
                      bottom: 68
                  },
                  xAxis: [{
                      type: 'category',
                      data: returnData.xDate || [],
                      axisLine: {
                          show: true,
                          lineStyle: {
                              color: '#ccc'


                          }
                      },
                      axisLabel: {
                          margin: 10,
                          color: '#999',
                          show: true,
                          textStyle: {
                              fontSize: 14
                          },
                      },
                  }],
                  yAxis: [
                      {
                          name: '占股票持仓比重(%)',
                          nameLocation: 'end',
                          max: 100,
                          nameTextStyle: {
                              color: '#999',
                              align: 'left'
                          },
                          color: '#ccc',
                          axisLabel: {
                              show: true,
                              typeStyle: {
                                  color: '#333'
                              },
                              color: '#999',
                              formatter: '{value} '
                          },
                          axisLine: {
                              show: false,
                              lineStyle: {
                                  color: '#ccc'
                              }
                          },
                          splitLine: {
                              lineStyle: {
                                  type: 'dotted',
                                  color: '#ccc'
                              }
                          }
                      }
                  ],
                  dataZoom: [
                      {
                          type: 'inside',
                          height: 30,
                          left: '70px',
                          right: '70px',
                          bottom: 0,
                          start: returnData.xDate.length > 100 ?  90 : 0,
                          realtime: true
                      }
                  ],
                  series: [
                      {
                          name: returnData.legend[0]|| '',
                          type: 'bar',
                          data: returnData.firstData || [],
                          stack: 'four',
                          barMaxWidth: '30px',
                          large: true,
                          itemStyle: {
                              normal: {
                                  color: chartsColors[0],
                                  barBorderRadius: [1, 1, 1, 1],
                              }
                          }
                      },
                      {
                          name: returnData.legend[1]|| '',
                          type: 'bar',
                          data: returnData.secData || [],
                          stack: 'four',
                          barMaxWidth: '30px',
                          large: true,
                          lineStyle: {
                              normal: {
                                  shadowColor: 'rgba(0, 0, 0, 0.2)',
                                  shadowOffsetY: 2,
                                  shadowOffsetX: 2,
                                  shadowBlur: 5
                              }
                          },
                          itemStyle: {
                              normal: {
                                  color: chartsColors[1],
                              }
                          },
                      },
                      {
                          name: returnData.legend[2]|| '',
                          type: 'bar',
                          data: returnData.threeData || [],
                          symbol: false,
                          large: true,
                          symbolSize: 0,
                          barMaxWidth: '30px',
                          stack: 'four',
                          lineStyle: {
                              normal: {
                                  shadowColor: 'rgba(0, 0, 0, 0.2)',
                                  shadowOffsetY: 2,
                                  shadowOffsetX: 2,
                                  shadowBlur: 5
                              }
                          },
                          itemStyle: {
                              normal: {
                                  color: chartsColors[2],
                              }
                          },
                      },
                      {
                          name: returnData.legend[3]|| '',
                          type: 'bar',
                          data: returnData.lastData || [],
                          symbol: false,
                          symbolSize: 0,
                          barMaxWidth: '30px',
                          large: true,
                          stack: 'four',
                          lineStyle: {
                              normal: {
                                  shadowColor: 'rgba(0, 0, 0, 0.2)',
                                  shadowOffsetY: 2,
                                  shadowOffsetX: 2,
                                  shadowBlur: 5
                              }
                          },
                          itemStyle: {
                              normal: {
                                  color: chartsColors[3],
                              }
                          },
                      }
                  ]
              }
              if (datas.length > 0) {
                  this.refs.holderStack.setOpitions(stackOptions)
              }
              this.setState({stackLoading: false})
          }
      })

  }
    // 股票多头，行业集中度
    async _getIndustry (params) {
      this.setState({industryLoading: true}, async () => {
          const data = await _getIndustry(params)
          console.log(data)
          if (data.result === 'success') {
              let industryOption = JSON.parse(JSON.stringify(areasOptions))

              if (data.industryConcentration) {
                  let resultContent = data.industryConcentration
                  let resultArr = []
                  let objectKeys = Object.keys(resultContent ? resultContent : {})
                  objectKeys.forEach(item => {
                      resultArr.push(resultContent[item])
                  })

                  industryOption.xAxis[0].data = objectKeys
                  industryOption.series = [{
                      type: 'line',
                      data: resultArr || [],
                      symbol: 'none',
                      lineStyle: {
                          normal: {
                              shadowColor: 'rgba(0, 0, 0, 0.2)',
                              shadowOffsetY: 2,
                              shadowOffsetX: 2,
                              shadowBlur: 5
                          }
                      },
                      areaStyle: {normal: {
                              color: areaColors[0]
                          }},
                      itemStyle: {
                          normal: {
                              color: chartsColors[0],
                          }
                      },
                  }]

                  industryOption.yAxis[0].name = '占比(%)'
                  industryOption.yAxis[0].nameTextStyle = {
                      color: '#999',
                      align: 'left'
                  }
                  industryOption.xAxis[0].boundaryGap = false
                  industryOption.yAxis[0].axisTick = { show: false }
                  industryOption.dataZoom = { show: false }
                  industryOption.grid = {
                          right: '46px',
                          bottom: '4%',
                          left: '46px',
                          top: '80px',
                          containLabel: false
                  }

                  this.refs.industryStack.setOpitions(industryOption)
              } else {
                  message.destroy()
                  message.info(data.reason)
              }

          }
          this.setState({industryLoading: false})
      })
    }

    // 股票多头，行业配置
    async _getIndustrySet (params) {
      await this.setState({stockSet: true})
        const data = await _getIndustrySetting(params)
        let industrySetOpt = JSON.parse(JSON.stringify(areasOptions))

        if (data.result === 'success') {
            const arr = data.industryConf
            if (arr.length > 0) {
                const keys = Object.keys(arr[0])
                let seriesArr = []
                let firstYear = [], secendYear = [], lastYear = []
                let names = []

                let xData = []
                arr.forEach(item => {
                    xData.push(item[keys[0]])
                    seriesArr.push(item[keys[1]])
                })

                if (seriesArr.length > 0) {
                    seriesArr.forEach(item => {
                        const keys = Object.keys(item)
                        names = keys
                        firstYear.push(item[keys[0]])
                        secendYear.push(item[keys[1]])
                        lastYear.push(item[keys[2]])
                    })
                }
                let seriesTemp = [firstYear, secendYear, lastYear]

                industrySetOpt.xAxis[0].data = xData
                industrySetOpt.yAxis[0].name = '股票持仓占比（%）'
                industrySetOpt.tooltip.formatter = (value) => {
                    let itemDesc = `${value[0].name}`
                    value.forEach(item => {
                        itemDesc += `<br />${item.marker} ${item.seriesName}： ${item.value}%`
                    })
                    return itemDesc
                }
                industrySetOpt.yAxis[0].nameTextStyle = {
                    color: '#999',
                        align: 'end'
                }
                industrySetOpt.legend.data = names
                industrySetOpt.dataZoom = { show: false }
                industrySetOpt.series = []
                names.forEach((item, index) => {
                    industrySetOpt.series.push({
                        name: item,
                        type: 'bar',
                        barGap: 0,
                        barWidth: '14px',
                        itemStyle: {
                            normal: {
                                color: chartsColors[index],
                            }
                        },
                        data: seriesTemp[index]
                    })
                })

                this.refs.industrySet.setOpitions(industrySetOpt)
            }
        }
        this.setState({stockSet: false})
    }
    // 获取固定时间
    async _getStatic (params) {
      const data = await _getStaticDate(params)
        console.log(data)
    }
    // 持仓信息，期货模块-仓位总览
    async _getFutureHold (params) {
      await this.setState({futureHolder: true})
      const data = await _getFutureHold(params)
        let futureOptions = JSON.parse(JSON.stringify(commonOptions))

        if (data.status === 'success') {
            let resultArr = {
                legend: [],
                xDate: [],
                lineData: [],
                barData: [],
                max: ''
            }
            let results = data.result

            resultArr.legend.push(data.nav_name)
            resultArr.max = data.nav_max
            results.forEach(item => {
                resultArr.xDate.push(item.t_date_std)
                resultArr.lineData.push(item.nav)
                resultArr.barData.push(item.position_ratio)
            })

            futureOptions.legend.data = resultArr.legend
            futureOptions.xAxis[0].data = resultArr.xDate
            futureOptions.yAxis[0].max = 100
            futureOptions.yAxis[1].max = resultArr.max
            futureOptions.yAxis[1].interval = resultArr.max / 5

            if (resultArr.lineData.length > 0) {
                futureOptions.series[0].data = resultArr.barData
                futureOptions.series[1].data = resultArr.lineData
                futureOptions.series[1].name = resultArr.legend[0]
            } else {
                futureOptions.series = []
            }

            this.refs.futureHolder.setOpitions(futureOptions)
        }
        this.setState({futureHolder: false})
    }
    // 持仓信息，期货模块-板块配置
    async _getFutureStack (params) {
      await this.setState({futureStack: true})
      const data = await _getFutureIndustry(params)

        if (data.result === 'success') {
            let datas = data.tectonicFutures
            let xDate = Object.keys(datas)
            let returnData = {}
            let firstData = [],  secData = [], threeData = [], lastData = [], legend = []

            // eslint-disable-next-line no-unused-vars
            for (let item in datas) {
                let keys = Object.keys(datas[item])
                legend = keys
                firstData.push(datas[item][keys[0]])
                secData.push(datas[item][keys[1]])
                threeData.push(datas[item][keys[2]])
                lastData.push(datas[item][keys[3]])
            }

            returnData.xDate = xDate
            returnData.firstData = firstData
            returnData.secData = secData
            returnData.threeData = threeData
            returnData.lastData = lastData
            returnData.legend = legend

            let stackOptions = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'line',
                        lineStyle: {
                            width: 60,
                            color: 'rgba(0, 0, 0, 0.1)'
                        },
                        z: -1
                    }
                },
                legend: {
                    icon: 'rect',
                    data: returnData.legend || [],
                    align: 'left',
                    bottom: 0,
                    color: '#eee',
                    show: false
                },
                grid: {
                    top: '50px',
                    right: '46px',
                    left: '46px',
                    bottom: 68
                },
                xAxis: [{
                    type: 'category',
                    data: returnData.xDate || [],
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ccc'


                        }
                    },
                    axisLabel: {
                        margin: 10,
                        color: '#999',
                        show: true,
                        textStyle: {
                            fontSize: 14
                        },
                    },
                }],
                yAxis: [
                    {
                        name: '占期货持仓比重(%)',
                        nameLocation: 'end',
                        max: 100,
                        nameTextStyle: {
                            color: '#999',
                            align: 'left'
                        },
                        color: '#ccc',
                        axisLabel: {
                            show: true,
                            typeStyle: {
                                color: '#333'
                            },
                            color: '#999',
                            formatter: '{value} '
                        },
                        axisLine: {
                            show: false,
                            lineStyle: {
                                color: '#ccc'
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                type: 'dotted',
                                color: '#ccc'
                            }
                        }
                    }
                ],
                dataZoom: [
                    {
                        type: 'inside',
                        height: 30,
                        left: '70px',
                        right: '70px',
                        bottom: 0,
                        start: returnData.xDate.length > 100 ?  50 : 0,
                        realtime: true
                    }
                ],
                series: [
                    {
                        name: returnData.legend[0]|| '',
                        type: 'bar',
                        data: returnData.firstData || [],
                        stack: 'four',
                        barMaxWidth: '30px',
                        large: true,
                        itemStyle: {
                            normal: {
                                color: chartsColors[0],
                                barBorderRadius: [1, 1, 1, 1],
                            }
                        }
                    },
                    {
                        name: returnData.legend[1]|| '',
                        type: 'bar',
                        data: returnData.secData || [],
                        stack: 'four',
                        barMaxWidth: '30px',
                        large: true,
                        lineStyle: {
                            normal: {
                                shadowColor: 'rgba(0, 0, 0, 0.2)',
                                shadowOffsetY: 2,
                                shadowOffsetX: 2,
                                shadowBlur: 5
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: chartsColors[1],
                            }
                        },
                    },
                    {
                        name: returnData.legend[2]|| '',
                        type: 'bar',
                        data: returnData.threeData || [],
                        symbol: false,
                        large: true,
                        symbolSize: 0,
                        barMaxWidth: '30px',
                        stack: 'four',
                        lineStyle: {
                            normal: {
                                shadowColor: 'rgba(0, 0, 0, 0.2)',
                                shadowOffsetY: 2,
                                shadowOffsetX: 2,
                                shadowBlur: 5
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: chartsColors[2],
                            }
                        },
                    },
                    {
                        name: returnData.legend[3]|| '',
                        type: 'bar',
                        data: returnData.lastData || [],
                        symbol: false,
                        symbolSize: 0,
                        barMaxWidth: '30px',
                        large: true,
                        stack: 'four',
                        lineStyle: {
                            normal: {
                                shadowColor: 'rgba(0, 0, 0, 0.2)',
                                shadowOffsetY: 2,
                                shadowOffsetX: 2,
                                shadowBlur: 5
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: chartsColors[3],
                            }
                        },
                    }
                ]
            }
            if (xDate.length > 0) {
                this.refs.futureStack.setOpitions(stackOptions)
            }

            this.setState({futureStack: false})
        }
    }

    // 持仓信息，期货模块-资产配比-饼图模块
    async _getFuturePie (params) {
        this.setState({pieLoading: true}, async () => {
            const data = await _getFuturePie(params)
            let futurePieOption = JSON.parse(JSON.stringify(chartsOptions))

            if (data.result === 'success') {
                let pieResult = [{name: '', value: ''}]
                const resultContent = data.varietyPie

                resultContent.forEach((item, index) => {
                    pieResult.push({
                        value: item.depositRatio,
                        name: item.cVariety,
                        itemStyle: {
                            normal: {
                                color: chartsColors[index],
                                shadowColor: 'rgba(0, 0, 0, 0.3)',
                                shadowBlur: 6,
                            }
                        },
                        getDate: data.getDate
                    })
                })
                pieResult = pieResult.filter(item => item['name'] !== '' && item['value'] !== '')

                futurePieOption.series = [{
                    type: 'pie',
                    animation: true,
                    color: chartsColors,
                    radius: ['32%', '54%'],
                    center: ['50%', '50%'],
                    animationEasing: 'elasticOut',
                    animationDelay: function (idx) {
                        return Math.random() * 500;
                    },
                    labelLine: {
                        show: true,
                        normal: {
                            length: 40,
                            length2: 20,
                            lineStyle: {
                                color: '#ccc'
                            }
                        }
                    },
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        alignTo: 'edge',
                        padding: [6, 5, 6, 5],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    data: pieResult
                }]
                futurePieOption.xAxis[0] = {show: false}
                futurePieOption.tooltip = {
                    formatter: (value) => {
                        let toolTip = `${moment(value.data.getDate * 1).format('YYYY-MM-DD')}<br />`
                        toolTip += "".concat(value.marker).concat(value.name, "\uFF1A").concat(value.value, "%")
                        return toolTip
                    }
                }
                this.refs.futureIndustryPie.setOpitions(futurePieOption)
                this.setState({pieLoading: false})
            }
        })

    }
    // 持仓信息，期货模块-资产配比-柱状图模块
    async _getFutureBar (params) {
      await this.setState({futureIndustry: true})
        const data = await _getFutureBar(params)
        let futureBarOption = JSON.parse(JSON.stringify(chartsOptions))
        if (data && data.result === 'success') {
            const barResult = data.varietyBar
            let barsData = {legend: [], longBar: [], shortBar: [], getDate: data.getDate}

            barResult.forEach(item => {
                barsData.legend.push(item.cVariety)
                barsData.longBar.push(item.longDepositRatio)
                barsData.shortBar.push(-item.shortDepositRatio)
            })
            futureBarOption.legend = {
                data:['多头保证金比例', '空头保证金占比'],
                left: "center",
                icon: 'circle',
                top: '4%'
            }
            futureBarOption.grid = {
                top: '12%',
                left: '3%',
                right: '4%',
                bottom: '30'
            }
            futureBarOption.tooltip = {
                trigger: 'axis',
                axisPointer : {
                    type : 'shadow'
                },

                formatter: function (params) {
                    let items = `${barsData.getDate ? moment(Number(barsData.getDate)).format('YYYY-MM-DD') : moment(new Date()).format('YYYY-MM-DD')}<br />${params[0].name}`
                    const itemDesc = [`<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, #fe5c5a 0%,#faa565 100%);'></span>`,
                        `<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, rgba(172, 237, 255, 1) 0%, rgba(14, 190, 247, 1) 100%);'></span>`]
                    params.forEach((item, index) => {
                        let desc = ''
                        if (item.seriesName.indexOf('空头') !== -1) {
                            desc = itemDesc[1]
                        } else {
                            desc = itemDesc[0]
                        }
                        items += `<br /> ${desc}${item.seriesName}：${Math.abs(item.value)}%`
                    })

                    return items
                }
            }

            futureBarOption.xAxis[0].data = barsData.legend
            futureBarOption.series = [

                {
                    name: '多头保证金比例',
                    type: 'bar',
                    data: barsData.longBar,
                    barWidth: '20px',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            color: '#999',
                            formatter: function(params){return Math.abs(params.value) + '%'}
                        }
                    },
                    itemStyle: {
                        normal: {
                            padding: 6,
                            shadowColor: '#fe5c5a',
                            shadowBlur: 6,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: '#fe5c5a'
                            }, {
                                offset: 1,
                                color: '#faa565'
                            }]),
                            barBorderRadius: [20, 20, 20, 20]
                        }
                    },
                },
                {
                    name: '空头保证金占比',
                    type: 'bar',
                    data: barsData.shortBar,
                    barWidth: '20px',
                    itemStyle: {
                        normal: {
                            top: 3,
                            shadowColor: 'rgba(14, 190, 247, 0.8)',
                            shadowBlur: 6,
                            color:  new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(172, 237, 255, 1)'
                            }, {
                                offset: 1,
                                color: 'rgba(14, 190, 247, 1)'
                            }]),
                            barBorderRadius: [20, 20, 20, 20],
                        }
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'bottom',
                            color: '#999',
                            padding: 6,
                            formatter: function(params){return Math.abs(params.value) + '%'}
                        }
                    }
                }
            ]

            this.refs.futureIndustry.setOpitions(futureBarOption)
        }
        this.setState({futureIndustry: false})
    }

}
