import React, { Component } from 'react'
import {Anchor} from 'antd'
import HeaderTitle from '../common_title/common_header_title'
import DetailStyle from './detail.module.sass'
import { _getContractList } from '../../apis/details'
import url from 'url'

export default class Contract extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contractInfo: null
        }
    }

    render () {
        return (
            <div className={`mt_20 container_flex_wrapper`}>
                <Anchor affix={true} className={`${DetailStyle.anchor_container} contract_anchor`}>
                    <Anchor.Link title={`投资目标`} href={`#invest_scope`} />
                    <Anchor.Link title={`投资理念`} href={`#invest_scope1`} />
                    <Anchor.Link title={`投资范围`} href={`#invest_scope2`} />
                    <Anchor.Link title={`业绩报酬条款`} href={`#invest_scope3`} />
                    <Anchor.Link title={`开放期`} href={`#invest_scope4`} />
                    <Anchor.Link title={`托管人信息`} href={`#invest_scope5`} />
                    <Anchor.Link title={`业绩比较基准`} href={`#invest_scope6`} />
                    <Anchor.Link title={`管理期限`} href={`#invest_scope7`} />
                    <Anchor.Link title={`费用条款`} href={`#invest_scope8`} />
                    {/*<Anchor.Link title={`投资策略`} href={`#invest_scope9`} />*/}
                </Anchor>

                <div className={`${DetailStyle.contract_content}`}>
                    <div id={`invest_scope`}>
                        <HeaderTitle title={`投资目标`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.investmentTarget ? this.state.contractInfo.investmentTarget : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope1`}>
                        <HeaderTitle title={`投资理念`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.investmentIdea ? this.state.contractInfo.investmentIdea : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope2`}>
                        <HeaderTitle title={`投资范围`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.investmentScope ? this.state.contractInfo.investmentScope : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope3`}>
                        <HeaderTitle title={`业绩报酬条款`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.feePerformanceRemark ? this.state.contractInfo.feePerformanceRemark : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope4`}>
                        <HeaderTitle title={`开放期`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.openRule ? this.state.contractInfo.openRule : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope5`}>
                        <HeaderTitle title={`托管人信息`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.fundCustodian ? this.state.contractInfo.fundCustodian : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope6`}>
                        <HeaderTitle title={`业绩比较基准`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>

                                {this.state.contractInfo ? this.state.contractInfo.benchmark ? this.state.contractInfo.benchmark : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope7`}>
                        <HeaderTitle title={`管理期限`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.duration ? this.state.contractInfo.duration : '--' : '--'}
                            </p>
                        </div>
                    </div>
                    <div id={`invest_scope8`}>
                        <HeaderTitle title={`费用条款`} iconStatus={true} />
                        <div className={`mt_20 ${DetailStyle.anchor_content}`}>
                            <p>
                                {this.state.contractInfo ? this.state.contractInfo.feeTerms ? this.state.contractInfo.feeTerms : '--' : '--'}
                            </p>
                        </div>
                    </div>

                    {/*<div id={`invest_scope9`}>*/}
                    {/*    <HeaderTitle title={`投资策略`} iconStatus={true} />*/}
                    {/*    <div className={`mt_20 ${DetailStyle.anchor_content}`}>*/}
                    {/*        <p>*/}
                    {/*            {this.state.contractInfo ? this.state.contractInfo.investmentStrategy ? this.state.contractInfo.investmentStrategy : '--' : '--'}*/}
                    {/*        </p>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </div>
        )
    }

    async componentDidMount () {
        const { query } = url.parse(window.location.href, true)
        await this._getContract({fund_id: query.id})
    }
    async _getContract (params) {
        const data = await _getContractList(params)
        console.log(data)
        if (data) {
            this.setState({contractInfo: data})
        }
    }
}
