import React from 'react';
import { DatePicker, Select, Button, Spin,  Table, message, Input, Cascader } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import {_getRadar, _getSocketMarket, _getBondsMarket, _getFutureMarket,
    _sceneMarketNeutral, _getSpecialLine, _getSpecialRisk, _getSceneList } from '../../apis/details/scene_api'

import HeaderTitle from '../common_title/common_header_title'
import CommonCharts from '../common_chart/index'

import locale from 'antd/es/date-picker/locale/zh_CN'
import detailsStyle from './scences.module.sass'
import HomeStyle from '../../page/home/home.module.sass'
import RiskTableScences from './component/risk_table_scences'
import axios from '../../Utils/axios'
import url from 'url'
import {
    radarOption,
    chartsColors,
    areaColors,
    linesOptions,
    barsOptions,
    areasOptions
} from '../../Utils/charts_config/charts_config'
import moment from 'moment'
import BondsTable from '../../component/common_table/details/scene/bondsTable'
import FutureTable from '../common_table/details/scene/futureTable'
import MarketBasis from '../common_table/details/scene/market_basic'
import echarts from 'echarts'
import { neutralOptions, options, futureOptions } from '../../Utils/common/data'

const { Option } = Select
const { query } = url.parse(window.location.href, true)
const columnsText = {
    conf_intveral: {name: '周收益率区间范围'},
    n_stdev_a: {name: '波动率'},
    return_ratio: {name: '场景收益占比'},
    calmar: {name: 'Calmar比率'},
    max_drawdown: {name: '最大回撤'},
    n_count: {name: '周期数'},
    sharpe: {name: '夏普比率'},
    sortino: {name: 'Sortino比率'},
}

function renderFunction (t, r) {
    let text = ''
    switch (r['指标']) {
        case '周期数':
            return t
        case 'Calmar比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case 'Sortino比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case '夏普比率':
            return t ?  isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        default:
            if (typeof t === 'object' && Array.isArray(t)) {
                t.forEach((item, index) => {
                    if (index < t.length - 1) {
                        text += `${parseFloat(item * 100).toFixed(2)}% ~`
                    } else {
                        text += `${parseFloat(item * 100).toFixed(2)}%`
                    }
                })
            } else {
                text = `${parseFloat(t * 100).toFixed(2)}%`
            }

            return text
    }
}
export default class ScencesAnalysis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        token:'76d12f63-43be-44c3-a16c-25eb8077b724',
        fundId:'',
        riskTableData: [],
        sceneList: [],
        sceneSelect: [],
        navType: 'n_swanav', //净值类型
        // 股票多头
        indexNameStock:'c_hs300_trend',
        startDateStock:'',
        endDateStock:'',
        confLevelStock:'0.90',
        // 影响净值的关键市场变量
        startDateMarket:'',
        endDateMarket:'',
        // 市场中性  市场分阶段表现
        startDateMarketPerformance:'',
        endDateMarketPerformance:'',
        dimensionMarketPerformance:'n_basis_cost',
        // 债券
        startDateBond:'',
        endDateBond:'',
        dimension1Bond:'n_long_yield',
        dimension2Bond:'n_long_yield',
        confLevelBond:'0.95',
        dimensionOptions: options,
        lastDimensionOptions: options,
        filterDimension: options,
        // 期货
        startDateFutures:'',
        endDateFutures:'',
        futureDevFutures:'n_nhci_volatility', // 期货选择,南华商品指数
        nQuantileFutures:'', // N分位：按分位数切分。传参形式为小数，如0.1(一分位)
        futureDayFutures:'scenario_future_30', // M日波动率,30天
        confLevelFutures:'0.95',
        // 特殊场景分析
        startDatespecial:'',
        endDatespecial:'',
        dimension1special:'', // 特殊情景
        dimension2special:'', // 业绩基准
        confLevelspecial:'', // 净值类型

        columnsOne: [
            {
                title: '指标',
                dataIndex: '指标',
                key: '1',
                className:`${detailsStyle.tableBgc}`,
                width: 200,
                render: (t, r) => {
                    if (t === '夏普比率') {
                        const renders =
                            <span>{t}
                                <Input className={`table_columns_rows_input`} id={`socketId`} defaultValue={'2.5'} />
                                <em className={`table_columns_percent`}>%</em>
                            </span>
                        return renders
                    } else {
                        return t
                    }
                }
            },
            {
                title: '市场上涨',
                dataIndex: '市场上涨',
                key: '2',
                className:`${detailsStyle.tableBgc1}`,
                render:  (t, r) => renderFunction(t, r)
            },
            {
                title: '市场震荡',
                dataIndex: '市场震荡',
                key: '3',
                className:`${detailsStyle.tableBgc2}`,
                render:  (t, r) => renderFunction(t, r)
            },
            {
                title: '市场下跌',
                dataIndex: '市场下跌',
                key: '4',
                className:`${detailsStyle.tableBgc3}`,
                render:  (t, r) => renderFunction(t, r)
            },
        ],
        dataSourceOne:[],
        chartOneLoading: false
    }

  }

    getEcharts() {}

    // 起点对齐
    changeCheckStatus = (e) => {
        console.log('起点对齐', e)
    }

    // 参数获取  总的净值类型
    async getNavType (value) {
        await this.setState({ navType: value })
    }
    // 参数获取  市场分析表现--股票多头
    async indexNameStock (value) {
        await this.setState({ indexNameStock: value })
    }
    async startDateStock (date, dateString) {
        console.log(dateString)
        if (dateString) {
            await this.setState({startDateStock: dateString})
        }
    }
    async endDateStock (date, dateString) {
        if (dateString) {
            await this.setState({endDateStock: dateString})
        }
    }
    async confLevelStock (value) {
        await this.setState({ confLevelStock: value })
    }

    // 参数获取  影响净值的关键市场变量
    async startDateMarket (date, dateString) {
        if (dateString) {
            await this.setState({startDateMarket: dateString})
        }
    }
    async endDateMarket (date, dateString) {
        if (dateString) {
            await this.setState({endDateMarket: dateString})
            if (this.state.startDateMarket) {
                await this._getRadar({token: '76d12f63-43be-44c3-a16c-25eb8077b724', fundId: query.id,
                    navType: 'n_swanav',
                    dateRange: `${moment(this.state.startDateMarket).format('YYYY-MM-DD')}:${moment(this.state.endDateMarket).format('YYYY-MM-DD')}`})
            }
        }
    }

    // 参数获取  市场中性  市场分阶段表现
    async startDateMarketPerformance (date, dateString) {
        if (dateString) {
            await this.setState({startDateMarketPerformance: dateString})
        }
    }
    async endDateMarketPerformance (date, dateString) {
        if (dateString) {
            await this.setState({endDateMarketPerformance: dateString})
        }
    }
    async dimensionMarketPerformance (value) {
        await this.setState({ dimensionMarketPerformance: value })
    }

    // 参数获取  债券
    async startDateBond (date, dateString) {
        if (dateString) {
            await this.setState({startDateBond: dateString})
        }
    }
    async endDateBond (date, dateString) {
        if (dateString) {
            await this.setState({endDateBond: dateString})
        }
    }
    async dimension1Bond (value) {
        await this.setState({ dimension1Bond: value })
    }
    async dimension2Bond (value) {
        await this.setState({ dimension2Bond: value })
    }
    async confLevelBond (value) {
        await this.setState({ confLevelBond: value })
    }

    // 参数获取  期货
    async startDateFutures (date, dateString) {
        if (dateString) {
            await this.setState({startDateFutures: dateString})
        }
    }
    async endDateFutures (date, dateString) {
        if (dateString) {
            await this.setState({endDateFutures: dateString})
        }
    }
    async futureDevFutures (value) {
        await this.setState({ futureDevFutures: value })
    }
    async nQuantileFutures (e) {
        const values = e.target.value
        await this.setState({ nQuantileFutures: values })
    }
    async futureDayFutures (value) {
        await this.setState({ futureDayFutures: value })
    }
    async confLevelFutures (value) {
        await this.setState({ confLevelFutures: value })
    }

    // 参数获取  特殊场景分析
    async startDatespecial (date, dateString) {
        if (dateString) {
            await this.setState({startDatespecial: dateString})
        }
    }
    async endDatespecial (date, dateString) {
        if (dateString) {
            await this.setState({endDatespecial: dateString})
        }
    }
    async dimension1special (value) {
        await this.setState({ dimension1special: value })
    }
    async dimension2special (value) {
        await this.setState({ dimension2special: value })
    }
    async confLevelspecial (value) {
        await this.setState({ confLevelspecial: value })
    }


    // 市场分阶段表现--市场中性  影响净值的关键市场变量
    async _initMarketData () {
        let params = {
            token:this.state.token,
            fundId:this.state.fundId,
            navType:this.state.navType,
            indexName:this.state.indexNameStock,
            dateRange:this.state.startDateMarket + ':' +this.state.endDateMarket,
        }
        let data = JSON.stringify({...params})
        const res = await axios({
            url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
            method: 'post',
            data: data
        })

        message.destroy()
        console.log(res,'res')
        if (res.status) {



        }

    }

    // 市场分阶段表现--市场中性  市场分阶段表现
    async _initMarketPerformanceData () {
        let params = {
            token:this.state.token,
            fundId:this.state.fundId,
            navType:this.state.navType,
            dimension:this.state.dimensionMarketPerformance,
            dateRange:this.state.startDateMarketPerformance + ':' +this.state.endDateMarketPerformance,
        }
        let data = JSON.stringify({...params})
        const res = await axios({
            url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
            method: 'post',
            data: data
        })

        message.destroy()
        console.log(res,'res')
        if (res.status) {



        }

    }

    // 市场分阶段表现--债券
    async _initBondData () {
        let params = {
            token:this.state.token,
            fundId:this.state.fundId,
            navType:this.state.navType,
            confLevel:this.state.confLevelBond,
            dimension1:this.state.dimension1Bond,
            dimension2:this.state.dimension2Bond,
            dateRange:this.state.startDateMarketPerformance + ':' +this.state.endDateMarketPerformance,
        }
        let data = JSON.stringify({...params})
        const res = await axios({
            url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
            method: 'post',
            data: data
        })

        message.destroy()
        console.log(res,'res')
        if (res.status) {



        }

    }

    // 市场分阶段表现--期货
    async _initFuturesData () {
        let params = {
            token:'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId:this.state.fundId,
            navType:this.state.navType,
            confLevel:this.state.confLevelFutures,
            futureDev:this.state.futureDevFutures,
            nQuantile:this.state.nQuantileFutures,
            futureDay:this.state.futureDayFutures,
            dateRange:this.state.startDateFutures + ':' +this.state.endDateFutures,
        }
        let data = JSON.stringify({...params})
        const res = await axios({
            url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
            method: 'post',
            data: data
        })

        message.destroy()
        console.log(res,'res')
        if (res.status) {



        }

    }

    // 市场分阶段表现--特殊场景分析
    async _initSpecialScencesData () {
        let params = {
            token:'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId:this.state.fundId,
            navType:this.state.navType,
            confLevel:this.state.confLevelFutures,
            futureDev:this.state.futureDevFutures,
            nQuantile:this.state.nQuantileFutures,
            futureDay:this.state.futureDayFutures,
            dateRange:this.state.startDateFutures + ':' +this.state.endDateFutures,
        }
        let data = JSON.stringify({...params})
        const res = await axios({
            url: 'http://192.168.1.51:7300/mock/5e8340507717fa12de7d1ad2/example/mock',
            method: 'post',
            data: data
        })

        message.destroy()
        console.log(res,'res')
        if (res.status) {



        }

    }

    // 股票多头
    createParamOne () {
        return (
            <div className="float_right">
                    <span>指数:&nbsp;&nbsp;</span>
                    <Select style={{marginRight:'10px'}} defaultValue={this.state.indexNameStock} onChange={this.indexNameStock.bind(this)}>
                        <Option value="c_hs300_trend">沪深300指数</Option>
                        <Option value="c_csi500_trend">中证500指数</Option>
                        <Option value="c_csi800_trend">中证800指数</Option>
                        <Option value="c_sse_trend">上证指数</Option>
                        <Option value="c_gem_trend">创业板指数</Option>
                    </Select>
                    <DatePicker locale={locale} onChange={this.startDateStock.bind(this)} placeholder={'开始日期'} />
                    <strong className={`split_span_date`}>~</strong>
                    <DatePicker locale={locale} onChange={this.endDateStock.bind(this)} placeholder={'结束日期'} />
            </div>
        )
    }

    // 市场中性 影响净值的关键市场变量
    createParamTwo () {
        return (
            <div className="float_right">
                <DatePicker locale={locale} onChange={this.startDateMarket.bind(this)} placeholder={'开始日期'} />
                <strong className={`split_span_date`}>~</strong>
                <DatePicker locale={locale} onChange={this.endDateMarket.bind(this)} placeholder={'结束日期'} />
            </div>
        )
    }

    // 市场中性 市场分阶段表现
    createParamTwo2 () {
        return (
            <div className="float_right">
                <DatePicker locale={locale} onChange={this.startDateMarketPerformance.bind(this)} placeholder={'开始日期'} />
                <strong className={`split_span_date`}>~</strong>
                <DatePicker locale={locale} onChange={this.endDateMarketPerformance.bind(this)} placeholder={'结束日期'} />
            </div>
        )
    }

    // 债券市场 阶段表现联动更改
    async dimessionChange (value) {
        const lastOptions = this.state.filterDimension.filter(item => {
            let swichValue = ''
            if (value[0] === '到期收益率') {
                swichValue = '到期收益率'
            } else if (value[0] === '宏观经济') {
                swichValue = '通货膨胀'
            } else if (value[0] === '通货膨胀') {
                swichValue = '宏观经济'
            } else if (value[0] === '货币供应') {
                swichValue = '实体融资需求'
            } else if (value[0] === '实体融资需求') {
                swichValue = '货币供应'
            }
            return item.value === swichValue
        })

        if (this.state.filterState) {
            await this.setState({lastDimensionOptions: lastOptions})
        }

        // console.log(lastOptions)
        const dimension = value[value.length - 1]
        const { dimensionLatest } = this.state
        if (dimension === dimensionLatest) {
            message.info('选择的指标不能同后面的指标一致')
            return
        }
        await this.setState({dimension: dimension, lastDimensionOptions: lastOptions})
        // console.log(this.state.lastDimensionOptions[0].value, this.state.lastDimensionOptions[0].children[0].value)
        // await this._bondMarket()
    }

    // 债券市场 阶段表现的设置
    async dimessionLatest (value) {
        const lastDimession = value[value.length -1]
        const { dimension } = this.state
        const dimenssionOptions = this.state.filterDimension.filter(item => {
            let swichValue = ''
            if (value[0] === '到期收益率') {
                swichValue = '到期收益率'
            } else if (value[0] === '宏观经济') {
                swichValue = '通货膨胀'
            } else if (value[0] === '通货膨胀') {
                swichValue = '宏观经济'
            } else if (value[0] === '货币供应') {
                swichValue = '实体融资需求'
            } else if (value[0] === '实体融资需求') {
                swichValue = '货币供应'
            }
            return item.value === swichValue
        })
        await this.setState({dimensionOptions: dimenssionOptions})
        if (!this.state.filterState) {
            await this.setState({dimensionOptions: dimenssionOptions})
        }

        if (lastDimession === dimension) {
            message.info('选择的指标不能同前面指标一致')
            return
        }

        await this.setState({dimensionLatest: lastDimession, filterState: false})
        // await this._bondMarket()
    }

    // 债券
    createParamThree () {
        return (
            <div className="float_right">
                    <div id={`cascader_container_scene`} style={{float: 'left', marginRight: '10px'}}>
                        <span className={`ml_20`}>切分维度1：</span>
                        <Cascader allowClear={false} options={this.state.dimensionOptions} onChange={this.dimessionChange.bind(this)} expandTrigger="hover" placeholder={`长债收益率`} className={`select_cascader_wrapper`} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container_scene')} />
                    </div>

                    <div id={`cascader_container_option`} style={{float: 'left'}}>
                        <span className={`ml_20`}>切分维度2：</span>
                        <Cascader allowClear={false} style={{width: '140px'}} options={this.state.lastDimensionOptions} onChange={this.dimessionLatest.bind(this)} expandTrigger="hover" placeholder={`期限利差`} className={`select_cascader_wrapper`} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container_option')} />
                    </div>
                    <span>统计日期 : </span>
                    <DatePicker locale={locale} onChange={this.startDateBond.bind(this)} placeholder={'开始日期'} />
                    <strong className={`split_span_date`}>~</strong>
                    <DatePicker locale={locale} onChange={this.endDateBond.bind(this)} placeholder={'结束日期'} />
            </div>
        )
    }
    // 期货选择指数类型
    async _futureDev (value) {
        const futureDev = value[value.length - 1]
        if (futureDev) {
            await this.setState({futureDev})
        }
        // await this._futureMarket()
    }
    // 期货
    createParamFour () {
        return (
            <div className="float_right">
                    <div id={`cascader_container_future`} style={{display: 'inline-block', marginRight: '10px'}}>
                        <Cascader options={futureOptions} onChange={this._futureDev.bind(this)} expandTrigger="hover" placeholder={`南华商品指数`} className={`select_cascader_wrapper`} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container_future')} />
                    </div>
                    <Select style={{marginRight:'10px'}} defaultValue={this.state.futureDayFutures} onChange={this.futureDayFutures.bind(this)}>
                        <Option value="scenario_future_30">30天</Option>
                        <Option value="scenario_future_60">60天</Option>
                        <Option value="scenario_future_90">90天</Option>
                        <Option value="scenario_future_180">180天</Option>
                        <Option value="scenario_future_360">360天</Option>
                    </Select>
                    <span>波动率    </span>
                    <Input placeholder={`请输入查询或权限描述`} style={{ width: '200px', marginLeft: '20px' }} onBlur={this.nQuantileFutures.bind(this)} />
                    {/* <Select  defaultValue="lucy" onChange={this.nQuantileFutures.bind(this)}>
                        <Option value="lucy">沪深300指数</Option>
                    </Select> */}
                    <span style={{marginRight:'10px'}}>%分位</span>
                    <span>统计日期 : </span>
                    <DatePicker locale={locale} onChange={this.startDateFutures.bind(this)} placeholder={'开始日期'} />
                    <strong className={`split_span_date`}>~</strong>
                    <DatePicker locale={locale} onChange={this.endDateFutures.bind(this)} placeholder={'结束日期'} />
            </div>
        )
    }

    // 业绩基准更改
    valueChange (value) {
        const val = value[value.length -1]
        this.setState({cascaderSelect: value})
        console.log(val)
        // const param = { ...this.props.specialParams, compareRule: val}

    }
    // 特殊场景更改
    _sceneChange (e) {
        this.setState({sceneSelect: e})
        if (this.props._specialList) {
            const param = {sceneIds: e.join(',')}
            this.props._specialList(param)
        }
    }
    // 业绩基准数据请求
    async _cascaderTags () {
        const data = await axios({
            url: '/hsApi/hszcpz/product/findAllProduct',
            method: 'POST',
            data: JSON.stringify({
                token: 'a69b7e63-79f3-403d-8dc0-201070625fd4'
            })
        })
        console.log(data, '-------------------------')
        message.destroy()

        this.setState({loading: false})

        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result = data

        if (result.result === 'success') {
            const fetchData = result.product

            let map = {}, dest = []
            fetchData.forEach(item => {
                if (!map[item.parent_name]) {
                    let children = []
                    item.contrastProduct.forEach(item => {
                        children.push({
                            value: item.benchmark_id,
                            label: item.benchmark_name
                        })
                    })
                    dest.push({
                        value: item.parent_name,
                        label: item.parent_name,
                        children: children
                    })

                    map[item.parent_name] = item.parent_name
                }
            })
            return dest

        } else {
            message.error('获取数据失败')
        }
    }
    // 特殊场景数据请求
    async _sceneList (params) {
        const param = {token: 'a69b7e63-79f3-403d-8dc0-201070625fd4', ...params}
        const data = await _getSceneList(param)

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }

        let result = data.result

        if (data.status === 'success') {
            return result
        }

    }
    // 特殊场景获取之后初始化
    async _initSceneList (param) {
        const data = await this._sceneList(param)
        const dataList = []
        if (data) {
            data.forEach(item => {
                dataList.push({value: item.c_scene_id, label: item.c_scene_name})
            })
        }

        this.setState({sceneList: dataList})
    }

    // 特殊场景分析
    createParamFive () {
        return (
            <div style={{margin:'20px 20px -10px',height:'40px'}}>
                <div className="float_right">
                <Button type="primary" style={{marginRight:'10px'}}>
                    <PlusOutlined />
                    添加对比产品
                </Button>
                <Button type="primary">重置</Button>
                </div>
                <div >
                    <div className={'float_left'} id={`scene_list_wrapper`}>
                        <span>特殊情景：</span>
                        <Select getPopupContainer={() => document.getElementById('scene_list_wrapper')} style={{width: '140px'}} showArrow={true} maxTagCount={0} value={this.state.sceneSelect} mode={`multiple`} placeholder='市场趋势场景' onChange={this._sceneChange.bind(this)}>
                            {this.state.sceneList ?
                                this.state.sceneList.map((item, index) => <Option key={index} value={item.value}>{item.label}</Option>): <Option key={0} value={'市场趋势场景'}>市场趋势场景</Option>}
                        </Select>
                    </div>
                    <div id='cascader_container_scene' className={'float_left left_common_header_content ml_10'}>
                        <span>业绩基准：</span>
                        <Cascader allowClear={false} value={this.state.cascaderSelect} onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container_scene')} className={`select_cascader_wrapper scene_cascader_container`} options={this.state.cascaderOptions} placeholder={'选择比较基准'} expandTrigger="hover" />
                    </div>
                    <span>净值类型 : </span>
                    <Select style={{marginRight:'10px'}} defaultValue="lucy" onChange={this.confLevelspecial.bind(this)}>
                        <Option value="lucy">复权累计净值</Option>
                    </Select>
                    <DatePicker locale={locale} onChange={this.startDatespecial.bind(this)} placeholder={'开始日期'} />
                    <strong className={`split_span_date`}>~</strong>
                    <DatePicker locale={locale} onChange={this.endDatespecial.bind(this)} placeholder={'结束日期'} />
                </div>

            </div>

        )
    }

    // 场景分析，股票多头调整
    async _getSocketChart (params) {
      const param = {...this.params, ...params}
      this.setState({params: param, chartOneLoading: true}, async () => {
          const data = await _getSocketMarket(params)
          console.log('股票多头', data)
          if (data && data.status === 'success' && data.result) {
              const navType = {n_swanav: '复权累计净值', n_added_nav: '累计净值', n_nav: '单位净值'}
              let lineOption = JSON.parse(JSON.stringify(linesOptions))
              let optionSeries = []
              let seriesData = []
              let areaMarket = []
              let markColor = []
              lineOption.yAxis[0].name = navType[this.state.navType]
              lineOption.legend.data = [query.name]
              lineOption.grid.bottom = '16%'
              lineOption.grid.top = '46px'
              lineOption.dataZoom[0].bottom = '28px'
              lineOption.yAxis[0].axisTick = { show: false }
              lineOption.yAxis[0].splitLine = {
                  lineStyle: {
                      type: 'dashed',
                      color: "#ddd",
                  }

              }

              data.result['figure']['scenario_data'].forEach((item, index) => {
                  areaMarket.push(Object.keys(item).toString())
              })

              areaMarket.forEach((item, index) => {
                  let colorItem = ''

                  if (data.result['figure']['scenario_data'][index][item] === 'blue') {
                      colorItem = 'rgba(183, 227, 255, 0.4)'
                  } else if (data.result['figure']['scenario_data'][index][item] === 'red') {
                      colorItem = 'rgba(255, 201, 201, 0.4)'
                  } else if (data.result['figure']['scenario_data'][index][item] === 'green') {
                      // colorItem =  'rgba(188, 255, 183, 0.4)'
                      colorItem = 'rgba(188, 255, 183, 0.4)'
                  } else {
                      colorItem = 'rgba(255, 237, 183, 0.4)'
                  }
                  if (index === 0) {
                      markColor.push([{}, {
                          xAxis: item.split('|')[1],
                          itemStyle: {color: colorItem}
                      }])
                  } else if (index === areaMarket.length -1) {
                      markColor.push([{
                          xAxis: item.split('|')[0],
                          itemStyle: {color: colorItem}
                      }, {}])
                  } else {
                      markColor.push([{
                          xAxis: item.split('|')[0],
                          itemStyle: {color: colorItem}
                      }, {
                          xAxis: item.split('|')[1]
                      }])
                  }

              })
              data.result.figure.fund_data.map(item => seriesData.push(item))
              lineOption.xAxis[0].data = data.result.figure.x_axis
              optionSeries = [{
                  name: query.name,
                  type: 'line',
                  smooth: true,
                  symbol: 'none',
                  markArea: {
                      data: markColor
                  },
                  itemStyle: {
                      normal: { color: chartsColors[0] }
                  },
                  data: seriesData
              }]

              let min = data.result.figure.fund_data.sort((a, b) => a-b)[0]
              let max = data.result.figure.fund_data.sort((a, b) => b - a)[0]
              lineOption.yAxis[0].min = (min - 0.01).toFixed(2)
              lineOption.yAxis[0].max = (max + 0.01).toFixed(2)
              lineOption.yAxis[0].interval = (max - parseFloat(min - 0.02).toFixed(2)) / 5
              lineOption.yAxis[0].axisLabel = {
                  color: '#666',
                  fontSize: 12,
                  formatter: (e) => parseFloat(e).toFixed(2)
              }

              lineOption.series = optionSeries
              this.refs.ChartOne.setOpitions(lineOption)

              if (data.result.table) {
                  let dataKey = Object.keys(data.result.table)
                  let dataList = []
                  console.log(data)
                  dataKey.forEach((item, index) => {
                      if (item !== 'n_count') {
                          dataList.push({
                              key: index,
                              '指标': columnsText[item].name,
                              '市场上涨': data.result.table[item]['市场上涨'],
                              '市场震荡': data.result.table[item]['震荡'],
                              '市场下跌': data.result.table[item]['市场下跌']
                          })
                      } else {
                          dataList.unshift({
                              key: index,
                              '指标': columnsText[item].name,
                              '市场上涨': data.result.table[item]['市场上涨'],
                              '市场震荡': data.result.table[item]['震荡'],
                              '市场下跌': data.result.table[item]['市场下跌']
                          })
                      }
                  })

                  if (dataList.length > 0) {
                      this.setState({dataSourceOne: dataList})
                  }
              }
          }
          this.setState({chartOneLoading: false})
      })
    }

    // 场景分析，雷达图radar 图表接口
    async _getRadar (params) {
        const data = await _getRadar(params)
        let radarOptions = JSON.parse(JSON.stringify(radarOption))

        if (data && data.result) {
            let series = []
            radarOptions.radar.startAngle = 90
            radarOptions.radar.scale = false
            radarOptions.radar.indicator = [
                {name: '常数项'},
                {name: '上证50涨跌幅'},
                {name: '中证500涨跌幅'},
                {name: '大小市值涨跌幅之差'},
                {name: '上证指数收益率波动率（周度波动率）'},
                {name: '上证指数流动性（平均成交量）'},
            ]
            series.push({
                type: 'radar',
                itemStyle: {
                    normal: { color: chartsColors[0] }
                },
                startAngle: 95,
                symbolSize: 10,
                lineStyle: {normal: { color: chartsColors[0] }},
                areaStyle: {color: areaColors[0] , normal: {color: areaColors[0]}},
                data: [{
                    value: data.result
                }]
            })

            radarOptions.series = series
            radarOptions.tooltip.formatter = (value, ticket) => {
                let itemDesc = ``
                value.value.forEach((item, i) => {
                    itemDesc += `${value.marker}${radarOptions.radar.indicator[i].name}：${parseFloat(item).toFixed(2)}<br />`
                })
                return itemDesc
            }
            this.refs.ChartTwo.setOpitions(radarOptions)
        }

    }

    // 场景分析，市场分阶段表现-图-表
    async _getMarketChart (params) {
        params = { ...this.state.marketParams, ...params }
        this.setState({ marketParams: params }, async () => {
            const data = await _sceneMarketNeutral(params)

            let neutralDate = params.dateRange.split(':')
            let { dimension } = params
            let rateData = null
            let lineData = null
            let series = []
            let neutralOptions = JSON.parse(JSON.stringify(barsOptions))
            if (data && data.result && data.status === 'success') {
                this.refs.marketTable._initData(data.result, dimension)
                neutralOptions.legend = {
                    x: 'center',
                    bottom: '0',
                    textStyle: {
                        color: '#90979c',
                    },
                    data: ['年化周收益率', '胜率']
                }
                neutralOptions.yAxis[0].axisTick = {show: false}
                neutralOptions.yAxis[0].splitLine = {
                    lineStyle: {
                        type: 'dashed',
                        color: '#ccc'
                    }
                }

                neutralOptions.tooltip.formatter = (value) => {
                    let itemDesc = `${neutralDate[0]}~${neutralDate[1]}<br />`
                    value.forEach((item, index) => {
                        if (index < value.length -1) {
                            itemDesc += `${item.marker}${item.seriesName}：${parseFloat(item.value).toFixed(2)}%<br />`
                        } else {
                            itemDesc += `${item.marker}${item.seriesName}：${parseFloat(item.value).toFixed(2)}<br />`
                        }
                    })

                    return itemDesc
                }
                neutralOptions.title = {}
                neutralOptions.yAxis[0].name = ''

                neutralOptions.yAxis.push({

                    min: 0,
                    max: 1,
                    splitNumber: 1 / 4,
                    nameLocation: 'end',
                    axisTick: {show: false},
                    nameTextStyle: {
                        color: '#999',
                        align: 'left'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: false,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999'
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#666'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dashed',
                            color: '#ccc'
                        }
                    }
                })

                rateData = data.result.n_return_a
                lineData = data.result.w_rate

                if (rateData && lineData) {
                    let keys = Object.keys(rateData)

                    if (keys.length > 3) {
                        const sortKey = []
                        keys.forEach((item, index) => {
                            switch (item) {
                                case '<5':
                                    sortKey[0] = item
                                    break
                                case '5-10':
                                    sortKey[1] = item
                                    break
                                case '10-15':
                                    sortKey[2] = item
                                    break
                                case '15-20':
                                    sortKey[3] = item
                                    break
                                case '>20':
                                    sortKey[4] = item
                                    break
                                default:
                                    break
                            }
                        })
                        keys = sortKey
                        neutralOptions.xAxis= [{data: keys}]
                    } else {
                        const midKey = []
                        keys.forEach((item, index) => {
                            switch (item) {
                                case '高':
                                    midKey[0] = '极大值'
                                    break
                                case '中':
                                    midKey[1] = '正常'
                                    break
                                case '低':
                                    midKey[2] = '极小值'
                                    break
                                default:
                                    break
                            }
                        })

                        neutralOptions.xAxis= [{data: midKey}]
                    }

                    neutralOptions.xAxis[0].offset = 0
                    neutralOptions.xAxis[0].axisLine = {
                        onZero: false,
                        show: true,
                        lineStyle: {
                            color: '#ccc'
                        }
                    }

                    neutralOptions.xAxis[0].axisLabel = {
                        margin: 10,
                        color: '#999',
                        show: true
                    }

                    neutralOptions.xAxis[0].axisTick = {
                        show: true,
                        position: 'bottom',
                        inside: false
                    }
                    let rateList = [], barList = []
                    keys.forEach((item, index) => {
                        rateList.push(rateData[item])
                        barList.push(lineData[item])
                    })

                    series.push({
                        name: '年化周收益率',
                        type: 'line',
                        smooth: true,
                        symbol: 'none',
                        data: rateList,
                        itemStyle: {
                            normal: { color: chartsColors[0] }
                        }
                    })

                    series.push({
                        name: '胜率',
                        type: 'bar',
                        barWidth: 30,
                        yAxisIndex: 1,
                        smooth: true,
                        data: barList,
                        itemStyle: {
                            normal: { barBorderRadius: 5, color: chartsColors[4]}
                        }
                    })

                }
                neutralOptions.series = series

                this.refs.ChartThree.setOpitions(neutralOptions)

            }
        })
    }

    // 场景分析，债券模块-市场分阶段表现
    async _getBondChart (params) {
        const data = await _getBondsMarket(params)
        const navType = {n_swanav: '复权累计净值', n_added_nav: '累计净值', n_nav: '单位净值'}
        let bondOption = JSON.parse(JSON.stringify(linesOptions)), optionSeries = [], areaMarket = [],  markColor = []
        bondOption.yAxis[0].axisTick = { show: false }
        bondOption.yAxis[0].name = navType[params.navType]
        // bondOption.legend.data = [query.name]
        bondOption.grid.bottom = '16%'
        bondOption.grid.top = '46px'
        bondOption.dataZoom[0].bottom = '28px'
        bondOption.yAxis[0].splitLine = {
            lineStyle: {
                type: 'dashed',
                color: "#ddd",
            }

        }

        if (data && data.result && data.status === 'success') {
            data.result.figure['scenario_data'].forEach((item, index) => {
                areaMarket.push(Object.keys(item).toString())
            })

            areaMarket.forEach((item, index) => {
                let colorItem = ''

                if (data.result.figure['scenario_data'][index][item] === '1') {
                    colorItem = 'rgba(255, 201, 201, 0.4)'
                } else if (data.result.figure['scenario_data'][index][item] === '2') {
                    colorItem = 'rgba(255, 237, 183, 0.4)'
                } else if (data.result.figure['scenario_data'][index][item] === '3') {
                    colorItem = 'rgba(183, 227, 255, 0.4)'
                } else if (data.result.figure['scenario_data'][index][item] === '4') {
                    colorItem =  'rgba(188, 255, 183, 0.4)'
                }
                if (index === 0) {
                    markColor.push([{}, {
                        xAxis: areaMarket[index + 1].split('|')[0],
                        itemStyle: {color: colorItem}
                    }])
                } else if (index <= areaMarket.length - 2) {
                    markColor.push([{
                        xAxis: item.split('|')[0],
                        itemStyle: {color: colorItem}
                    }, {
                        xAxis: areaMarket[index + 1].split('|')[0],
                        itemStyle: {color: colorItem}
                    }])
                } else if (index === areaMarket.length -1) {
                    markColor.push([{
                        xAxis: item.split('|')[0],
                        itemStyle: {color: colorItem}
                    }, {}])
                }
            })
            let seriesArr = []
            data.result.figure.fund_data.forEach(item => seriesArr.push(item))
            bondOption.xAxis[0].data = data.result.figure.x_axis
            optionSeries = [{
                name: query.name,
                type: 'line',
                smooth: true,
                symbol: 'none',
                markArea: {
                    data: markColor
                },
                itemStyle: {
                    normal: { color: chartsColors[0] }
                },
                data: seriesArr
            }]
            bondOption.series = optionSeries
            let min = data.result.figure.fund_data.sort((a, b) => a-b)[0]
            let max = data.result.figure.fund_data.sort((a, b) => b - a)[0]
            bondOption.yAxis[0].min = (min - 0.01).toFixed(2)
            bondOption.yAxis[0].max = (max + 0.01).toFixed(2)
            bondOption.yAxis[0].interval = (max - parseFloat(min - 0.02).toFixed(2)) / 5
            bondOption.yAxis[0].axisLabel = {
                color: '#666',
                fontSize: 12,
                formatter: (e) => parseFloat(e).toFixed(2)
            }
            if(data.result.table) {
                this.refs.bondsTable._initData(data.result.table, params.dimension1, params.dimension2)
            }

        }

        this.refs.ChartFour.setOpitions(bondOption)
    }

    // 场景分析，期货模块-市场分阶段表现
    async _getFutureChart (params) {
        params = { ...this.state.futureParams, ...params}
        this.setState({futureParams: params}, async () => {
            const data = await _getFutureMarket(params)
            console.log('期货模块-市场分阶段表现', data)
            if (data && data.result && data.status === 'success') {
                const navType = {n_swanav: '复权累计净值', n_added_nav: '累计净值', n_nav: '单位净值'}
                let futureOption = JSON.parse(JSON.stringify(linesOptions))
                let optionSeries = []
                let areaMarket = []
                let markColor = []

                // futureOption.legend.data = [query.name]
                futureOption.grid.bottom = '16%'
                futureOption.grid.top = '46px'
                futureOption.dataZoom[0].bottom = '28px'
                futureOption.yAxis[0].name = navType[params.navType]
                futureOption.yAxis[0].axisTick = { show: false }
                futureOption.yAxis[0].splitLine = {
                    lineStyle: {
                        type: 'dashed',
                        color: "#ddd",
                    }

                }


                data.result.figure['scenario_data'].forEach((item, index) => {
                    areaMarket.push(Object.keys(item).toString())
                })
                areaMarket.forEach((item, index) => {
                    let colorItem = ''

                    if (data.result.figure['scenario_data'][index][item] === 'high') {
                        colorItem = 'rgba(255, 201, 201, 0.4)'
                    } else if (data.result.figure['scenario_data'][index][item] === 'mid') {
                        colorItem = 'rgba(183, 227, 255, 0.4)'
                    } else if (data.result.figure['scenario_data'][index][item] === 'low') {
                        colorItem = 'rgba(188, 255, 183, 0.4)'
                        // colorItem =  'rgba(255, 237, 183, 0.4)'
                    } else {
                        colorItem =  'rgba(188, 255, 183, 0.4)'
                    }

                    if (index === 0) {
                        markColor.push([{}, {
                            xAxis: item.split('|')[1],
                            itemStyle: {color: colorItem}
                        }])
                    } else if (index <= areaMarket.length - 2) {
                        markColor.push([{
                            xAxis: item.split('|')[0],
                            itemStyle: {color: colorItem}
                        }, {
                            xAxis: item.split('|')[1],
                            itemStyle: {color: colorItem}
                        }])
                    } else if (index === areaMarket.length -1) {
                        markColor.push([{
                            xAxis: item.split('|')[0],
                            itemStyle: {color: colorItem}
                        }, {}])
                    }

                })

                futureOption.xAxis[0].data = data.result.figure.x_axis
                let seriesArr = []
                data.result.figure.fund_data.map(item => seriesArr.push(item))
                optionSeries = [{
                    name: query.name,
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    markArea: {
                        data: markColor
                    },
                    itemStyle: {
                        normal: { color: chartsColors[0] }
                    },
                    data: seriesArr
                }]
                let min = data.result.figure.fund_data.sort((a, b) => a-b)[0]
                let max = data.result.figure.fund_data.sort((a, b) => b - a)[0]
                futureOption.yAxis[0].min = (min - 0.01).toFixed(2)
                futureOption.yAxis[0].max = (max + 0.01).toFixed(2)
                futureOption.yAxis[0].interval = (max - parseFloat(min - 0.02).toFixed(2)) / 5
                futureOption.yAxis[0].axisLabel = {
                    color: '#666',
                    fontSize: 12,
                    formatter: (e) => parseFloat(e).toFixed(2)
                }
                futureOption.series = optionSeries
                this.refs.ChartFive.setOpitions(futureOption)
                if (data.result.table) {
                    this.refs.futureTable._initData(data.result.table)
                }
            }
        })
    }

    // 场景分析，特殊场景收益走势图和回撤走势图
    async _getSpecialScene (params) {
      params = { ...this.specialParams, ...params }
      this.setState({specialParams: params}, async () => {
          const data = await _getSpecialLine(params)
          let navOptions = JSON.parse(JSON.stringify(linesOptions)), downOptions = JSON.parse(JSON.stringify(areasOptions))
          let xData = []
          let proLineName = []
          let proAreaName = []
          let linesSeries = []
          let areaSeries = []
          console.log(data, '-----特殊场景分析')
          if (data && data.result && data.status === 'success') {
              let keys = data.result.hasOwnProperty('name_list') ? data.result.name_list : []
              xData = data.result.hasOwnProperty('figure') ? data.result.figure.x_axis : []
              keys.forEach((item, index) => {
                  proLineName.push(item)
                  proAreaName.push(item)
                  let datas = [], areaDatas = []
                  data.result.figure.y_axis.return_fig[item].forEach(item => {
                      if (item !=='') {
                          datas.push(isNaN(item) ? item : parseFloat(item * 100).toFixed(2))
                      } else {
                          datas.push('--')
                      }
                  })
                  linesSeries.push({
                      name: item,
                      type: 'line',
                      symbol: false,
                      showSymbol: false,
                      symbolSize: 0,
                      lineStyle: {
                          normal: {
                              shadowColor: 'rgba(0, 0, 0, 0.2)',
                              shadowOffsetY: 2,
                              shadowOffsetX: 2,
                              shadowBlur: 5
                          }
                      },
                      itemStyle: {
                          normal: {
                              color: chartsColors[index],
                          }
                      },
                      data:  datas|| []
                  })

                  data.result.figure.y_axis.drawdown_fig[item].forEach(item => {
                      if (item !== '') {
                          areaDatas.push(isNaN(item) ? item : parseFloat(item * 100).toFixed(2))
                      } else {
                          areaDatas.push('--')
                      }

                  })

                  areaSeries.push({
                      name: item,
                      type: 'line',
                      symbol: false,
                      symbolSize: 0,
                      lineStyle: {
                          normal: {
                              shadowColor: 'rgba(0, 0, 0, 0.2)',
                              shadowOffsetY: 2,
                              shadowOffsetX: 2,
                              shadowBlur: 5
                          }
                      },
                      areaStyle: {
                          color: areaColors[index],
                          opacity: 1
                      },
                      itemStyle: {
                          normal: {
                              color: chartsColors[index],
                          }
                      },
                      data: areaDatas || []
                  })
              })

              downOptions.tooltip.formatter = (value) => {
                  let itemDesc = `${value[0].name}<br />`
                  value.forEach(item => {
                      itemDesc += `${item.marker}${item.seriesName}：${isNaN(item.value) ? '--' : parseFloat(item.value).toFixed(2)+'%'}<br />`
                  })

                  return itemDesc
              }


              navOptions.tooltip.formatter = (value) => {
                  let itemDesc = `${value[0].name}<br />`
                  value.forEach(item => {
                      itemDesc += `${item.marker}${item.seriesName}：${isNaN(item.value) ? '--' : parseFloat(item.value).toFixed(2)+'%'}<br />`
                  })
                  return itemDesc
              }

              downOptions.xAxis[0].data = xData
              downOptions.series = areaSeries
              downOptions.legend.data = proAreaName
              navOptions.xAxis[0].data = xData
              navOptions.series = linesSeries
              navOptions.legend.data = proLineName

              downOptions.dataZoom[0].width = 0
              downOptions.dataZoom[0].height = 0
              downOptions.dataZoom[1] = {width: 0, height: 0}

          }
            console.log(navOptions, '----特殊场景---单位净值')
            this.refs.ChartSix.setOpitions(navOptions)
            this.refs.ChartSeven.setOpitions(downOptions)
              const Chart_rate = document.getElementById('ChartSix')
              const Chart_area = document.getElementById('ChartSeven')
              const areaContext = echarts.init(Chart_area)
              const rateContext = echarts.init(Chart_rate)
              rateContext.on('dataZoom', (e) => {
                  // console.log(e)
                  const batch = e.batch
                  if (batch) {
                      batch.forEach((item, index) => {
                          areaContext.dispatchAction({
                              type: 'dataZoom',
                              dataZoomIndex: index,
                              start: item.start,
                              end: item.end
                          })
                      })
                  } else {
                      areaContext.dispatchAction({
                          type: 'dataZoom',
                          start: e.start,
                          end: e.end
                      })
                  }

              })

      })
    }

    // 场景分析，风险收益指标
    async _getSpecialRisk (params) {
        const data = await _getSpecialRisk(params)
        if (data) {
            return data
        }
    }

    async componentDidMount() {
        const data = await this._cascaderTags()
        console.log(data, 'cascaderOptions')
        await this.setState({cascaderOptions: data})
        await this.setState({ fundId: query.id , chartOneLoading: true})
        await this._initSceneList({dateRange: '2018-05-11:2020-05-11'})
        //场景分析，股票多头
        await this._getSocketChart({
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId: query.id,
            indexName: 'c_hs300_trend',
            navType: 'n_swanav',
            confLevel: '0.90',
            dateRange: '2015-04-01:2020-04-19',
            Rfann: '0.025'
        })
        // 雷达图
        await this._getRadar({token: 'a69b7e63-79f3-403d-8dc0-201070625fd4', fundId: query.id,
            navType: 'n_swanav',
            dateRange: '2015-04-01:2020-04-19'})

        // 市场分阶段表现-图表
        await this._getMarketChart({
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId: query.id,
            navType: 'n_swanav',
            dateRange: '2015-04-01:2020-04-19',
            dimension: 'n_basis_cost'
        })

        // 债券模块-市场分阶段表现
        await this._getBondChart({
                token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
                fundId: query.id,
                indexName: 'c_hs300_trend',
                navType: 'n_swanav',
                confLevel: '0.90',
                dateRange: '2015-04-01:2020-04-19',
                dimension1: 'n_long_yield',
                dimension2: 'n_term_spread',
                Rfann: '0.025'
        })

        // 期货模块-市场分阶段表现
        await this._getFutureChart({
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId: query.id,
            indexName: 'c_hs300_trend',
            navType: 'n_swanav',
            confLevel: '0.90',
            dateRange: '2015-04-01:2020-04-19',
            Rfann: '0.025',
            futureDev: 'n_nhci_volatility',
            nQuantile: '0.2',
            futureDay: 'scenario_future_30'
        })

        // 特殊场景-收益和回撤走势图
        await this._getSpecialScene({
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundIds: query.id,
            startLine: 1,
            navType: 'n_added_nav',
            statPeriod: 'year',
            compareRule: '000300.SH',
        })

        this.refs.riskTable._initData({
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundIds: query.id,
            statPeriod: 'year',
            startLine: '1',
            compareRule: '000300.SH',
            dateRange: '',
            fundNames: ['汇鸿汇升星汇旗舰私募证券投资基金'],
            navType: 'n_added_nav',
            noRiskReturnRate: '2.5',
        })

    }

    // 市场阶段表现 切分维度选择
    async _neutralSelection (value) {
        value = value[value.length - 1]
        await this.setState({neutralType: value})
        let params = {
            token: 'a69b7e63-79f3-403d-8dc0-201070625fd4',
            fundId: query.id,
            navType: this.state.navType,
            dateRange: this.state.neutralDate,
            // dimension: 'n_csi500_change'
            dimension: value
        }
        // await this._neutralChart(params)
    }

    // 设置 债券市场 阶段表现联动table返回
    displayRender (label) {
        return label[label.length - 1]
    }

    render() {

        return (
            <div>
                <div style={{backgroundColor:'#fff',height:'70px',paddingTop:'20px',marginBottom:'-22px'}}>
                    <div style={{float:'right'}}>
                        <span>净值类型 : </span>
                        <Select className={`mr_10`} defaultValue={this.state.navType} onChange={this.getNavType.bind(this)}>
                            <Option value="n_swanav">复权累计净值</Option>
                            <Option value="n_added_nav">累计净值</Option>
                            <Option value="n_nav">单位净值</Option>
                        </Select>
                        <Button style={{margin:'0px 40px 10px 0px'}} type="primary">导出</Button>
                    </div>
                </div>

                <div className={detailsStyle.positions_info} style={{borderTop:'1px solid #ccc'}}>
                    <div>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>市场分析表现--股票多头</div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`市场分阶段表现`} children={this.createParamOne()} />
                        <Spin spinning={this.state.chartOneLoading} tip={`Loading...`}>
                            <CommonCharts idName='ChartOne' ref='ChartOne' styles={{width: '100%', height: '500px'}} />
                        </Spin>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} `} styles={{width: '100%', height: '60px'}}>
                        <div style={{display:'flex',justifyContent:'center'}}>
                            <div style={{marginRight:'30px'}}>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#FFEAE9',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>市场上涨</span>
                            </div>
                            <div style={{marginRight:'30px'}}>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E2F4FF',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>市场震荡</span>
                            </div>
                            <div>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E5FFE2',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>市场下跌</span>
                            </div>
                        </div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} `} styles={{width: '100%', height: '60px',paddingTop:'20px'}}>
                        <div style={{float:'right'}}>
                            <span>置信水平 : </span>
                            <Select defaultValue={this.state.confLevelStock} style={{width: 120}} onChange={this.confLevelStock.bind(this)}>
                                <Option value="0.90">90%</Option>
                                <Option value="0.95">95%</Option>
                                <Option value="0.99">99%</Option>
                            </Select>
                        </div>
                    </div>
                    <div  className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <Spin spinning={this.state.chartOneLoading} tip={`Loading...`}>
                            <Table className={`${detailsStyle.stockTable}`} size='middle' columns={this.state.columnsOne} dataSource={this.state.dataSourceOne} pagination={false} bordered/>
                        </Spin>
                    </div>

                </div>

                <div className={detailsStyle.positions_info}>
                    <div>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>市场分阶段表现--市场中性</div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`影响净值的关键市场变量`}  children={this.createParamTwo()}/>
                        <CommonCharts idName='ChartTwo' ref='ChartTwo' styles={{width: '100%', height: '500px'}} />
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`市场分阶段表现`} children={this.createParamTwo2()}/>
                        <div className={`clear-fix`}>
                            <div className={`float_left ${HomeStyle.home_chart_left}`} style={{paddingTop:'60px'}}>
                                {/* <h3 className={`${HomeStyle.home_chart_tit}`}></h3> */}
                                <div style={{float:'right',marginRight:'20px',marginTop:'-40px'}} id={`cascader_container_neutral`}>
                                    <span>切分维度 : </span>
                                    <Cascader allowClear={false} options={neutralOptions} onChange={this._neutralSelection.bind(this)} expandTrigger="hover" placeholder={`年化基差成本`} className={`select_cascader_wrapper`} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container_neutral')} />
                                </div>
                                <div className={`${HomeStyle.home_chart_split}`}>
                                    <CommonCharts idName={`ChartThree`} ref={`ChartThree`} styles={{width: '100%', height: '400px'}} />
                                </div>
                            </div>
                            <div className={`float_right ${HomeStyle.home_chart_right}`} style={{paddingTop:'60px'}}>
                                {/* <h3 className={`${HomeStyle.home_chart_tit}`}></h3> */}
                                <MarketBasis ref={`marketTable`} />
                            </div>
                        </div>
                    </div>

                </div>
                <div className={detailsStyle.positions_info}>

                    <div>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>市场分阶段表现--债券</div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`市场分阶段表现`} children={this.createParamThree()} />
                        <CommonCharts idName='ChartFour' ref='ChartFour' styles={{width: '100%', height: '500px'}} />
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} `} styles={{width: '100%', height: '60px'}}>
                        <div style={{display:'flex',justifyContent:'center'}}>
                            <div style={{marginRight:'30px'}}>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#FFEAE9',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>长债收益率上行&期限利差扩大</span>
                            </div>
                            <div style={{marginRight:'30px'}}>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E2F4FF',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>长债收益率上行&期限利差收窄</span>
                            </div>
                            <div style={{marginRight:'30px'}}>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#FFF9E3',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>长债收益率下行&期限利差扩大</span>
                            </div>
                            <div>
                                <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E5FFE2',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                <span>长债收益率下行&期限利差收窄</span>
                            </div>

                        </div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} `} styles={{width: '100%', height: '60px',paddingTop:'20px'}}>
                        <div style={{float:'right'}}>
                            <span>置信水平 : </span>
                            <Select defaultValue={this.state.confLevelBond} style={{width: 120}} onChange={this.confLevelBond.bind(this)}>
                                <Option value="0.90">90%</Option>
                                <Option value="0.95">95%</Option>
                                <Option value="0.99">99%</Option>
                            </Select>
                        </div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <BondsTable ref={`bondsTable`} />
                    </div>

                </div>
                <div className={detailsStyle.positions_info}>

                    <div>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>市场分阶段表现--期货</div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`市场分阶段表现`} children={this.createParamFour()} />
                        <CommonCharts idName='ChartFive' ref='ChartFive' styles={{width: '100%', height: '500px'}} />
                        <div className={`${detailsStyle.charts_item_bg} `} styles={{width: '100%', height: '60px'}}>
                            <div style={{display:'flex',justifyContent:'center'}}>
                                <div style={{marginRight:'30px'}}>
                                    <span style={{display:'inline-block',width:'30px',height:'16px',background:'#FFEAE9',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                    <span>高波动</span>
                                </div>
                                <div style={{marginRight:'30px'}}>
                                    <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E2F4FF',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                    <span>震荡</span>
                                </div>
                                <div>
                                    <span style={{display:'inline-block',width:'30px',height:'16px',background:'#E5FFE2',marginRight:'10px',verticalAlign:'text-bottom'}}></span>
                                    <span>低波动</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`} styles={{width: '100%', height: '60px'}}>
                        <div style={{float:'right'}}>
                            <span>置信水平 : </span>
                            <Select defaultValue={this.state.confLevelFutures} style={{width: 120}} onChange={this.confLevelFutures.bind(this)}>
                                <Option value="0.90">90%</Option>
                                <Option value="0.95">95%</Option>
                                <Option value="0.99">99%</Option>
                            </Select>
                        </div>
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <FutureTable ref={`futureTable`} />
                    </div>

                </div>
                <div className={detailsStyle.positions_info}>

                    <div>
                        <div className={detailsStyle.hold_btn}><span className={'iconfont iconqushitu'}></span>特殊场景分析</div>
                    </div>
                    {this.createParamFive()}
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`单位净值走势图`} checkStatus={true} changeStatus={this.changeCheckStatus.bind(this)} />
                        <CommonCharts idName='ChartSix' ref='ChartSix' styles={{width: '100%', height: '600px'}} />
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`回撤走势图`} />
                        <CommonCharts idName='ChartSeven' ref='ChartSeven' styles={{width: '100%', height: '600px'}} />
                    </div>
                    <div className={`${detailsStyle.charts_item_bg} mt_20`}>
                        <HeaderTitle iconStatus={true} title={`收益-风险指标`} />
                        <RiskTableScences ref={`riskTable`} _initData={this._getSpecialRisk.bind(this)} />
                    </div>

                </div>
            </div>
        )
    }
    }
