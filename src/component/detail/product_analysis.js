import React, { Component } from 'react'
import {DatePicker, message, Spin} from 'antd'
import DetailsStyle from './detail.module.sass'
import HeaderTitle from '../common_title/common_header_title'
import NoneBorderTitle from '../common_title/none_border_header'
import CommonCharts from '../common_chart'
import WeekTables from '../common_table/details/week_table'
import AssetsTable from '../common_table/details/assets_table'
import TargetList from '../common_table/details/target_table'
import locale from 'antd/es/date-picker/locale/zh_CN'
import { _getWeekList, _getAssetsRateList, _getTargetList, _getSubProductNav,
    _getAssetsPercenter, _getSubChart, _getRateChart, _getSubReturn, _getSubRate } from '../../apis/details'
import url from 'url'
import moment from 'moment'
import {areaColors, areasOptions, barsOptions, chartsColors} from '../../Utils/charts_config/charts_config'
const { query } = url.parse(window.location.href, true)
export default class ProductAnalysis extends Component {
    constructor (props) {
        super(props)
        this.state = {
            navLoading: false,
            startDate: moment(new Date().getTime() - (365 * 24 * 60 * 60 * 1000)),
            endDate: moment(new Date()),
            percentLoading: false,
            advisorRateLoading: false,
            rateLoading: false,
            advisorLoading: false,
            checkDate: true
        }
        this.rateChartOption = {
            color: chartsColors,
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    return    params[0].name + "<br/>" + params[0].seriesName+':' + Number(params[0].value*100).toFixed(2) + '%' + '<br/>'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    name:'',
                    type: 'category',
                    data: [],
                    axisTick: {
                        alignWithLabel: true
                    },
                    axisPointer: {
                        type: 'shadow'
                    },
                    axisLabel:{
                         rotate: 60
                    },
                    axisLine: {
                        onZero: false
                    }
                }
            ],
            yAxis: [
                {
                    name: '收益率(%)',
                    type: 'value',
                    axisLine:{
                            show:false
                    },
                    splitLine:{show:true},

                }
            ],
            series: [
                {
                    name: '',
                    type: 'bar',
                    barWidth: '50%',
                    itemStyle: {

                    },
                    data: []
                }
            ]
        };
        this.assetChartOption = {
            color:chartsColors,
            tooltip: {
                trigger: 'item',
                formatter: '{b}: {c} ({d}%)'
            },

            series: [
                {

                    type: 'pie',
                    radius: ['50%', '70%'],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    labelLine: {
                        show: true
                    },
                    data: []
                }
            ]
        };
        this.subAdvsiorChartOption = {
            color: chartsColors,
            tooltip: {
                trigger: 'axis',
                formatter: function (params) {
                    return    params[0].name + "<br/>" + params[0].seriesName+':' + Number(params[0].value*100).toFixed(2) + '%' + '<br/>'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    start: 0,
                    end: 100
                }
            ],
            xAxis: [
                {
                    name:'',
                    type: 'category',
                    data: [],
                    axisTick: {
                        alignWithLabel: true
                    },
                    axisPointer: {
                        type: 'shadow'
                    },
                    axisLabel:{
                         rotate: 60
                    },
                    axisLine: {
                        onZero: false
                    }
                }
            ],
            yAxis: [
                {
                    name: '收益率(%)',
                    type: 'value',
                    axisLine:{
                            show:false
                    },
                    splitLine:{show:true},

                }
            ],
            series: [
                {
                    type: 'bar',
                    barWidth: '50%',
                    data: []
                }
            ]
        };
    }

    render () {
        return (<div className={`analysis_container`}>
                {/*  周收益  */}
                <div className={`container_bg_fff`}>
                    <div className={`${DetailsStyle.padding_header}`}>
                        <DatePicker locale={locale} placeholder={`开始日期`} value={this.state.startDate} onChange={this.changeStart.bind(this, 'start')} />
                        <span className={`split_span_date`}>~</span>
                        <DatePicker locale={locale} placeholder={`结束日期`} value={this.state.endDate} onChange={this.changeStart.bind(this, 'end')} />
                    </div>

                    <div className={`clear-fix ${DetailsStyle.week_rate_container}`}>
                        <div className={`float_left ${DetailsStyle.week_rate}`}>
                            <HeaderTitle iconStatus={true} title={`每周收益列表`} />
                            <WeekTables ref={`weekTable`} _initData={this._getWeek.bind(this)} />
                        </div>
                        <div className={`float_right ${DetailsStyle.week_nav}`}>
                            <HeaderTitle iconStatus={true} title={`母基金净值图`} />
                            <Spin spinning={this.state.navLoading} tip={`loading...`}>
                                <CommonCharts idName={`navParentChart`} ref={`navParentChart`} styles={{width: '100%', height: '460px'}} />
                            </Spin>
                        </div>
                    </div>
                </div>
                {/* 资产收益情况 */}
                <div className={`container_bg_fff mt_20 ${DetailsStyle.none_border_container}`}>
                    <NoneBorderTitle iconStatus={true } title={`资产收益情况`} />
                   <AssetsTable ref={`assetsTable`} _initData={this._getAssets.bind(this)} />
                </div>

                <div className={`mt_20`}>
                    <div className={`${DetailsStyle.split_middle_container}`}>
                        <div className={`container_bg_fff ${DetailsStyle.split_middle_left} mr_5`}>
                            <HeaderTitle iconStatus={true} title={`分类收益贡献`} />
                            <Spin spinning={this.state.rateLoading} tip={`Loading...`}>
                                <CommonCharts idName={`rateChart`} ref={`rateChart`} styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                        <div className={`container_bg_fff ${DetailsStyle.split_middle_right} ml_5`}>
                            <HeaderTitle iconStatus={true} title={`资产分配比例`} />
                            <Spin spinning={this.state.percentLoading} tip={`Loading...`}>
                                <CommonCharts idName={`assetChart`} ref={`assetChart`} styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                    </div>
                </div>

                <div className={`container_bg_fff mt_20 ${DetailsStyle.none_border_container}`}>
                   <NoneBorderTitle iconStatus={true } title={`产品指标清单`} />
                   <TargetList ref={`targetList`} _initData={this._getTargetList.bind(this)} />
                </div>

                <div className={`mt_20`}>
                    <div className={`${DetailsStyle.split_middle_container}`}>
                        <div className={`container_bg_fff ${DetailsStyle.split_middle_left} mr_5`}>
                            <HeaderTitle iconStatus={true} title={`子投顾累计收益率`} />
                            <Spin spinning={this.state.advisorLoading} tip={`Loading...`}>
                                <CommonCharts idName={`subAdvsiorChart`} ref={`subAdvsiorChart`} styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                        <div className={`container_bg_fff ${DetailsStyle.split_middle_right} ml_5`}>
                            <HeaderTitle iconStatus={true} title={`子投顾月/周收益`} />
                            <Spin spinning={this.state.advisorRateLoading} tip={`Loading...`}>
                                <CommonCharts idName={`advsiorRateChart`} ref={`advisorRateChart`} styles={{width: '100%', height: '500px'}} />
                            </Spin>
                        </div>
                    </div>
                </div>


                <div className={`container_bg_fff mt_20 ${DetailsStyle.week_rate_container}`}>
                    <HeaderTitle iconStatus={true} title={`子基金资产净值分布走势图`} />
                    <Spin spinning={this.state.subAssetsLoading} tip={`Loading...`}>
                        <CommonCharts idName={`subAssetsChart`} ref={`subAssetsChart`} styles={{width: '100%', height: '600px'}} />
                    </Spin>
                </div>

        </div>)
    }

    async componentDidMount () {
        await this.setState({navLoading: true, rateLoading: true})
        await this._getDataInt()
    }
    // 统一初始化
    async _getDataInt () {
        // 资产收益情况
        this.refs.assetsTable._initData({fund_id: query.id, t_date_start: moment(this.state.startDate).format('YYYY-MM-DD'), t_date_end: moment(this.state.endDate).format('YYYY-MM-DD')})
        //母基金净值图
        await this._getProductNav({fund_id: query.id, tDateStart: this.state.startDate, tDateEnd: this.state.endDate})
        // 每周收益列表
        this.refs.weekTable._initData({fund_id: query.id, tDateStart: this.state.startDate,  tDateEnd: this.state.endDate})
        // 产品指标清单
        this.refs.targetList._initData(({fund_id: query.id, t_date_start: moment(this.state.startDate).format('YYYY-MM-DD'), t_date_end: moment(this.state.endDate).format('YYYY-MM-DD')}))
        // 分类收益贡献
        await this._getrateChart({fund_id: query.id, t_date_start: moment(this.state.startDate).format('YYYY-MM-DD'), t_date_end: moment(this.state.endDate).format('YYYY-MM-DD')})
        // 子基金资产净值分布
        await this._getSubReturn({t_date_start: moment(this.state.startDate).format('YYYY-MM-DD'), t_date_end: moment(this.state.endDate).format('YYYY-MM-DD'), fund_id: query.id})
        // 子基金周/月净值
        await this._getSubProductRate({t_date_start: moment(this.state.startDate).format('YYYY-MM-DD'), t_date_end: moment(this.state.endDate).format('YYYY-MM-DD'), fund_id: query.id})
        // 子投顾累计收益率
        await this._getSubChart({t_date_start: this.state.startDate, t_date_end: this.state.endDate, fund_id: query.id})
        // 资产分配比例
        await this._getAssetsPercenter({t_date_start: this.state.startDate, t_date_end: this.state.endDate, fund_id: query.id})

    }
    // 头部时间选择
    changeStart (tag, mom, str) {
        if (tag === 'start') {
            if (str) {
                const currentVal = moment(moment(str).format('YYYY-MM-DD')).valueOf(), endDate = moment(moment(this.state.endDate).format('YYYY-MM-DD')).valueOf()
                if (endDate < currentVal) {
                    message.info('开始时间不能大于结束时间')
                    return
                }
                this.setState({startDate: moment(str)}, async () => {
                    if (endDate && this.state.checkDate) {
                        await this._getDataInt()
                    }
                })
            } else {
                this.setState({startDate: null})
            }
        } else {
            if (str) {
                const currentVal = moment(moment(str).format('YYYY-MM-DD')).valueOf(), startVal = moment(moment(this.state.startDate).format('YYYY-MM-DD')).valueOf()
                if (startVal > currentVal) {
                    message.info('开始时间不能大于结束时间')
                    return
                }
                this.setState({endDate: moment(str), checkDate: true}, async () => {
                    await this._getDataInt()
                })
            } else {
                this.setState({endDate: null})
            }
        }
    }
    // 周收益列表
    async _getWeek (params) {
        let param = { ...params}
        const data = await _getWeekList(param)
        if (data) {
            return data
        }
    }

    // 资产列表
    async _getAssets (params) {
        let param = { token: '123456', ...params}
        const data = await _getAssetsRateList(param)
        if (data) {
            return data
        }
    }

    // 产品指标清单
    async _getTargetList (params) {
        let param = { token: '123456', ...params}
        const data = await _getTargetList(param)
        if (data) {
            return data
        }
    }

    // 母基金净值图
    async _getProductNav (params) {
        this.setState({navLoading: true}, async () => {
            const data = await _getSubProductNav(params)
            if (data && data.status && data.result) {
                const result = data.result
                let productNavOption = JSON.parse(JSON.stringify(areasOptions))
                productNavOption.xAxis[0].data = result.figure[`x_axis`]
                productNavOption.legend.data = result[`name_list`]
                productNavOption.grid = {
                    top: '50px',
                    right: 70,
                    left: '46px',
                    bottom: 90,
                    y: 100
                }

                let yAxisName = []
                let seriesArr = []
                result[`name_list`].forEach((item, index) => {
                    let maxVal = result.figure[`y_axis`][item].sort((a, b) => b - a)[0]
                    if (item === '总市值') {
                        maxVal = Math.trunc(Number(maxVal / 10000) + 500)
                        maxVal = maxVal - (maxVal % 1000) + 500
                        yAxisName.push({
                            type: 'value',
                            name: item,
                            max: maxVal,
                            splitNumber: 15,
                            interval: maxVal / 5,
                            nameTextStyle: {
                                color: '#999'
                            },
                            position: 'left',
                            axisLine: {
                                show: false,
                                lineStyle: {
                                    color: '#ccc',
                                    type: 'dotted'
                                }
                            },
                            splitLine: {

                                lineStyle: {
                                    type: 'dotted',
                                    color: "#ccc",
                                }

                            },
                            axisLabel: {
                                color: '#666',
                                fontSize: 12,
                            }
                        })
                    } else {
                        maxVal = parseFloat(Number(maxVal) + 0.01).toFixed(2)
                        let minVal = result.figure[`y_axis`][item].sort((a, b) => a - b)[0]
                        const split = (maxVal - parseFloat(minVal - 0.01).toFixed(2)) / 5
                        yAxisName.push({
                            type: 'value',
                            name: item,
                            max: maxVal,
                            interval: split,
                            min: parseFloat(minVal - 0.01).toFixed(2),
                            nameTextStyle: {
                                color: '#999'
                            },
                            position: 'left',
                            axisLine: {
                                show: false,
                                lineStyle: {
                                    color: '#ccc',
                                    type: 'dotted'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    type: 'dotted',
                                    color: "#ccc",
                                }

                            },
                            axisLabel: {
                                color: '#666',
                                fontSize: 12,
                                formatter: (e) => parseFloat(e).toFixed(2)
                            }
                        })
                    }


                    if (item === '总市值') {
                        let totalVal = []
                        result.figure[`y_axis`][item].forEach(item => {
                            totalVal.push(parseFloat(item / 10000).toFixed(2))
                        })
                        result.figure[`y_axis`][item] = totalVal

                        seriesArr.push({
                            type: 'line',
                            name: item,
                            data: result.figure[`y_axis`][item],
                            symbol: 'none',
                            yAxisIndex: index,
                            lineStyle: {
                                normal: {
                                    shadowColor: 'rgba(0, 0, 0, 0.2)',
                                    shadowOffsetY: 2,
                                    shadowOffsetX: 2,
                                    shadowBlur: 5
                                }
                            },
                            areaStyle: {normal: {
                                    color: areaColors[index]
                                }},
                            itemStyle: {
                                normal: {
                                    color: chartsColors[index],
                                }
                            },
                        })
                    } else {
                        let totalVal = []
                        result.figure[`y_axis`][item].forEach(item => {
                            totalVal.push(parseFloat(item).toFixed(4))
                        })
                        result.figure[`y_axis`][item] = totalVal

                        seriesArr.push({
                            type: 'line',
                            name: item,
                            data: result.figure[`y_axis`][item],
                            symbol: 'none',
                            yAxisIndex: index,
                            lineStyle: {
                                normal: {
                                    shadowColor: 'rgba(0, 0, 0, 0.2)',
                                    shadowOffsetY: 2,
                                    shadowOffsetX: 2,
                                    shadowBlur: 5
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: chartsColors[index],
                                }
                            },
                        })
                    }

                })
                yAxisName[1].position = 'right'
                yAxisName[1].name = '总市值（万）'
                productNavOption.yAxis = yAxisName
                if (result.figure[`x_axis`].length > 0) {
                    productNavOption.series = seriesArr
                } else {
                    productNavOption.series = []
                }


                productNavOption.tooltip.formatter =   (value) => {

                    let itemDesc = `${value[0].name}<br />`
                    value.forEach(item => {
                        if (item.seriesName === '总市值') {
                            itemDesc += `${item.marker}${item.seriesName}：${item.value}万<br />`
                        } else {
                            itemDesc += `${item.marker}${item.seriesName}：${item.value}<br />`
                        }
                    })

                    return itemDesc
                }
                this.refs.navParentChart.setOpitions(productNavOption)
            }
            this.setState({navLoading: false})
        })
    }

    // 资产分配比例
    async _getAssetsPercenter (params) {
        this.setState({percentLoading: true}, async () => {
            const data = await _getAssetsPercenter(params)
            if (data && data.status && data.result) {
                this.assetChartOption.tooltip = {
                    formatter: (value) => {
                        let toolTip = ``
                        toolTip += "".concat(value.marker).concat(value.name, "\uFF1A").concat(value.percent, "%")
                        return toolTip
                    }
                }
                this.assetChartOption.series =[
                    {

                        type: 'pie',
                        radius: ['50%', '70%'],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        },
                        animationDelay: function (idx) {
                            return Math.random() * 500;
                        },
                        itemStyle: {
                            normal: {
                                shadowColor: 'rgba(0, 0, 0, 0.3)',
                                shadowBlur: 6,
                            }
                        },
                        labelLine: {
                            show: true,
                            normal: {
                                length: 30,
                                length2: 30,
                                lineStyle: {
                                    color: '#ccc'
                                }
                            }
                        },
                        label: {
                            show: true,
                            formatter: '{b}: {d}% ',
                            margin: 10,
                            padding: [6, 5, 6, 5],
                            textStyle: {
                                color: '#666',
                                fontSize: 13
                            }
                        },
                        data: data.result.map((item,index) => {
                            return {
                                value:item.proportion,
                                name:item.c_type_name
                            }
                        })
                    }
                ]

            } else {
                this.assetChartOption.series = []
            }
            this.refs.assetChart.setOpitions(this.assetChartOption)
            this.setState({percentLoading: false})
        })

    }

    //子投顾累计收益率
    async _getSubChart (params) {
        await this.setState({advisorLoading: true})
        const data = await _getSubChart(params)
        if (data && data.result) {
            let xName = [], seriesArr = []
            data.result.forEach(item => {
                xName.push(item.c_type)
                seriesArr.push(item.accumulatedReturn ? parseFloat(String(item.accumulatedReturn * 100)).toFixed(2) : '')
            })

            this.subAdvsiorChartOption.xAxis[0].data = xName
            this.subAdvsiorChartOption.series = [
                {
                    type: 'bar',
                    barWidth: 30,
                    itemStyle: {
                        normal: {
                            color: chartsColors[0],
                            barBorderRadius: [1, 1, 1, 1],
                            shadowColor: 'rgba(0, 0, 0, 0.3)',
                            shadowBlur: 6,
                        }
                    },
                    data: seriesArr
                }
            ]
        } else {
            this.subAdvsiorChartOption.series = []
        }
        this.subAdvsiorChartOption.tooltip.formatter = value => {
            let itemDesc = ``
            value.forEach(item => {
                itemDesc += `${item.marker}${value[0].name}:${isNaN(item.value) ? '--': parseFloat(item.value).toFixed(2) + '%'}<br />`
            })

            return itemDesc
        }
        this.refs.subAdvsiorChart.setOpitions(this.subAdvsiorChartOption)
        this.setState({advisorLoading: false})
    }
    // 分类收益贡献
    async _getrateChart (params) {
        this.setState({rateLoading: true}, async () => {
            const data = await _getRateChart(params)
            console.log('分类收益贡献', data)
            if (data && data.result) {
                let xNames = [], seriesArr = []
                data.result.forEach((item, index) => {
                    xNames.push(`${item.c_org_name ? item.c_org_name:'未知投顾'}（${item.c_type_name ? item.c_type_name :'未知分类'}）` )
                    seriesArr.push(item.n_return ? parseFloat(String(item.n_return * 100)).toFixed(2) : '')
                })

                this.rateChartOption.xAxis[0].data = xNames
                this.rateChartOption.series = [{
                    type: 'bar',
                    barWidth: '50%',
                    itemStyle: {
                        normal: {
                            color: chartsColors[0],
                            barBorderRadius: [1, 1, 1, 1],
                            shadowColor: 'rgba(0, 0, 0, 0.3)',
                            shadowBlur: 6,
                        }
                    },
                    data: seriesArr
                }]
            } else {
                this.rateChartOption.series = []
            }
            this.rateChartOption.tooltip.formatter = value => {
                let itemDesc = ``
                value.forEach(item => {
                    itemDesc += `${item.marker}${value[0].name}:${isNaN(item.value) ? '--': parseFloat(item.value).toFixed(2) + '%'}<br />`
                })

                return itemDesc
            }

            this.refs.rateChart.setOpitions(this.rateChartOption)
            this.setState({rateLoading: false})
        })
    }

    // 子基金资产净值分布
    async _getSubReturn (params) {
        this.setState({subAssetsLoading: true}, async () => {
            const data = await _getSubReturn(params)
            let barsOption = JSON.parse(JSON.stringify(barsOptions))
            console.log('子基金资产分布', data)
            let names = [], xAxis = [], seriesArr = [], tempObj = {}
            if (data && data.result && data.status) {
                const result = data.result
                // n_total_market_value
                result.forEach((item, index) => {
                    item.forEach(item => {
                        if (names.indexOf(item.c_subject_name) === -1) {
                            names.push(item.c_subject_name)
                        }
                        if (xAxis.indexOf(item.t_date) === -1) {
                            xAxis.push(item.t_date)
                        }
                    })
                })

                names.forEach((it, ind) => {
                    let tempArr = []
                    xAxis.forEach((item, index) => {
                        // console.log(result[index].length, ind)
                        if (result[index].length > ind) {
                            tempArr.push(result[index][ind]['n_total_market_value'] / 10000)
                        } else {
                            tempArr.push('--')
                        }

                    })

                    seriesArr.push({
                        name: it,
                        type: 'bar',
                        data: tempArr,
                        stack: 'four',
                        barMaxWidth: '30px',
                        large: true,
                        itemStyle: {
                            normal: {
                                color: chartsColors[ind%8],
                                barBorderRadius: [1, 1, 1, 1],
                            }
                        }
                    })
                })

            }
            barsOption.yAxis[0].name = '资产净值（万）'
            // barsOption.yAxis[0].
            barsOption.xAxis[0].data = xAxis
            barsOption.grid = {
                left: '1%',
                right: '1%',
                bottom: '20%',
                containLabel: true}
            barsOption.legend = {
                icon: 'rect',
                data: names,
                align: 'left',
                bottom: 0,
                color: '#eee',
                show: true
            }
            barsOption.series = seriesArr
            barsOption.dataZoom = [
                {
                    show: true,
                    realtime: false,
                    start: 0,
                    end: 40,
                    bottom: 70,
                }
            ]
            barsOption.tooltip.formatter = value => {
                let itemDesc = `资产净值（万元）${value[0].name}<br />`
                value.forEach(item => {
                    if (item.value !== '--') {
                        itemDesc += `${item.marker}${item.seriesName}: ${isNaN(item.value) ? '--': parseFloat(item.value).toFixed(2)}<br />`
                    }
                })

                return itemDesc
            }

            this.refs.subAssetsChart.setOpitions(barsOption)
            this.setState({subAssetsLoading: false})
        })
    }

    // 子基金周/月净值
    async _getSubProductRate (params) {
        this.setState({advisorRateLoading: true}, async () => {
            const data = await _getSubRate(params)
            let barOption = JSON.parse(JSON.stringify(barsOptions))

            if (data && data.status && data.result) {
                let xNames = [], weekVal = [], monthVal = [], seriesArr = []
                data.result.forEach((item, index) => {
                    xNames.push(item.c_type)
                    weekVal.push(item.weekReturn !== null ? item.weekReturn * 100 : '--')
                    monthVal.push(item.monthReturn !== null ? item.monthReturn * 100 : '--')
                })

                if (weekVal.length > 0) {
                    seriesArr.push({
                        name: '周收益',
                        type: 'bar',
                        data: weekVal,
                        barMaxWidth: '30px',
                        large: true,
                        itemStyle: {
                            normal: {
                                color: chartsColors[0],
                                barBorderRadius: [1, 1, 1, 1],
                                shadowColor: 'rgba(0, 0, 0, 0.3)',
                                shadowBlur: 6,
                            }
                        }
                    })
                }

                if (monthVal.length > 0) {
                    seriesArr.push({
                        name: '月收益',
                        type: 'bar',
                        data: monthVal,
                        barMaxWidth: '30px',
                        large: true,
                        itemStyle: {
                            normal: {
                                color: chartsColors[1],
                                barBorderRadius: [1, 1, 1, 1],
                                shadowColor: 'rgba(0, 0, 0, 0.3)',
                                shadowBlur: 6,
                            }
                        }
                    })
                }
                barOption.legend = {
                    icon: 'line',
                    data: ['月收益', '周收益'],
                    align: 'left',
                    bottom: 0,
                    color: '#eee',
                    show: true
                }
                barOption.yAxis[0].name = '月收益率（%）'
                barOption.tooltip.formatter = value => {
                    let itemDesc = `${value[0].name}<br />`
                    value.forEach(item => {
                        itemDesc += `${item.marker}${item.seriesName}：${isNaN(item.value) ? '--': parseFloat(item.value).toFixed(2) + '%'}<br />`
                    })

                    return itemDesc
                }
                barOption.dataZoom = [
                    {
                        show: true,
                        realtime: false,
                        start: 0,
                        end: 100,
                        bottom: 26,
                    }
                ]
                barOption.xAxis[0].data = xNames
                barOption.xAxis[0].axisLabel = {
                    rotate: 60
                }
                barOption.grid = {
                    left: '3%',
                    right: '3%',
                    bottom: '12%',
                    containLabel: true
                }
                barOption.xAxis[0].axisLine = {
                    onZero: false
                }
                barOption.xAxis[0].nameTextStyle = {
                    color: '#999',
                    align: 'left'
                }
                barOption.series = seriesArr
            }

            this.refs.advisorRateChart.setOpitions(barOption)
            this.setState({advisorRateLoading: false})
        })
    }
}
