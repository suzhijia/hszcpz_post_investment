const fitlerData = {
  bigTypes: [
   {
     name: '股票',
     Class: 'customer_selections',
     id: 'stock',
     dataType: 'asset_type',
   },
   {
     name: '市场中性',
     Class: 'customer_selections',
     id: 'marketing',
     dataType: 'asset_type',
   },
   {
     name: '债券',
     Class: 'customer_selections',
     id: 'bonds',
     dataType: 'asset_type',
   },
   {
     name: '期货',
     Class: 'customer_selections',
     id: 'future',
     dataType: 'asset_type',
   },
   {
     name: '期权',
     Class: 'customer_selections',
     id: 'options',
     dataType: 'asset_type',
   },
   {
     name: '宏观',
     Class: 'customer_selections',
     id: 'large',
     dataType: 'asset_type',
   },
   {
     name: '其他',
     Class: 'customer_selections',
     id: 'other',
     dataType: 'asset_type',
   },
  ],
  stock: [
    {
      name: '股票多头',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '量化选股',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '指数增强',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: 'ETF',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    // {
    //   name: '期货',
    //   Class: 'customer_selections',
    //   dataType: 'strategy_type',
    // },
  ],
  marketing: [
    {
      name: 'T0',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '高频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '低频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
  ],
  bonds: [
    {
      name: '利率债和高等级信用债',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '低等级信用债',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '可转债',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
  ],
  future: [
    {
      name: '高频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '中高频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '低频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
  ],
  options: [
    {
      name: '波动率曲面套利',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '做市套利',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '低频',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
  ],
  large: [
    {
      name: '宏观策略',
      Class: 'customer_selections',
      dataType: 'c_strategy_type',
    }
  ],
  other: [
    {
      name: '相对价值',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '事件驱动',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '组合策略',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '多策略',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '其他二级策略',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '新三板',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '海外基金',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
    {
      name: '货币基金',
      Class: 'customer_selections',
      dataType: 'strategy_type',
    },
  ],
  productStar: [
    {
      name: '一星',
      Class: 'customer_selections',
      id: 'fund_star_1',
      dataType: 'fund_star',
      title: '1'
    },
    {
      name: '二星',
      Class: 'customer_selections',
      id: 'fund_star_2',
      dataType: 'fund_star',
      title: '2'
    },
    {
      name: '三星',
      Class: 'customer_selections',
      id: 'fund_star_3',
      dataType: 'fund_star',
      title: '3'
    },
    {
      name: '四星',
      Class: 'customer_selections',
      id: 'fund_star_4',
      dataType: 'fund_star',
      title: '4'
    },
    {
      name: '五星',
      Class: 'customer_selections',
      id: 'fund_star_5',
      dataType: 'fund_star',
      title: '5'
    },
  ],
  wrapperStar: [
    {
      name: '一星',
      Class: 'customer_selections',
      id: 'consultant_star_1',
      dataType: 'consultant_star',
      title: '1'
    },
    {
      name: '二星',
      Class: 'customer_selections',
      id: 'consultant_star_2',
      dataType: 'consultant_star',
      title: '2'
    },
    {
      name: '三星',
      Class: 'customer_selections',
      id: 'consultant_star_3',
      dataType: 'consultant_star',
      title: '3'
    },
    {
      name: '四星',
      Class: 'customer_selections',
      id: 'consultant_star_4',
      dataType: 'consultant_star',
      title: '4'
    },
    {
      name: '五星',
      Class: 'customer_selections',
      id: 'consultant_star_5',
      dataType: 'consultant_star',
      title: '5'
    },
  ],
  consultantLevel:[
    {
      name: '核心池',
      Class: 'customer_selections',
      dataType: 'consultant_level',
    },
    {
      name: '观察池',
      Class: 'customer_selections',
      dataType: 'consultant_level',
    },
    {
      name: '备选池',
      Class: 'customer_selections',
      dataType: 'consultant_level',
    },
  ]
}

export { fitlerData }
