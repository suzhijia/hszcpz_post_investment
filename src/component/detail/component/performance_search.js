import React, { Component } from 'react'
import { Button, Icon, DatePicker, Select, Cascader, message } from 'antd'
import AddCompanyModal from './add_product_modal'


import moment from 'moment'
const { Option } = Select

let options = [
    {
      value: '综合',
      label: '综合',
      children: [
        {
          value: '沪深300',
          label: '沪深300',
        },
        {
            value: '中证500',
            label: '中证500',
        },
        {
            value: '上证指数',
            label: '上证指数',
        },
      ],
    },
    {
      value: '朝阳永续',
      label: '朝阳永续',
      children: [
        {
          value: '股票多头',
          label: '股票多头',
        },
      ],
    },
  ]

export default class PerformanceSearch extends Component {
    constructor (props) {
        super(props)
        this.state = {
            navType: 'n_added_nav',
            dateStatus: false,
            // periodStatus: false,
            compareRule: '000300.SH',
            dateRange: '',
            statPeriod: 'year',
            startDateVal: null,
            endDateVal: null
        }
    }
    
    displayRender (label) {
        return label[label.length - 1]
    }

    // 更改业绩基准
    valueChange (value) {
        const paramsVal = value[value.length - 1]
        // console.log(paramsVal,'paramsVal')
        this.setState({compareRule: paramsVal})
        
    }

    _addModal = () => {
        this.refs.addCompanyPro.showModal();
    }
    async UNSAFE_componentWillMount () {
        if (this.props.hasOwnProperty('_cascaderTags')) {
            const menuList = await this.props._cascaderTags()
            // console.log(menuList)
            options = menuList
        }
    }

    async componentDidMount () {
        // console.log(this.props,'777')
    }
    // 更改净值
    async changeNav (e) {
        await this.setState({navType: e})
        
    }
    // 选择统计周期调整
    async periodChange(e) {
        await this.setState({statPeriod: e, dateStatus: false})
        
    }
    //选择开始时间
    async startDate (e, str) {
        
        await this.setState({startDateVal: moment(str), dateStatus: true })
    }
    // 结束时间
    async endDate (e, str) {
        await this.setState({endDateVal: moment(str), dateStatus: true})
       
    }

    async reset() {
        this.setState({
                navType: 'n_added_nav',
                dateStatus: false,
                // periodStatus: false,
                compareRule: '000300.SH',
                dateRange: '',
                statPeriod: 'year',
                startDateVal: null,
                endDateVal: null
        })
        const params = { statPeriod: this.state.statPeriod, navType: this.state.navType, dateRange: '', compareRule: this.state.compareRule}
       
    }
    getSecData() {
        console.log("getSecData", this.state.startDateVal, this.state.statPeriod)
        if (!this.state.statPeriod) {
            if (this.state.startDateVal == '') {
                message.info('请选择开始时间!')
                return;
            }
            if (this.state.startDateVal.split('-').join('') >= this.state.endDateVal.split('-').join('')) {
                message.info('结束时间要大于开始时间!')
                return;
            }    
        }
        // let params = { statPeriod: this.state.statPeriod, navType: this.state.navType, dateRange: this.state.startDateVal+ ':' +this.state.endDateVal, compareRule: this.state.compareRule}
        let params = { statPeriod: this.state.statPeriod, navType: this.state.navType, compareRule: this.state.compareRule}
        this.props.getHeadSecData(params)
    }
    render() {
        const {dateStatus, statPeriod, startDateVal, endDateVal } = this.state;
        return (
          <div style={{padding: 20, background: '#fff', overflow: 'hidden', height: '100%'}} >
            <div className={'float_left'}>
                <div className={'float_left left_common_header_content'}>
                    <span>统计周期：</span>
                    <Select style={{width: '140px'}} placeholder='选择统计周期' value={!dateStatus ? statPeriod : ''} onChange={this.periodChange.bind(this)}>
                        <Option value="year">今年以来</Option>
                        <Option value="m1">近一个月</Option>
                        <Option value="m3">近三月</Option>
                        <Option value="m6">近六月</Option>
                        <Option value="y1">近一年</Option>
                        <Option value="y2">近二年</Option>
                        <Option value="y3">近三年</Option>
                        <Option value="y5">近五年</Option>
                        <Option value="total">成立以来</Option>
                    </Select>
                </div>
                <div className={'float_left left_common_header_content ml_10'}>
                    <DatePicker  onChange={this.startDate.bind(this)} placeholder={'开始日期'} value={dateStatus ? startDateVal : null} />
                    <strong>~</strong>
                    <DatePicker  onChange={this.endDate.bind(this)} placeholder={'结束日期'} value={dateStatus ? endDateVal : null} />
                </div>

                <div className={'float_left left_common_header_content ml_10'}>
                    <span>净值类型：</span>
                    <Select value={this.state.navType} style={{width: '130px'}} placeholder='复权累计净值' onChange={this.changeNav.bind(this)}>
                        <Option value='n_added_nav'>复权累计净值</Option>
                        <Option value='n_nav'>单位净值</Option>
                        <Option value='n_swanav'>累计净值</Option>
                    </Select>
                </div>

                <div id='cascader_container' className={'float_left left_common_header_content ml_10'}>
                    <span>业绩基准：</span>
                    <Cascader onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container')} className={`select_cascader_wrapper cascader_container_product`} options={options} placeholder={'选择比较基准'} expandTrigger="hover" />
                </div>
 
                <div className={'float_left left_common_header_content ml_10'}>
                    <Button type='ghost' onClick={this._addModal}><Icon type='plus' className={'product_common_header_plus'} />{this.props._type === 'advisor' ? '添加对比投顾': '添加对比产品'}</Button>
                </div>
                <div className={'float_left left_common_header_content ml_10'}>
                    <Button type='primary' onClick={this.reset.bind(this)}>重置</Button>
                </div>
                <div className={'float_left left_common_header_content ml_10'}>
                    <Button type=''  onClick={this.getSecData.bind(this)}>查询</Button>
                </div>

            </div>
            <AddCompanyModal ref='addCompanyPro' />
            <div className={'float_right'}>

                {/* <Button type='danger'>导出</Button> */}
            </div>
        </div>)
    }
}
