import React, { Component } from 'react'
import { Table, Tag, Input, message, Cascader, Spin } from 'antd'

import {_cascaderTags} from '../../../apis/details/performance_api'
// import axios from '../../../Utils/axios'
// import cookie from 'js-cookie'
// import tableStyle from './risk_table.module.sass'
// import iscroll from 'iscroll'

export default class RiskTable extends Component {
    constructor () {
        super()
        this.state = {
            riskTableData:[],
            dataList:[],
            options: [],
            loading: false,
            columns: [
                {
                    key: '1',
                    dataIndex: 'name',
                    title: '产品名称',
                    width: 200,
                    fixed: 'left',
                    ellipsis: true
                },
                {
                    key: '2',
                    dataIndex: 'n_return_a',
                    fixed: 'left',
                    ellipsis: true,
                    width: 120,
                    title: () => {
                        return <span>收益率<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '3',
                    dataIndex: 'n_stdev_a',
                    width: 120,
                    fixed: 'left',
                    ellipsis: true,
                    title: () => {
                        return <span>波动率<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '4',
                    dataIndex: 'n_sharpe_a',
                    fixed: 'left',
                    width: 160,
                    ellipsis: true,
                    title: () => {
                        return <span className={`clear-fix`}>
                            Sharpe比率<br />
                            <Input className={`table_columns_rows_input float_left`} onBlur={this.sharpeChange.bind(this)} defaultValue={'2.5'} />
                            <em className={`table_columns_percent float_left`}>%</em><Tag className={`table_tags_rates float_left`}>年化</Tag>
                        </span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`,
                },
                {
                    key: '5',
                    dataIndex: 'n_dd_a',
                    width: 108,
                    title: () => {
                        return <span>下行标准差<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '6',
                    dataIndex: 'n_sor_a',
                    width: 120,
                    title: () => {
                        return <span>Sortino比率<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '7',
                    dataIndex: 'n_correlation',
                    width: 110,
                    title: () => {
                        return <span>与基准指数相关系数</span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '8',
                    dataIndex: 'n_spearman',
                    width: 150,
                    title: () => {
                        return <span>
                                    Spearman秩相关
                                    <Cascader allowClear={false} onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} className={`table_select_tips`} options={this.state.options} placeholder={'选择比较基准'} expandTrigger="hover" />
                                </span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '9',
                    dataIndex: 'n_alpah',
                    width: 80,
                    title: () => {
                        return <span>Alpha<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '10',
                    dataIndex: 'n_beta',
                    width: 100,
                    title: () => {
                        return <span>Beta系数</span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '11',
                    dataIndex: 'n_info_a',
                    width: 100,
                    title: () => {
                        return <span>信息比率<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '12',
                    width: 100,
                    dataIndex: 'n_max_drawdown',
                    title: () => {
                        return <span>最大回撤</span>
                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '13',
                    dataIndex: 'n_mdd_repair_time',
                    width: 100,
                    title: () => {
                        return <span>最大回撤回补天数（天）</span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t)}`
                },
                {
                    key: '14',
                    dataIndex: 'n_calmar_a',
                    width: 98,
                    title: () => {
                        return <span>Calmar比率<Tag className={`table_tags_rates`}>年化</Tag></span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                },
                {
                    key: '15',
                    dataIndex: 'n_VaR',
                    title: () => {
                        return <span>年化VaR（95%置信度）</span>
                    },
                    render: t => isNaN(t) ? '--' : `${parseFloat(t).toFixed(2)}`
                }
            ],
        }
    }

    displayRender (label) {
        return label[label.length - 1]
    }

    valueChange (value) {
        const val = value[value.length - 1]
        this.props.valueChange(val)
    }

    sharpeChange (value) {
        const val = value.target.value
        if (val) {
            this.props.sharpeChange(val)
        }

    }

    async componentDidMount () {
        const param = { token: "88d34ede-9302-4b5a-8701-4e940d425d9b" }
        const options = await _cascaderTags(param)
        let map = {}, dest = [];
        options.forEach(item => {
            if (!map[item.parent_name]) {
                let children = []
                item.contrastProduct.forEach(item => {
                    children.push({
                        value: item.benchmark_id,
                        label: item.benchmark_name
                    })
                })
                dest.push({
                    value: item.parent_name,
                    label: item.parent_name,
                    children: children
                })

                map[item.parent_name] = item.parent_name
            }
            return dest
        })
        await this.setState({ options: dest })
    }
    
    render() {
        return <Spin spinning={this.state.loading} delay={10} tip={`loading`}><Table scroll={{x: 2200}} dataSource={this.props.dataSource} columns={this.state.columns} pagination={false} /></Spin>
    }
}
