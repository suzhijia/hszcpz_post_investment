import React, { Component } from 'react'
import { Modal, Tag, Button, Input, Icon, Row, Col, DatePicker, Select, message } from 'antd'
import { fitlerData } from './product_data.js'
import CompanyTable from './company_table.js'
import Swiper from 'swiper/dist/js/swiper.js'

import {_getAddCompanyInitTableList, _getAddCompanyInput, _getAddCompanyTable} from '../../../apis/details/performance_api'
import 'swiper/dist/css/swiper.min.css'
import './add_product_modal.sass'
import moment from 'moment'
import axios from '../../../Utils/axios'
import cookie from 'js-cookie'
import locale from 'antd/es/date-picker/locale/zh_CN'


const Option = Select.Option;
let timer = null
export default class AddCompanyModal extends Component {
  constructor (...props) {
        super(...props)
        this.state = {
          visible: false, 
		  expend: true,
		  loading: false,
          tags: [],
          dataSource: [],
          fundSearch: '',
          addressSearch: '',
          rateRanking: '',
          drawdownRanking: '',
          sharpeRanking: '',
          mangerSearch: '',
          expend: true,
          changeN: true,
          backN: true,
          sharpeN: true,
          coreSelect: true,
          stock: false,
          marketing: false,
          bonds: false,
          future: false,
          options: false,
          currentPage: 1,
          totalPage: 1,
          pageSize: 5,
          takeDate: new Date().getTime(),
          sumCount: 0,
          searchParams: null,
          loading: false,
          selectionStatus: true,
          asset_type: [],
          strategy_type: [],
          c_asset_type: [],
          c_strategy_type: [],
          consultant_level: [],
          consultant_star: [],
          begin_org_foundation_date: '',
          end_org_foundation_date: '',
          is_reg: [],
          fund_status: [],
          begin_fund_foundation_date: null,
          end_fund_foundation_date: null,
          begin_new_nav_date: '',
          end_new_nav_date: '',
          issue_type: '',
          fund_star: [],
          date_type: 'year',
          returnType: 'section',
          drawdownType: 'section',
          fund_consultant: '',
          fund_manager: '',
          fund_name: '',
          reg_address: '',
          drawdown_end: '',
          drawdown_begin: '',
          sharpe_end: '',
          sharpe_begin: '',
          return_end: '',
          return_begin: '',
          c_person_name: '',
          c_org_name: ''
        }
  }
	async componentDidMount () {
			window.__LOADING__ = this.toggleLoading
			document.addEventListener('click', (e) => {
					e.stopPropagation();
					this.setState({dropList: null})
			}, false)
	}

  showModal = () => {
      this.setState({ visible: true})
	  this.check();
	  this._initData()
  }

  hideModal () {
      this.setState({ visible: false })
      this.resetOptions();
  }
    
  resetState = () => {
    this.setState({
        asset_type: [],
        strategy_type: [],
        consultant_level: [],
        consultant_star: [],
        begin_org_foundation_date: '',
        end_org_foundation_date: '',
        is_reg: [],
        fund_status: [],
        begin_fund_foundation_date: '',
        end_fund_foundation_date: '',
        begin_new_nav_date: '',
        end_new_nav_date: '',
        issue_type: '',
        fund_star: [],
        returnType: '',
        sharpeType: '',
        drawdownType: '',
        fund_consultant: '',
        fund_manager: '',
        fund_name: '',
        reg_address: ''
    })
  }

  resetOptions = async () => {
      let selectElement = document.getElementsByClassName('customer_select_checked')
      let dataInput = document.getElementsByClassName('ant-calendar-picker-input')
      let selectInput = document.querySelectorAll('.common_search_selection .ant-select-selection-selected-value')
      Array.from(selectElement).forEach(item => {
          item.setAttribute('class', 'customer_selections')
          const targetEle = item
          const attributeStatus = item.getAttribute('data-type')
          targetEle.setAttribute('class', 'customer_selections')
          this.setState({[attributeStatus]: null})
          this.resetState()
      })
      dataInput[0].removeAttribute('readonly')
      dataInput[0].value = '-'
      Array.from(dataInput).forEach(item => {
          item.removeAttribute('readOnly')
          item.value = ''
      })
      await this.setState({fundSearch: '', mangerSearch: '', fund_name: '', addressSearch: '', return_begin: null,
            return_end: '',
            drawdown_begin: '',
            sharpe_begin: '',
            sharpe_end: '',
            c_org_name: '',
            c_person_name: '',
            date_type: '',
            returnType: '',
            begin_fund_foundation_date: null,
            end_fund_foundation_date: null,
            drawdownType: '',
            drawdown_end: '',
            dataSource: [],
            currentPage: 1,
            pageSize: 5,
      })
      Array.from(selectInput).forEach(item => item.innerHTML = '')
      if (this.state.fund_name === '' || this.state.selectionStatus) {
          // this.refs.HomeTable.changeSize(1,5)
          this._initData()
          // this._axiosData();
      }
  }

  check() {
    const ele = document.querySelector('.swiper-container')
    if(ele) {
         //  执行dom加载完成后的操作，例如echart的初始化操作
         new Swiper('.swiper-container', {
            observer:true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: (index, className) => (`<span class=${className}>${index + 1}</span>`)
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        })
        //  清除定时器
        if(!timer) {
            clearTimeout(timer)
        }
    } else {
        //  自我调用
        timer = setTimeout(this.check, 0)
    }
  }
    
  toggle = () => {
      this.setState({
          expend: !this.state.expend,
          selectionStatus: !this.state.selectionStatus,
          begin_fund_foundation_date: null,
          end_fund_foundation_date: null
      })
  }
    
	beginFund = async (e, str) => {
		this.setState({begin_fund_foundation_date: moment(str)})
	}
	endFund = async (e, str) => {
			await this.setState({end_fund_foundation_date: moment(str)})
	}

    selectionCheck = async e => {
		await this.setState({date_type: e})
		if (this.state.sharpe_begin && this.state.sharpe_end | this.state.return_begin && this.state.return_end | this.state.drawdown_begin && this.state.drawdown_end) {
			this.searchProduct()
		}
	}

  renderDropList () {
    let listItem = ''
    if (this.state.dropList) {
        let list = this.state.dropList
        let arrList = []
        for(let i =0; i < 11; i++) {
            if (i < list.length) {
                arrList.push(<li key={i} onClick={this.enterSearch.bind(this)}>{list[i]}</li>)
                continue
            }
        }

        listItem = <div id="customerDrop" className={`customer_drop_down`}>
            <ul className={`customer_drop_list`}>
                {arrList}
            </ul>
        </div>
    }

    return listItem
  }

  clicks = (e) => {
        const classStatus = e.target.getAttribute('class')
        const targetEle = e.target
        const attributeStatus = e.target.getAttribute('data-type')
        const id = e.target.getAttribute('id')
        let temp = null
        const eleContainer = e.target.innerHTML

        if (classStatus === 'customer_selections') {
            targetEle.setAttribute('class', 'customer_selections customer_select_checked')
            this.setState({ [id]: eleContainer })
            switch (attributeStatus) {
                case 'asset_type':
                    temp = this.state.asset_type
                    temp.push(eleContainer)
                    this.setState({ asset_type: temp })
                    break

                case 'c_asset_type':
                    temp = this.state.c_asset_type
                    temp.push(eleContainer)
                    this.setState({ c_asset_type: temp })
                    break

                case 'strategy':
                    temp = this.state.strategy
                    temp.push(eleContainer)
                    this.setState({ strategy: temp })
                    break
                case 'strategy_type':
                    temp = this.state.strategy_type
                    temp.push(eleContainer)
                    this.setState({ strategy_type: temp })
                    break

                case 'c_strategy_type':
                    temp = this.state.c_strategy_type
                    temp.push(eleContainer)
                    this.setState({ c_strategy_type: temp })
                    break

                case 'fund_star':
                    temp = this.state.fund_star
                    temp.push(e.target.getAttribute('title'))
                    this.setState({ fund_star: temp })
                    break

                case 'consultant_star':
                    temp = this.state.consultant_star
                    temp.push(e.target.getAttribute('title'))
                    this.setState({ consultant_star: temp })
                    break
                case 'consultant_level':
                    temp = this.state.consultant_level
                    temp.push(eleContainer)
                    this.setState({ consultant_level: temp })
                    break

                default:
                    break
            }
            // 删除选项
        } else {
            targetEle.setAttribute('class', 'customer_selections')
            switch (attributeStatus) {
                case 'asset_type':
                    temp = this.state.asset_type
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ asset_type: temp })
                    break

                case 'c_asset_type':
                    temp = this.state.c_asset_type
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ c_asset_type: temp })
                    break

                case 'strategy':
                    temp = this.state.strategy
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ strategy: temp })
                    break

                case 'strategy_type':
                    temp = this.state.strategy_type
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ strategy_type: temp })
                    break

                case 'c_strategy_type':
                    temp = this.state.c_strategy_type
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ c_strategy_type: temp })
                    break

                case 'fund_star':
                    temp = this.state.fund_star
                    temp.splice(temp.indexOf(e.target.getAttribute('title')), 1)
                    this.setState({ fund_star: temp })
                    break

                case 'consultant_star':
                    temp = this.state.consultant_star
                    temp.splice(temp.indexOf(e.target.getAttribute('title')), 1)
                    this.setState({ consultant_star: temp })
                    break

                case 'consultant_level':
                    temp = this.state.consultant_level
                    temp.splice(temp.indexOf(eleContainer), 1)
                    this.setState({ consultant_star: temp })
                    break

                default:
                    break
            }

            this.setState({ [id]: null })
		}
		this.searchProduct()
  }

  unLimited(e) {
        let checkFalse = document.getElementsByClassName('check_false')
        Array.from(checkFalse).forEach(item => {
            item.addEventListener('click', e => {
                const checkState = e.target.getAttribute('data-check')
							let checkList = e.target.parentNode.getElementsByTagName('dd')
                switch (checkState) {
                    case 'asset_type':
                        this.setState({
                            stock: false,
                            marketing: false,
                            bonds: false,
                            future: false,
                            options: false,
                            otherOptions: false,
                            futureOptions: false,
                            other: null,
                            large: null
                        })
                        Array.from(checkList).forEach(item => {
                            item.setAttribute('class', 'customer_selections')
                        })
                        break
                    case 'strategy_type':
                        const checkLi = e.target.parentNode.parentNode.parentNode.querySelectorAll('.customer_selections')
                        Array.from(checkLi).forEach(item => {
                            item.setAttribute('class', 'customer_selections')
                        })
						break
					case 'consultant_level':
                        const checkDd = e.target.parentNode.querySelectorAll('.customer_selections')
                        Array.from(checkDd).forEach(item => {
                            item.setAttribute('class', 'customer_selections')
						})
                    break
                    default:
                        Array.from(checkList).forEach(item => {
                            item.setAttribute('class', 'customer_selections')
                        })
                    break
                }
				this.setState({ [checkState]: [] })
				this.searchProduct()
            }, false)
        })
	}

	async enterSearch (e) {
		const searchValue = e.target.value || e.target.innerHTML
		await this.setState({fund_name: searchValue, dropList: null})
		setTimeout(() => {
				this.searchProduct()
		}, 50)

	}
	changeInputs (e) {
		const stateId = e.target.getAttribute('id')
		if (isNaN(e.currentTarget.value)) {
				message.destroy()
				message.info('请输入正确的数字')
				return
		}
		this.setState({[stateId]: e.target.value})
	}

	changeName (e) {
	const stateId = e.target.getAttribute('id')
	this.setState({[stateId]: e.target.value})
	}
	async nameChange (e) {
		this.setState({fund_name: e.target.value})
	}
	
	async searchLike (e) {
		const currentValue = e.target.value
		const params = {fund_name: currentValue,  token: "e91432c2-2f47-4c58-aa54-1b0bd4a971b4", pageSize: 10, pageNo: 1 }
		if (currentValue !== '' && (/\S+/.test(currentValue))) {
			const data = await _getAddCompanyInput(params)
			
			this.setState({loading: false})
			if (data.result === 'success') {
					const fundName = data.fundNames
					if (fundName.length > 0 && currentValue !== '') {
							this.setState({dropList: fundName})
					}
			} else {
					this.setState({dropList: null})
			}
		}
		if (currentValue === '') {
				this.setState({dropList: null})
				return
		}
	}

    searchProduct = () => {
		let thisState = this.state
        let searchParams = {
			// token: cookie.get('token')
			token: "88d34ede-9302-4b5a-8701-4e940d425d9b"
        }
        if (thisState.selectionStatus) {
            if (thisState.return_begin && thisState.selectionStatus) {
                searchParams['return_begin'] = thisState.return_begin
            }
            if (thisState.return_end && thisState.selectionStatus) {
                searchParams['return_end'] = thisState.return_end
            }
            if (thisState.drawdown_begin && thisState.selectionStatus) {
                searchParams['drawdown_begin'] = thisState.drawdown_begin
            }
            if (thisState.drawdown_end && thisState.selectionStatus) {
                searchParams['drawdown_end'] = thisState.drawdown_end
            }
            if (thisState.asset_type.length > 0) {
                searchParams['asset_type'] = thisState.asset_type
            }
            if (thisState.strategy_type.length > 0 && thisState.strategy_type[0]!== null && thisState.asset_type.length > 0) {
                searchParams['strategy_type'] = thisState.strategy_type
            }

            if (thisState.c_strategy_type.length > 0 && thisState.c_strategy_type[0]!== null) {
                searchParams['c_strategy_type'] = thisState.c_strategy_type
            }

            if (thisState.consultant_level.length > 0 && thisState.consultant_level[0]!== null) {
                searchParams['consultant_level'] = thisState.consultant_level
            }

            if (thisState.consultant_star.length > 0 && thisState.consultant_star[0]!== null) {
                searchParams['consultant_star'] = thisState.consultant_star
            }

            if (thisState.begin_org_foundation_date) {
                searchParams['begin_org_foundation_date'] = thisState.begin_org_foundation_date
            }

            if (thisState.end_org_foundation_date) {
                searchParams['end_org_foundation_date'] = thisState.end_org_foundation_date
            }

            if (thisState.is_reg.length > 0 && thisState.is_reg[0]!== null) {
                searchParams['is_reg'] = thisState.is_reg
            }

            if (thisState.fund_status.length > 0 && thisState.fund_status[0]!== null) {
                searchParams['fund_status'] = thisState.fund_status
            }

            if (thisState.begin_fund_foundation_date) {
                searchParams['begin_fund_foundation_date'] = moment(moment(thisState.begin_fund_foundation_date).format('YYYY-MM-DD')).valueOf() / 1000
            }

            if (thisState.end_fund_foundation_date) {
                searchParams['end_fund_foundation_date'] = moment(moment(thisState.end_fund_foundation_date).format('YYYY-MM-DD')).valueOf() / 1000
            }

            if (thisState.begin_new_nav_date) {
                searchParams['begin_new_nav_date'] = thisState.begin_new_nav_date
            }

            if (thisState.end_new_nav_date) {
                searchParams['end_new_nav_date'] = thisState.end_new_nav_date
            }

            if (thisState.issue_type.length > 0 && thisState.issue_type[0]!== null) {
                searchParams['issue_type'] = thisState.issue_type
            }

            if (thisState.fund_star.length > 0 && thisState.fund_star[0]!== null) {
                searchParams['fund_star'] = thisState.fund_star
            }

            // eslint-disable-next-line no-mixed-operators
            if (thisState.date_type && thisState.selectionStatus && thisState.selectionStatus && thisState.return_begin &&  thisState.return_end ||
                // eslint-disable-next-line no-mixed-operators
                thisState.date_type && thisState.selectionStatus && thisState.selectionStatus && thisState.drawdown_begin &&  thisState.drawdown_end ||
                // eslint-disable-next-line no-mixed-operators
                thisState.date_type && thisState.selectionStatus && thisState.selectionStatus && thisState.sharpe_begin &&  thisState.sharpe_begin) {
                searchParams['date_type'] = thisState.date_type
            }

            if (thisState.returnType && thisState.selectionStatus && thisState.return_begin &&  thisState.return_end) {
                searchParams['returnType'] = thisState.returnType
            }

            if (thisState.sharpeType && thisState.selectionStatus && thisState.sharpe_end && thisState.sharpe_begin) {
                searchParams['sharpeType'] = thisState.sharpeType
            }

            if (thisState.sharpe_begin && thisState.selectionStatus) {
                searchParams['sharpe_begin'] = thisState.sharpe_begin
            }

            if (thisState.sharpe_end && thisState.selectionStatus) {
                searchParams['sharpe_end'] = thisState.sharpe_end
            }

            if (thisState.drawdownType && thisState.selectionStatus && thisState.drawdown_begin &&  thisState.drawdown_end) {
                searchParams['drawdownType'] = thisState.drawdownType
            }

        }

        if (thisState.find_c_interval) {
            searchParams['find_c_interval'] = thisState.find_c_interval
        }

        if (thisState.find_c_rank_type) {
            searchParams['find_c_rank_type'] = thisState.find_c_rank_type
        }

        if (thisState.c_org_name) {
            searchParams['c_org_name'] = thisState.c_org_name
        }

        if (thisState.c_person_name) {
            searchParams['c_person_name'] = thisState.c_person_name
        }

        if (thisState.fund_name) {
            searchParams['fund_name'] = thisState.fund_name
        }

        let searchInput = document.getElementById('search_product_name')
        Array.from(searchInput).forEach(item => {
            if (item.value) {
                searchParams[item.getAttribute('data')] = item.value
                let searchHistory = window.localStorage.getItem('searchHistory') || []
                searchHistory.push(item.value)
                window.localStorage.setItem(searchHistory, searchHistory)
            }
        })

        this.setState({searchParams})
        this._axiosData(searchParams)

    }

    handleClose = (value) => {
        const attentionArr = JSON.parse(window.sessionStorage.getItem("attentionArr"));
        attentionArr.fundId.splice(attentionArr.fundName.indexOf(value), 1)
        attentionArr.fundName = attentionArr.fundName.filter((item) => item !== value)
        window.sessionStorage.setItem("attentionArr", JSON.stringify(attentionArr))
        window.sessionStorage.setItem("stroageId", attentionArr.fundId)
    }

    compare = () => {
        this.hideModal();
        const stroageId = window.sessionStorage.getItem("stroageId");
        const params = { fundIds: stroageId }
        this._axiosData(params) 
    }

  empty = () => {
        window.sessionStorage.removeItem("attentionArr")
        window.sessionStorage.removeItem("stroageId")
	}
	
	// table分页
	getTableData = (page) => {
		const params = { pageNo: page.current, pageSize: page.pageSize }
		this.setState({ currentPage: page.current }, () => {
			this._axiosData(params)
		})
	}

	changeSize = (current, size) => { 
		this.setState({
			pageSize: size,
			currentPage: current
		}, () => {
			const params = { pageNo: current, pageSize: size }
			this._axiosData(params)
		})
	}

	async _axiosData(params) {
		this.setState({loading: true})
		params = Object.assign(params, { pageSize: this.state.pageSize, pageNo: this.state.currentPage, token: "88d34ede-9302-4b5a-8701-4e940d425d9b" })
			const data = await _getAddCompanyTable(params)
			if (data.result === 'success') {
				let dataList = data.sumlist
				if (dataList) {
					for (let i = 0; i < dataList.length; i++) {
						dataList[i].key = i + 1
					}
				}

				this.setState({
					dataSource: dataList,
					totalPage: data.pageCount,
					takeDate: data.takeDate || new Date().getTime(),
					sumCount: data.sumCount,
					currentPage: this.state.currentPage,
					loading: false
				})
			}
	}

	async _initData() {
		this.setState({ loading: true})
		const params = { pageSize: this.state.pageSize, pageNo: this.state.currentPage, token: "88d34ede-9302-4b5a-8701-4e940d425d9b" }
		const data = await _getAddCompanyInitTableList(params)
		let dataList = data.sumlist
				if (dataList) {
						for (let i = 0; i < dataList.length; i++) {
								dataList[i].key = i  + 1
						}
				}
				this.setState({
						dataSource: dataList,
						totalPage: data.pageCount,
						takeDate: data.takeDate || new Date().getTime(),
						sumCount: data.sumCount,
						loading: false
				})
  }
    
  render() {
		const { visible, expend } = this.state;
		const tags = window.sessionStorage.getItem("attentionArr") ? JSON.parse(window.sessionStorage.getItem("attentionArr")).fundName : [];
        return (
          <Modal
            title=''
            closable={true}
            visible={visible}
            destroyOnClose={true}
            maskClosable={true}
            centered={false}
            className={`add_product_modal_content home_global`}
            wrapClassName={`add_product_modal_container`}
            style = {{top: '5%'}}
            onCancel={this.hideModal.bind(this)}
            footer={null}>
              <div className={`add_modal_title`}><h1>添加对比产品</h1></div>
              <div className={`compare_product_content clear-fix`}>
                  <div className={`float_left`}>
                      <div className={`left_title float_left`}><span>对比产品：</span></div>
                        <div className={`float_left`}>
                          {tags.map((item) => <Tag className={`compare_tags`} onClose={() => this.handleClose(item)} closable key={item}>{item}</Tag>)}
                          {/* {tags.map((item, index) => <Tag onClose={() => this.handleClose(item)} className={`compare_tags`} key={index} closable={true}>{item}</Tag>)} */}
                      </div>
                  </div>
              </div>
            <div className={`tab_container`}>
                <ul className={`tab_fixed_content`}>
                    <li className={`position_relative product_name_input`}>
                        <Input className='searchProduct' id='search_product_name' onKeyUp={this.searchLike.bind(this)} onPressEnter={this.enterSearch} value={this.state.fund_name} type='text' onChange={this.nameChange.bind(this)} data='fund_name' placeholder={'产品名称'} />
                        { this.renderDropList() }
                    </li>
                    <li className={`position_relative product_name_input`}><Input className='searchProduct' id='c_org_name' placeholder={'投顾名称'}  value={this.state.c_org_name} onChange={this.changeName.bind(this)} onPressEnter={this.searchProduct} /></li>
                    <li className={`position_relative product_name_input`}><Input className='searchProduct' id='c_person_name' placeholder={'投资经理名称'} value={this.state.c_person_name} onChange={this.changeName.bind(this)} onPressEnter={this.searchProduct} /></li>
                    <li className={`position_relative search`}><Button type="primary" onClick={this.searchProduct}>搜索</Button></li>
                    <li><Button type="primary" onClick={this.toggle}>{expend ? '收起筛选项' : '展开筛选项'} <Icon type={expend ? 'up' : 'down'} /></Button></li>
                    <li><Button type="primary" onClick={this.resetOptions}>重置</Button></li>
                </ul>
            </div>
            <div className={`tab_container modal_swiper_container`} style={{ display: this.state.expend ? 'block' : 'none' }}>
              <div className={`swiper-container`}>
                <div className="swiper-wrapper">
                  <div className="swiper-slide">
                    <Row className={`tab_tit_row swiper_select_row clear-fix`}>
                        <Col className='float_left' style={{width: '80px'}}><span className={`tab_column_tit`}>大类策略：</span></Col>
                        <Col span={20}>
                          <dl className='clear-fix'>
                            <dt onClick={this.unLimited.bind(this)} className={`${this.state.asset_type.length > 0 ? '' : 'dt_select'} check_false`} data-check='asset_type'>不限</dt>
                            {fitlerData.bigTypes.map((item, index) => <dd key={index} data-type={item.dataType}  id={item.id} onClick={this.clicks} className={item.Class}>{item.name}</dd>)}
                          </dl>
                        </Col>
                      </Row>
                      <Row className={`swiper_select_row clear-fix`} type='flex' justify='start'>
                            <Col style={{width: '80px'}}><span className={`tab_column_tit`}>细分策略：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dt onClick={this.unLimited.bind(this)} className={`${this.state.strategy_type.length > 0 ? '' : 'dt_select'} check_false`} data-check='strategy_type'>不限</dt>
                                </dl>
                            </Col>
                            <Col>
                                <ul style={{display: this.state.stock ? 'block': 'none'}} className={`list_options clear-fix`}>
                                    <li>股票</li>
                                    {fitlerData.stock.map((item, index) => <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>)}
                                </ul>

                                <ul style={{display: this.state.marketing ? 'block' : 'none'}} className={`list_options clear-fix`}>
                                    <li>市场中性</li>
                                    {
                                        fitlerData.marketing.map((item, index)=>{
                                            return <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>
                                        })
                                    }
                                </ul>

                                <ul style={{display: this.state.bonds ? 'block' : 'none'}} className={`list_options clear-fix`}>
                                    <li>债券</li>
                                    {
                                        fitlerData.bonds.map((item, index)=>{
                                            return <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>
                                        })
                                    }
                                </ul>

                                <ul style={{display: this.state.future ? 'block' : 'none'}} className={`list_options clear-fix`}>
                                    <li>期货</li>
                                    {
                                        fitlerData.future.map((item, index)=>{
                                            return <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>
                                        })
                                    }
                                </ul>

                                <ul style={{display: this.state.options ? 'block' : 'none'}} className={`list_options clear-fix`}>
                                    <li>期权</li>
                                    {
                                        fitlerData.options.map((item, index)=>{
                                            return <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>
                                        })
                                    }
                                </ul>


                                <ul style={{display: this.state.stockOptions ? 'block' : 'none'}} className={`list_options clear-fix`}>
                                    <li>其他</li>
                                    {
                                        fitlerData.other.map((item, index)=>{
                                            return <li key={index} data-type={item.dataType} onClick={this.clicks} className={item.Class}>{item.name}</li>
                                        })
                                    }
                                </ul>


                                <div style={{display: (
                                    !(this.state.moreOptions)&&
                                    !(this.state.groupOptions)&&
                                    !(this.state.otherOptions)&&
                                    !(this.state.bondsOptions)&&
                                    !(this.state.boardOptions)&&
                                    !(this.state.macroOptions)&&
                                    !(this.state.eventOptions)&&
                                    !(this.state.valueOptions)&&
                                    !(this.state.stockOptions)&&
                                    !(this.state.options) &&
                                    !(this.state.bonds) &&
                                    !(this.state.future) &&
                                    !(this.state.marketing) &&
                                    !(this.state.stock)) ? 'block' : 'none'}} className={`core_rating`}>请选择大类策略</div>
                            </Col>
                        </Row>
                  </div>
                  <div className={`swiper-slide`}>
                      <Row type='flex' className={`tab_tit_row swiper_select_row clear-fix`} justify='start'>
                          <Col style={{width: '80px'}}><span className={ `tab_column_tit`}>产品星级：</span></Col>
                          <Col>
                              <dl className='clear-fix'>
                                  <dt onClick={this.unLimited.bind(this) } className={`${this.state.fund_star.length > 0 ? '' : 'dt_select'} check_false`} data-check='fund_star'>不限</dt>
                                  {
                                      fitlerData.productStar.map((item, index)=>{
                                          return <dd key={index} title={item.title} id={item.id} data-type={item.dataType} onClick={this.clicks } className={item.Class}>{item.name}</dd>
                                      })
                                  }
                              </dl>
                          </Col>
                          <Col  className={ `line_split_tab`}><span className={ `tab_column_tit`}>产品成立时间：</span></Col>
                          <Col>
                              <span></span>
                              <DatePicker allowClear={false} locale={locale} value={this.state.begin_fund_foundation_date} onChange={this.beginFund} className={`tab_search_radius date_picker_content`} placeholder='起始日期' />
												<span style={{ color: "#d9d9d9", margin: '0 10px'}}>——</span>
                              <DatePicker allowClear={false} locale={locale} value={this.state.end_fund_foundation_date} onChange={this.endFund} className={`tab_search_radius  date_picker_content`} placeholder='截止日期' />
                          </Col>
                      </Row>
                      <Row type='flex' className={`swiper_select_row clear-fix`} justify='start'>
                            <Col style={{width: '80px'}}><span className={ `tab_column_tit`}>投顾星级：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                <dt onClick={this.unLimited.bind(this) } className={`${this.state.consultant_star.length > 0 ? '' : 'dt_select'} check_false`} data-check='consultant_star'>不限</dt>
                                {
                                    fitlerData.wrapperStar.map((item, index)=>{
                                        return <dd key={index} title={item.title} id={item.id} data-type={item.dataType} onClick={this.clicks } className={item.Class}>{item.name}</dd>
                                    })
                                }
                                </dl>
                            </Col>
                            <Col  className={ `line_split_tab`}><span className={ `tab_column_tit`}>投顾层级：</span></Col>
                            <Col>
                                    <span></span>
                                    <dl className='clear-fix'>
                                        <dt onClick={this.unLimited.bind(this) } className={`${this.state.consultant_level.length > 0 ? '' : 'dt_select'} check_false`} data-check='consultant_level'>不限</dt>
                                        {
                                            fitlerData.consultantLevel.map((item, index)=>{
                                                return <dd key={index} data-type={item.dataType} onClick={this.clicks } className={item.Class}>{item.name}</dd>
                                            })
                                        }
                                    </dl>
                                </Col>
                            </Row>
                    </div>
                  <div className={`swiper-slide`}>
                        <Row type='flex' className={`swiper_select_row clear-fix`} justify='start' style={{marginTop: 10}}>
                                <Col style={{width: 'calc(100% - 122px)'}}>
                                    <Row type='flex' className='pb_10'>
                                        <Col>
                                            <div className={ `tab_selection_wrapper`}>
                                                <span className={ `tab_selections_tit`}>收益风险区间：</span>
                                                <Select value={this.state.date_type} placeholder={`今年以来`} onChange={this.selectionCheck.bind(this) } className={`${ `selection_content`} common_search_selection`}>
                                                    <Option value="year">今年以来</Option>
                                                    <Option value="m1">近一个月</Option>
                                                    <Option value="m3">近三月</Option>
                                                    <Option value="m6">近六月</Option>
                                                    <Option value="y1">近一年</Option>
                                                    <Option value="y2">近二年</Option>
                                                    <Option value="y3">近三年</Option>
                                                    <Option value="y5">近五年</Option>
                                                    <Option value="total">成立以来</Option>
                                                </Select>
                                            </div>
                                        </Col>
                                        <Col>
                                            <div className={ `tab_selection_wrapper`}>
                                                <span className={ `tab_selections_tit`}>Sharpe比率：</span>
                                                <Select defaultValue={this.state.sharpeType} onChange={this.sharpeChange} className={ `selection_content`} placeholder={`绝对Sharpe比区间`}>
                                                    <Option value="section">绝对Sharpe比区间</Option>
                                                    <Option value="ranking">Sharpe比市场排名</Option>
                                                </Select>
                                                <Input id='sharpe_begin' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.sharpe_begin} />
                                                <span>{this.state.sharpeN ? '' : '/1'}</span>
                                                <span className={ `selection_percent_split`}>~</span>
                                                <Input id='sharpe_end' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.sharpe_end} /><span>{this.state.sharpeN ? '' : `/${this.state.sharpeRanking}`}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row type='flex' className='pb_10'>
                                        <Col>
                                            <div className={ `tab_selection_wrapper`}>
                                                <span className={ `tab_selections_tit`}>收益：</span>
                                                <Select defaultValue={this.state.returnType} placeholder={`绝对收益区间`} onChange={this.inChange} className={`${ `selection_content`} common_search_selection`}>
                                                    <Option value="section">绝对收益区间</Option>
                                                    <Option value="ranking">收益市场排名</Option>
                                                </Select>
                                                <Input id='return_begin' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.return_begin} />
                                                <span>{this.state.changeN ? '%' : `/1`}</span>
                                                <span className={ `selection_percent_split`}>~</span>
                                                <Input id='return_end' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.return_end} /><span>{this.state.changeN ? '%' : `/${this.state.rateRanking}`}</span>
                                            </div>


                                        </Col>
                                        <Col>
                                            <div>
                                                <span className={ `tab_selections_tit`}>回撤：</span>
                                                <Select value={this.state.drawdownType} placeholder={`绝对回撤区间`} onChange={this.backChange} className={`${ `selection_content`} common_search_selection`}>
                                                    <Option value="section">绝对回撤区间</Option>
                                                    <Option value="ranking">回撤市场排名</Option>
                                                </Select>
                                                <Input id='drawdown_begin' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.drawdown_begin} />
                                                <span>{this.state.backN ? '%' : `/1`}</span>
                                                <span className={ `selection_percent_split`}>~</span>
                                                <Input id='drawdown_end' onChange={this.changeInputs } maxLength={6} className={ `percent_tab_input`} value={this.state.drawdown_end} /><span>{this.state.backN ? '%' : `/${this.state.drawdownRanking}`}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                </Col>

                        </Row>
                  </div>
                </div>
                <div className={`swiper-button-next modal_swiper_button_next`}>
                    {/* <span className={`iconfont`}>&#xe60a;</span> */}
                </div>
                <div className={`swiper-button-prev modal_swiper_button_prev`}>
                    {/* <span className={`iconfont`}>&#xe609;</span> */}
                </div>
                <div className={`swiper-pagination modal_pagination`}></div>
              </div>
            </div>
			<CompanyTable ref="HomeTable" dataSource={this.state.dataSource} {...this.state} changeRate={this.selectionCheck.bind(this)} tableChange={this.getTableData.bind(this)} changeSize={this.changeSize.bind(this)} />
            {/* <CommonTable ref="HomeTable" query = {this.props.query} dateType={this.state.date_type}sessionNameArr= {this.props.sessionNameArr} stroageId={this.props.stroageId} getTableData={this.getTableData} ischeckedRef={this.ischeckedRef}  attetionParent={this.attetionParent} {...this.state} ></CommonTable> */}
            <div className={`product_compare_btns_container`}>
                <Button type='primary' onClick={this.compare} className={` mr_10`}>确定</Button>
                <Button type='primary' onClick={this.empty}>清空</Button>
            </div>
          </Modal>
        )
 }
}
