import React, { Component } from 'react'
import {Table, Tag, Input, message, Cascader, Spin} from 'antd'
import tableStyle from './risk_table.module.sass'
import url from 'url'

const { query } = url.parse(window.location.href, true)

export default class RiskTableScences extends Component {
    constructor () {
        super()
        this.state = {
            riskTableData:[],
            dataList:[],
            options: [],
            loading: false,
            columns: [
                {
                    key: '1',
                    dataIndex: 'name',
                    title: '产品名称',
                    // width: 200,
                    // fixed: 'left',
                    // ellipsis: true
                },
                {
                    key: '2',
                    dataIndex: 'n_return_a',
                    // fixed: 'left',
                    // ellipsis: true,
                    // width: 80,
                    title: () => {
                        return <div><span>收益率</span><br/><Tag color="blue">年化</Tag></div>

                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '3',
                    dataIndex: 'n_stdev_a',
                    // width: 70,
                    // // fixed: 'left',
                    // ellipsis: true,
                    title: () => {
                        return <div><span>波动率</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '4',
                    dataIndex: 'n_sharpe_a',
                    // fixed: 'left',
                    // width: 130,
                    // ellipsis: true,
                    title: () => {
                        return <div className={`clear-fix`}>
                            <span>Sharpe比率</span><br/>
                            <span>
                                <Input  className={`${tableStyle.inputStyle}`}/>
                                <em >%</em><Tag color="blue">年化</Tag>
                            </span>

                        </div>
                    }
                },
                {
                    key: '5',
                    dataIndex: 'n_dd_a',
                    // width: 108,
                    title: () => {
                        return <div><span>下行标准差</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '6',
                    dataIndex: 'n_sor_a',
                    // width: 100,
                    title: () => {
                        return <div><span>Sortino比率</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '7',
                    dataIndex: 'n_correlation',
                    // width: 94,
                    title: () => {
                        return <span>与基准指数相关系数</span>
                    }
                },
                {
                    key: '8',
                    dataIndex: 'n_spearman',
                    // width: 130,
                    title: () => {
                        return <span>
                                    Spearman秩相关
                                    <Cascader allowClear={false} onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} className={`table_select_tips`} options={this.state.options} placeholder={'选择比较基准'} expandTrigger="hover" />
                                </span>
                    }
                },
                {
                    key: '9',
                    dataIndex: 'n_alpah',
                    // width: 80,
                    title: () => {
                        return <div><span>Alpha</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '10',
                    dataIndex: 'n_beta',
                    // width: 80,
                    title: () => {
                        return <span>Beta系数</span>
                    }
                },
                {
                    key: '11',
                    dataIndex: 'n_info_a',
                    // width: 80,
                    title: () => {
                        return <div><span>信息比率</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '12',
                    // width: 80,
                    dataIndex: 'n_max_drawdown',
                    title: () => {
                        return <span>最大回撤</span>
                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '13',
                    dataIndex: 'n_mdd_repair_time',
                    // width: 106,
                    title: () => {
                        return <span>最大回撤回补天数（天）</span>
                    }
                },
                {
                    key: '14',
                    dataIndex: 'n_calmar_a',
                    // width: 98,
                    title: () => {
                        return <div><span>Calmar比率</span><br/><Tag color="blue">年化</Tag></div>
                    }
                },
                {
                    key: '15',
                    dataIndex: 'n_VaR',
                    title: () => {
                        return <span>年化VaR（95%置信度）</span>
                    }
                }
            ],
            advisorColumns: [
                {
                    key: '1',
                    dataIndex: 'name',
                    title: '投顾名称',
                    width: 200,
                    fixed: 'left',
                    ellipsis: true
                },
                {
                    key: '2',
                    dataIndex: 'n_return_a',
                    fixed: 'left',
                    ellipsis: true,
                    width: 80,
                    title: () => {
                        return <span>收益率<Tag >年化</Tag></span>
                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '3',
                    dataIndex: 'n_stdev_a',
                    width: 70,
                    fixed: 'left',
                    ellipsis: true,
                    title: () => {
                        return <span>波动率<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '4',
                    dataIndex: 'n_sharpe_a',
                    fixed: 'left',
                    width: 130,
                    ellipsis: true,
                    title: () => {
                        return <span className={`clear-fix`}>
                            Sharpe比率<br />
                            <Input className={`table_columns_rows_input float_left`} onBlur={this.sharpeChange.bind(this)} defaultValue={'2.5'} />
                            <em className={`table_columns_percent float_left`}>%</em><Tag className={`table_tags_rates float_left`}>年化</Tag>
                        </span>
                    }
                },
                {
                    key: '5',
                    dataIndex: 'n_dd_a',
                    width: 108,
                    title: () => {
                        return <span>下行标准差<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '6',
                    dataIndex: 'n_sor_a',
                    width: 100,
                    title: () => {
                        return <span>Sortino比率<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '7',
                    dataIndex: 'n_correlation',
                    width: 94,
                    title: () => {
                        return <span>与基准指数相关系数</span>
                    }
                },
                {
                    key: '8',
                    dataIndex: 'n_spearman',
                    width: 130,
                    title: () => {
                        return <span>
                                    Spearman秩相关
                                    <Cascader allowClear={false} onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} className={`table_select_tips`} options={this.state.options} placeholder={'选择比较基准'} expandTrigger="hover" />
                                </span>
                    }
                },
                {
                    key: '9',
                    dataIndex: 'n_alpah',
                    width: 80,
                    title: () => {
                        return <span>Alpha<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '10',
                    dataIndex: 'n_beta',
                    width: 80,
                    title: () => {
                        return <span>Beta系数</span>
                    }
                },
                {
                    key: '11',
                    dataIndex: 'n_info_a',
                    width: 80,
                    title: () => {
                        return <span>信息比率<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '12',
                    width: 80,
                    dataIndex: 'n_max_drawdown',
                    title: () => {
                        return <span>最大回撤</span>
                    },
                    render: t => isNaN(t) ? t : `${parseFloat(t * 100).toFixed(2)}%`
                },
                {
                    key: '13',
                    dataIndex: 'n_mdd_repair_time',
                    width: 106,
                    title: () => {
                        return <span>最大回撤回补天数（天）</span>
                    }
                },
                {
                    key: '14',
                    dataIndex: 'n_calmar_a',
                    width: 98,
                    title: () => {
                        return <span>Calmar比率<Tag >年化</Tag></span>
                    }
                },
                {
                    key: '15',
                    dataIndex: 'n_VaR',
                    width: 120,
                    title: () => {
                        return <span>年化VaR（95%置信度）</span>
                    }
                }
            ]
        }
    }

    displayRender (label) {
        return label[label.length - 1]
    }

    valueChange (value) {
        const val = value[value.length - 1]
        const param = {...this.props._params, compareRule: val}
        this._initData(this.props.type, param)
    }

    sharpeChange (value) {
        const val = value.target.value
        if (val) {
            const param = {...this.props._params, noRiskReturnRate: val}
            this._initData(this.props.type, param)
        }

    }

    async componentDidMount () {
        // const options = await this._cascaderTags()
        await this.setState({riskTableData:this.props.riskData})

    }

    async _initData (params) {
        params = {...this.state.riskParams, ...params}
        this.setState({loading: true, riskParams: params}, async () => {
            if (this.props._initData) {
                const result = await this.props._initData(params)
                if (result.hasOwnProperty('result')) {
                    let data = []
                    let datas = []
                    data.push(result.result)

                    if (data.length > 0 && data[0].hasOwnProperty('name_list')) {
                        console.log(data, '-----')
                        data.forEach((item) => {
                            const nameSpace = item['name_list']
                            let keys = Object.keys(item)

                            nameSpace.forEach((its, ind) => {
                                let values = {}
                                values['key'] = ind
                                values['name'] = its
                                keys.forEach(it => {
                                    if (it !== 'name_list' && it !== 'id') {
                                        values[it] = isNaN(item[it][its]) ? item[it][its] : item[it][its]
                                    }
                                })
                                datas.push(values)
                            })
                        })

                    }
                    console.log(datas)
                    this.setState({dataList: datas, loading: false})
                }
            }
        })
    }



    render () {
        return (
        <Spin spinning={this.state.loading} delay={10} tip={`loading`}>
            <Table dataSource={this.state.dataList} columns={this.state.columns} pagination={false} scroll={{x:2200}} />
        </Spin>)
    }
}
