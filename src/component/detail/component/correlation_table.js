import React, { Component } from 'react'
import { Table } from 'antd'

export default class CorrelationTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            columns: [
                {
                    title: '序号',
                    dataIndex: 'serial',
                    width: 80,
                },
                {
                    title: '名称',
                    dataIndex: 'name'
                },
                {
                    title: '1',
                    dataIndex: 'number1',
                },
                {
                    title: '2',
                    dataIndex: 'number2'
                },
                {
                    title: '3',
                    dataIndex: 'number3'
                }
            ]
        }
    }

    async _initData (params) {
        if (this.props._productCorrelation) {
            const data = await this.props._productCorrelation(params)
            let dataSource = []
            let cols = []
            if (data) {
                // console.log(data)
                const names = data.name
                names.forEach((item, index) => {
                    let tep = [item].concat(data.corr[item])
                    let obj = {key: index, serial: index + 1}
                    let colObj = []
                    let cl = [{title: '序号', dataIndex: 'serial', key: '0', width: 80}]

                    tep.forEach((items, ind) => {

                        ind === 0 ? obj['name'] =items : obj[`number${ind}`] = items
                        if (ind === 0) {
                            cl.push({
                                [`title`]: '名称',
                                [`key`]: ind + 1,
                                [`dataIndex`]: 'name',
                                [`width`]: 200
                            })
                        } else {
                            cl.push({
                                [`title`]: `${ind}`,
                                [`key`]: ind + 1,
                                [`dataIndex`]: `number${ind}`,
                                render: (r, t, index) => {
                                    let res = ``
                                    if (r === 1) {
                                        res = <span className={``}>{r}</span>
                                    } else if (r < 0.3) {
                                        res = <span className={`table_td_bg_green`}>{r}</span>
                                    } else if (r > 0.8){
                                        res = <span className={`table_td_bg_red`}>{r}</span>
                                    } else {
                                        res = <span className={``}>{r}</span>
                                    }
                                    return res
                                }
                            })
                        }

                        colObj.concat(cl)
                    })
                    cols.push(cl)
                    dataSource.push(obj)
                })

                console.log(cols[0])
                if(dataSource.length > 0) {
                    await this.setState({columns: cols[0], dataSource})
                }
            }
        }
    }

    async componentDidMount () {
        await this._initData()
    }

    render () {
        return <Table className={`correlation_table_performance`} bordered columns={this.state.columns} dataSource={this.state.dataSource} pagination={false} />
    }
}
