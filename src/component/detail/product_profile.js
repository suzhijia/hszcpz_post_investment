import React, { Component } from 'react'
import { Tabs } from 'antd'
import DetailsStyle from './detail.module.sass'
import ListTable from '../../component/common_table/details/list_table'
import RecordList from '../../component/common_table/details/rate_record'
import ProductPublish from '../../component/common_table/details/product_publish'
import Contract from './contract'

export default class ProductProfile extends Component {
    render () {
        return (<div className={`analysis_container container_bg_fff`}>
            <Tabs defaultActiveKey={`1`} className={`${DetailsStyle.tab_container} profile_tabs`}>
                <Tabs.TabPane tab={`合同概况`} key={`1`}>
                    <Contract />
                </Tabs.TabPane>
                <Tabs.TabPane tab={`申赎记录`} key={`2`}>
                    <ListTable />
                </Tabs.TabPane>
                <Tabs.TabPane tab={`产品公告`} key={`3`}>
                    <ProductPublish />
                </Tabs.TabPane>
                <Tabs.TabPane tab={`分红记录`} key={`4`}>
                    <RecordList />
                </Tabs.TabPane>
            </Tabs>
        </div>)
    }
}
