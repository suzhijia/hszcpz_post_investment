import React, { Component } from 'react'


const areaColors = [
  'rgba(44, 129, 202, 0.3)',
  'rgba(245, 110, 100, 0.3)',
  'rgba(255, 203, 46, 0.3)',
  'rgba(191, 76, 212, 0.3)',
  'rgba(110, 199, 231, 0.3)',
  'rgba(243, 163, 158, 0.3)',
  'rgba(250, 226, 154, 0.3)',
  'rgba(213, 163, 224, 0.3)'
]
const chartsColors = ['#2c81ca', '#f56e64', '#ffcb2e', '#bf4cd4', '#6ec7e7', '#f3a39e', '#fae29a', '#d5a3e0',]
const options1= {
  grid : {
     left: '3%',
     right: '4%',
     bottom: '10%',
     containLabel: true
  },
 
  tooltip : {
      trigger: 'axis',
      axisPointer: {
          type: 'cross',
          animation: false,
          label: {
              backgroundColor: '#505765'
          }
      },
    
  },
  legend: {
     left:'center',
     top:10,
     data:[],
  },
  dataZoom: [
      {
          show: true,
          realtime: true,
          start: 0,
          end: 100
      },
      {
          type: 'inside',
          realtime: true,
          start: 0,
          end: 100
      }
  ],
  xAxis : {
          type : 'category',
          name: '',
          boundaryGap : true,
          axisLine: {onZero: false},
          data : []
      },
  yAxis: {
          name: '累计收益率(%)',
          type: 'value',
          axisLine:{
             show:false
         },
          splitLine:{show:true}
         
      },
  series: [
      {
          name:'鹏华资产-清水源',
          type:'line',
          animation: false,
          lineStyle: {
              width: 1
          },
         
          data:[
             1,2,3,4,5
          ]
      },
      {
          name:'沪深300',
          type:'line',
        
          animation: false,
          
          lineStyle: {
              width: 1
          },
          data:[
             10,20,30,40,70
          ]
         
      }
  ]
};
const options2 = {
  grid : {
      left: '3%',
      right: '4%',
      bottom: '10%',
      containLabel: true
  },
tooltip: {
      trigger: 'axis',
      formatter: (value) => {
          let itemDesc = `${value[0].name}<br />`
          value.forEach(item => {
              itemDesc += `${item.marker}${item.seriesName}：${!isNaN(item.value)? `${parseFloat(item.value).toFixed(2)}%` : '--'}<br />`
          })

          return itemDesc
      },
      axisPointer: {
          type: 'line',
          lineStyle: {
              width: 60,
              color: 'rgba(0, 0, 0, 0.1)'
          },
          z: -1
      }
  },
  legend: {
      left:'center',
      top:10,
      data:['鹏华资产-清水源','沪深300'],
      
      
  },
  
  xAxis :
      {
          type : 'category',
          name: '',
          boundaryGap : false,
          axisLine: {onZero: false},
          data : [
              "2010-01-10","2010-02-10","2010-03-10","2010-04-10","2010-05-10",
          ]
      },
  yAxis: {
          name: '动态回撤(%)',
          type: 'value',
          axisLine:{
              show:false
          },
          splitLine:{show:true}
          
      },
  series: [
      {
          name:'鹏华资产-清水源',
          type:'line',
          animation: false,
          lineStyle: {
              width: 1
          },
          areaStyle: {},
          
          data:[
              -100,-222,-583,-564,-125
          ]
      },
      {
          name:'沪深300',
          type:'line',
      
          animation: false,
          
          lineStyle: {
              width: 1
          },
          areaStyle: {},
          data:[
              -310,-420,-130,-240,-770
          ]
          
      }
  ]
};
const options3 = {
  xAxis: {
      type: 'category',
      name: '收益率区间(%)',
      data: []
  },
  yAxis: {
      name: '占比(%)',
      type: 'value',
      axisLine:{
          show:false
      },
      splitLine:{show:true}
},
tooltip : {
  trigger: 'axis',
  axisPointer: {
      type: 'cross',
      animation: false,
      label: {
          backgroundColor: '#505765'
      }
  },
    formatter: (val) => {
        const formatterData = val.filter((item) => { if (item.seriesType === 'line') { return item } });
        let itemDesc = `${formatterData[0].name}<br />`
        formatterData.forEach(item => {
            itemDesc += `${item.marker}${item.seriesName}：${!isNaN(item.value)? `${parseFloat(item.value).toFixed(2)}%` : '--'}<br />`
        })
        return itemDesc              
  }

},
  series: []
};
const monthColumns = [
  {
    title: '',
    key: '0',
    dataIndex: 'tDate',
    render: (text, recoder, indx) => {
        if (text === 'avg') {
            text = '均值'
        }
        return text
    }
},
{
    title: '一月',
    key: '1',
    dataIndex: '01',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '二月',
    key: '2',
    dataIndex: '02',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '三月',
    key: '3',
    dataIndex: '03',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '四月',
    key: '4',
    dataIndex: '04',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '五月',
    key: '5',
    dataIndex: '05',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '六月',
    key: '6',
    dataIndex: '06',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '七月',
    key: '7',
    dataIndex: '07',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '八月',
    key: '8',
    dataIndex: '08',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '九月',
    key: '9',
    dataIndex: '09',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '十月',
    key: '10',
    dataIndex: '10',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '十一月',
    key: '11',
    dataIndex: '11',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '十二月',
    key: '12',
    dataIndex: '12',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
},
{
    title: '全年',
    key: '13',
    dataIndex: 'nYearReturn',
    render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
}
]
const historyColumns = [
  {
      title: '',
      key: '1',
      width: '130px',
      dataIndex: 'first'
  },
  {
      title: '今年以来',
      key: '2',
      dataIndex: 'firstYear',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  },
  {
      title: '近一个月',
      key: '3',
      dataIndex: 'one',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  },
  {
      title: '近三个月',
      key: '4',
      dataIndex: 'three',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  },
  {
      title: '近六个月',
      key: '5',
      dataIndex: 'six',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  },
  {
      title: '近一年',
      key: '6',
      dataIndex: 'year',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  },
  {
      title: '近三年',
      key: '7',
      dataIndex: 'secondYear',
      render: (text, record, index) => {
          if (record.key === 0) {
              return isNaN(text) ? text : parseFloat(text * 100).toFixed(2) + '%'
          } else {
              return isNaN(text) ? text : parseFloat(text).toFixed(2)
          }
      }
  }
]
const rankColumns = [
  {
      key: '1',
      dataIndex: 'yearDate',
      title: '',
      width: '120px'
  },
  {
      key: '2',
      dataIndex: 'nReturn',
      title: '收益率',
      render: (t) => t ? parseFloat(t).toFixed(2) + '%' : '--'
  },
  {
      key: '3',
      dataIndex: 'return',
      title: '排名',
      render: (t) => t ? t : '--'
  },
  {
      key: '4',
      dataIndex: 'nMaxDrawdown',
      title: '最大回撤',
      render: (t) => t ? parseFloat(t).toFixed(2) + '%' : '--'
  },
  {
      key: '5',
      dataIndex: 'max_drawdown',
      title: '排名',
      render: (t) => t ? t : '--'
  },
  {
      key: '6',
      dataIndex: 'nSharpeA',
      title: '夏普比率',
      render: (t) => t ? parseFloat(t).toFixed(2) : '--'
  },
  {
      key: '7',
      dataIndex: 'sharpe_a',
      title: '排名',
      render: (t) => t ? t : '--'
  }
]


export { areaColors, chartsColors, options1, options2, options3, rankColumns, historyColumns, monthColumns };