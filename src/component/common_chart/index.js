import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import { message, Empty } from 'antd'

export default class CommonCharts extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            chartShow: true
        }
    }

    async setOpitions (options) {
        const { idName } = this.props
        const echartsDom = document.getElementById(idName)
        let echart = echarts.init(echartsDom)

        if (options) {
            if (options.series.length > 0) {
                this.setState({chartShow: false})
                echart.clear()
                echart.setOption(options)
            } else {
                this.setState({chartShow: true})
            }
        } else {
            this.setState({chartShow: false})
            message.destroy()
            message.error('图表配置项设置错误')
        }

    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        const { idName, styles } = this.props
        return (
            <div className={`position_relative`}>
                <div id={idName} style={{ visibility: !this.state.chartShow ? 'visible' : 'hidden', ...styles }}></div>
                <Empty style={{visibility: this.state.chartShow ? 'visible' : 'hidden'}} className={`empty_charts_content`} image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
            )
    }
}
