import React, { Component } from 'react'
import NoneTitle from '../../common_title/none_border_header'
import { Table, message, Button } from 'antd'
import { _getPublishList } from '../../../apis/details/index'
import url from 'url'
import moment from 'moment'
export default class ProductPublish extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: [],
            baseParams: {
                currentPage: 1,
                sumCount: '',
                pageSize: 10,
                token: '123456',
                total: 0
            },
            loading: true,
            pageSize: 10,
            pageNo: 1,
            total: 0,
            columns: [
                {
                    title: '公告名称',
                    key: '1',
                    dataIndex: 'cName'
                },
                {
                    title: '公告内容',
                    key: '2',
                    dataIndex: 'content'
                },
                {
                    title: '公告时间',
                    key: '3',
                    dataIndex: 'tDate',
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    title: '附件',
                    key: '4',
                    dataIndex: 'fileName'
                },
                {
                    title: '下载',
                    key: '5',
                    dataIndex: 'fileUrl',
                    render: (record, i) => <Button onClick={() => this.handleDownload(record)} ghost type='primary'>下载</Button>
                }
            ]
        }
    }

    handleDownload (url) {
        window.open(url, '_blank')
    }
    async componentDidMount() {
        const { query } = url.parse(window.location.href, true)
        await this._getData({fund_id: query.id, page_no: this.state.pageNo, page_size: this.state.pageSize})
    }

    async _getData(params) {
        let param = {...params}
        this.setState({params: param, loading: true})
        const data = await _getPublishList(param)
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        let result = data.result

        if (data.status) {
            result.publishList.forEach((item, index) => item.key = index)
            this.setState({
                dataSource: result.publishList,
                total: result.sumCount,
                pageNo: result.pageNo
           })
        } else {
            message.info(data.errorReason)
            return
        }
        this.setState({loading: false})
    }

    tableChange = (pagination, filter, sorter, e) => {
        console.log('tableChange', pagination, filter, sorter, e)
        const { pageSize, current } = pagination
        const params = {...this.state.params, ...{page_size: pageSize, page_no: current}}
        this.setState({params: params, pageSize: pageSize, pageNo: current})
        this._getData(params)
    }

    render() {
        const { columns, dataSource, loading } = this.state
        return (<div className={`mt_20`}>
            <NoneTitle title={`产品公告`} iconStatus={true} />
            <div className={`position_relative mt_20`}>
                <div className={`position_absolute table_total_position`}>
                    共 <span className={`color_number_ff37`}>{this.state.total || 0}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
                </div>
                <Table
                    className={`table_border_top`}
                    dataSource={dataSource}
                    columns={columns}
                    loading={loading}
                    onChange={this.tableChange}
                    pagination={{
                        showSizeChanger: true,
                        showQuickJumper: true,
                        current: this.state.pageNo,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        pageSize: this.state.pageSize,
                        className: 'add_modal_pagination',
                    }} />
            </div>
        </div>)
    }
}
