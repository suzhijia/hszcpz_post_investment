import React, { Component } from 'react'
import NoneTitle from '../../common_title/none_border_header'
import { Table, message, Select, } from 'antd'
import { _getBonusList } from '../../../apis/details/index'
import url from 'url'
import moment from 'moment'


const { query } = url.parse(window.location.href, true)
const Option = Select.Option;
export default class RecordList extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: [],
            baseParams: {
                currentPage: 1,
                sumCount: 0,
                pageSize: 10,
                token: '123456'
            },
            loading: true,
            pageNo: 1,
            pageSize: 10,
            dividend_type: 'all',
            columns: [
                {
                    title: '产品名称',
                    key: '1',
                    dataIndex: 'fundName'
                },
                {
                    title: '权益登记日',
                    key: '2',
                    dataIndex: 'recordDate',
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    title: '红利发放日',
                    key: '3',
                    dataIndex: 'confirmedDate',
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    title: '分红类型',
                    key: '4',
                    dataIndex: 'dividendType',
                    render: text => text ? text === 'cash' ? '现金' : '其他' : '--'
                },
                {
                    title: '每10份红利',
                    key: '5',
                    dataIndex: 'dividendAmt10'
                },
                {
                    title: '分红总金额',
                    key: '6',
                    dataIndex: 'dividendAmt'
                }
            ]
        }
    }


    render () {
        const { columns, dataSource, baseParams, loading } = this.state;
        const { currentPage, sumCount, pageSize } = baseParams;
        const takeDate = new Date().getTime();
        return (<div className={`mt_40`}>
            <NoneTitle title={`分红记录`} iconStatus={true} />
            <div className={`position_relative mt_30`}>
                <div className={`position_absolute table_total_position`}>
                    共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(takeDate).format('YYYY-MM-DD')}</span>
                </div>
                <Select value={this.state.dividend_type} onChange={this.dividendChange.bind(this)} style={{ position: 'absolute', top: '-42px', right: 20, width: 160 }}>
                    <Option value="all">全部</Option>
                    <Option value="mo">母基金分红</Option>
                    <Option value="son">子基金分红</Option>
                </Select>
                <Table
                    className={`table_border_top`}
                    dataSource={dataSource}
                    columns={columns}
                    loading={loading}
                    onChange={this.tableChange}
                    pagination={{ showSizeChanger: true,
                        showQuickJumper: true,
                        current: currentPage,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        pageSize: pageSize,
                        total: sumCount,
                        className: 'add_modal_pagination' }} />
            </div>
        </div>)
    }

    async componentDidMount() {
        await this._getData({fund_id: query.id, page_no: this.state.pageNo, page_size: this.state.pageSize, dividend_type: this.state.dividend_type})
    }
    dividendChange (str) {
        console.log(str)
        this.setState({dividend_type: str}, async () => {
            await this._getData({fund_id: query.id, page_no: this.state.pageNo, page_size: this.state.pageSize, dividend_type: this.state.dividend_type})
        })
    }
    // 初始化
    async _getData(params) {
        let param = {...params}
        this.setState({params: param})
        const data = await _getBonusList(param)
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        let result = data.result
        this.setState({loading: false})
        if (data.status) {
            result.bonusRecord.forEach((item, index) => item.key = index)
            this.setState({
                dataSource: result.bonusRecord,
                sumCount: result.pageNo,
                currentPage: result.current,
                total: result.sumCount
            })
        } else {
            message.info(data.errorReason)
            return
        }

    }
    // 表格更改
    tableChange = (pagination, filter, sorter, e) => {
        console.log('tableChange', pagination, filter, sorter, e)
        const { pageSize, current } = pagination
        const params = {...this.state.params, ...{page_size: pageSize, page_no: current}}
        this.setState({params: params, pageSize: pageSize, pageNo: current})
        this._getData(params)
    }
}
