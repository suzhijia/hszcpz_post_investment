import React, { Component } from 'react'
import { Table } from 'antd'
import moment from "moment";

export default class SubTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            total: 0,
            page_size: 10,
            page_no: 1,
            columns: [
                {
                    dataIndex: 'date',
                    title: '日期',
                    key: '1'
                },
                {
                    dataIndex: 'market_value',
                    title: '净值合计',
                    key: '2',
                    render: text => text ? parseFloat(text).toFixed(2) : '--'
                },
                {
                    dataIndex: 'nav_simulation',
                    title: '模拟单位净值',
                    key: '3',
                    render: text => text ? parseFloat(text).toFixed(4) : '--'
                },
                {
                    dataIndex: 'market_value_simulation',
                    title: '模拟份额合计',
                    key: '4',
                    render: text => text ? parseFloat(text).toFixed(2) : '--'
                },
                {
                    dataIndex: 'win_rate',
                    title: '胜率',
                    key: '5',
                    render: text => text ? parseFloat(String(text * 100)).toFixed(2) + '%' : '--'
                },
                {
                    dataIndex: 'var',
                    title: 'Var（1,0.05）',
                    key: '6',
                    render: text => text ? parseFloat(text).toFixed(2) : '--'
                },
                {
                    dataIndex: 'cvar_list',
                    title: 'Cvar（1,0.05）',
                    key: '7',
                    render: text => text ? parseFloat(text).toFixed(2) : '--'
                }
            ]
        }
    }

    render () {
        return (
            <div className={`position_relative mt_30`}>
                <div className={`position_absolute table_total_position`}>
                    共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
                </div>
                <Table
                    className={`table_border_top`}
                    pagination={{
                        showQuickJumper: true,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        total: this.state.total,
                        showSizeChanger: true
                    }}
                    onChange={this.tableChange.bind(this)}
                    loading={this.state.loading}
                    columns={this.state.columns}
                    dataSource={this.state.dataSource} />
            </div>)
    }

    componentDidMount () {}

    _initData (params) {
        const param = {page_size: this.state.page_size, page_no: this.state.page_no, ...this.state.param, ...params}
        this.setState({param, loading: true}, async () => {
            if(this.props._initData) {
                const data = await this.props._initData(param)
                if (data && data.result && data.status) {
                    console.log(data)
                    const list = data.result
                    list.forEach((item, index) => {
                        item.key = index
                    })

                    this.setState({dataSource: list, loading: false, total: data.sum_count})
                } else {
                    this.setState({dataSource: [], loading: false, total: 0})
                }

            }
        })
    }

    tableChange = (pagination, filter, sorter, e) => {
        // console.log('tableChange', pagination, filter, sorter, e)
        const { current, pageSize } = pagination
        const params = { ...this.state.param, page_no: pagination.current, page_size: pagination.pageSize }
        this.setState({param: params, page_no: current, page_size: pageSize})
        this._initData(params)
    }
}
