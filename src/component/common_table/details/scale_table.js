import React, { Component } from 'react'
import { Table } from 'antd'
import iscroll from "iscroll";
import moment from "moment";

export default class ScaleTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            dataSource: [],
            page_no: 1,
            page_size: 10,
            columns: [
                {
                    dataIndex: 'nav_date',
                    key: '1',
                    title: '市值日期',
                    width: 120,
                    ellipsis: true
                },
                {
                    dataIndex: 'nav_value',
                    key: '2',
                    title: '母基金市值',
                    width: 120,
                    ellipsis: true,
                    render: text => text ? parseFloat(text).toFixed(2) : '--'
                }
            ]
        }
    }

    async componentDidMount() {
        const eleTable = document.getElementById('productScaleTable').querySelector('.ant-table-body')
        eleTable.style.overflow = 'hidden'
    }
    async initData(params) {
        params = {page_no: this.state.page_no, page_size: this.state.page_size,  ...this.state.navParams, ...params}
        await this.setState({loading: true, navParams: params})
        if (this.props._getUnitList) {
            const data = await this.props._getUnitList(params);
            console.log('data', data)
            if (data) {
                let dataSource = data.data
                const names = data.n_name
                dataSource.forEach((item, index) => item.key = index)
                let cl = this.state.columns
                if (cl.length < names.length) {
                    names.forEach((item, index) => {
                        if (index !== 0) {
                            cl.push({
                                title: item,
                                key: index + 2,
                                dataIndex: `product_${index}`,
                                width: 100,
                                ellipsis: true,
                                render: text => text ? parseFloat(text).toFixed(2) : '--'
                            })
                        }
                    })
                }
                this.setState({
                    columns: cl,
                    dataSource: dataSource,
                    sumCount: data.pageNo,
                    currentPage: data.current,
                    total: data.sum_count
                }, () => {
                    new iscroll('#productScaleTable .ant-table-body', {
                        scrollbars: 'custom',
                        mouseWheel: true,
                        preventDefault: false,
                        scrollX: true,
                        scrollY: false,
                        interactiveScrollbars: true,
                        shrinkScrollbars: false,
                        bounce: false
                    })
                })
            }
            await this.setState({loading: false})
        }
    }

    tableChange = (pagination, filter, sorter, e) => {
        // console.log('tableChange', pagination, filter, sorter, e)
        const { current, pageSize } = pagination
        const params = { ...this.state.navParams, page_no: pagination.current, page_size: pagination.pageSize }
        this.setState({navParams: params, page_no: current, page_size: pageSize})
        this.initData(params)
    }

    render () {
        return (<div className={`position_relative mt_30`}>
            <div className={`position_absolute table_total_position`}>
                共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
            </div>
            <Table
                className={`table_border_top`}
                id={`productScaleTable`}
                loading={this.state.loading}
                pagination={{
                    showSizeChanger: true,
                    showQuickJumper: true,
                    current: this.state.page_no,
                    pageSizeOptions: ['10', '20', '50', '100'],
                    pageSize: this.state.page_size,
                    total: this.state.total,
                    className: 'add_modal_pagination'
                }}
                onChange={this.tableChange.bind(this)}
                scroll={{x: 100 * this.state.dataSource.length}}
                columns={this.state.columns} dataSource={this.state.dataSource} />
        </div>)
    }
}
