import React, { Component } from 'react'
import { Table, message } from 'antd'
import moment from 'moment'
import iscroll from 'iscroll'

let temp = {}, dTemp = {}
function cellDMerge (text, arr, col, tempVar) {
    let i = 0
    if (text !== tempVar[col]) {
        tempVar[col] = text
        arr.forEach(item => {
            if (item[col] === tempVar[col]) {
                i += 1
            }
        })
    }
    return i
}

export default class AssetsTable extends Component {
    constructor (props) {
        super(props)

        this.state = {
            loading: false,
            total: 0,
            columns: [
                {
                    dataIndex: 'c_type_name',
                    title: '资产大类',
                    key: '1',
                    width: 110,
                    className: 'table_row_span',
                    render: (text, record, index) => {
                        return {
                            children: text,
                            props: {
                                rowSpan: cellDMerge(record.c_type_name, this.state.dataSource, 'c_type_name', temp)
                            }
                        }
                    }
                },
                {
                    dataIndex: 'proportion',
                    title: '大类占比',
                    key: '2',
                    className: 'table_row_span',
                    width: 80,
                    render: (text, record, index) => {
                        return {
                            children: text,
                            props: {
                                rowSpan: cellDMerge(record.c_type_name, this.state.dataSource, 'c_type_name', dTemp)
                            }
                        }
                    }
                },
                {
                    dataIndex: 'c_subject_name',
                    title: '产品名称',
                    key: '3',
                    ellipsis: true
                },
                {
                    dataIndex: 'c_org_name',
                    title: '产品投顾',
                    key: '4',
                    ellipsis: true,
                    render: text => text ? text : '--'
                },
                {
                    dataIndex: 'start_date',
                    title: '认购时间',
                    key: '5',
                    render: (text) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    dataIndex: 'new_nav_date',
                    title: '最新净值时间',
                    key: '6',
                    width: 110,
                    render: (text) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    dataIndex: 'n_num',
                    title: '认购份额',
                    key: '7'
                },
                {
                    dataIndex: 'n_total_cost',
                    title: '认购成本',
                    key: '8'
                },
                {
                    dataIndex: 'n_unit_market_value',
                    title: '最新净值',
                    key: '9'
                },
                {
                    dataIndex: 'sum_cost',
                    title: '收益率',
                    key: '10',
                },
                {
                    dataIndex: 'n_return',
                    title: '收益',
                    key: '11'
                },
                {
                    dataIndex: 'n_return_m',
                    title: '收益占比',
                    key: '12'
                },
                {
                    dataIndex: 'n_total_market_value',
                    title: '最新市值',
                    key: '13'
                },
                {
                    dataIndex: 'market_value_ratio',
                    title: '最新市值占比（%）',
                    key: '14',
                    width: 140
                },
            ]
        }
    }

    render () {
        return (<div className={`position_relative mt_30`}>
            <div className={`position_absolute table_total_position`}>
                共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
            </div>
            <Table
                id={`productAssetsTable`}
                className={`table_border_top table_rowspan_content table_none_pagination`}
                loading={this.state.loading}
                columns={this.state.columns}
                dataSource={this.state.dataSource}
                pagination={{pageSize: 100}}
                scroll={{y: 500}} />
        </div>)
    }

    componentDidMount () {
        const eleTable = document.getElementById('productAssetsTable').querySelector('.ant-table-body')
        eleTable.style.overflow = 'hidden'
        document.getElementById('productAssetsTable').querySelector('.ant-table-header').style.overflow = 'hidden'
        document.getElementById('productAssetsTable').querySelector('.ant-table-header').style.marginBottom = '0'
    }

    _initData (params) {
        let param = {...this.state.params, ...params}
        this.setState({param, loading: true, params: param}, async () => {
            if (this.props._initData) {
                const data = await this.props._initData(param)
                if (data && data.status && data.result) {
                    let dataSource = data.result
                    dataSource.forEach((item, index)=> {
                        item.key = index
                        item['proportion'] = (item['proportion']) ? isNaN(item['proportion']) ? item['proportion'] : parseFloat(String(item['proportion'])).toFixed(2) + '%' : '--'
                        item['product_advisor'] = isNaN(item['product_advisor']) ? item['product_advisor'] : parseFloat(item['product_advisor']).toFixed(2)
                        item['n_num'] = item['n_num'] ? isNaN(item['n_num']) ? item['n_num'] : parseFloat(item['n_num']).toFixed(2) : '--'
                        item['n_total_cost'] = item['n_total_cost'] ? isNaN(item['n_total_cost']) ? item['n_total_cost'] : parseFloat(item['n_total_cost']).toFixed(2) : '--'
                        item['n_unit_market_value'] = isNaN(item['n_unit_market_value']) ? item['n_unit_market_value'] : parseFloat(item['n_unit_market_value']).toFixed(4)
                        item['sum_cost'] = item['sum_cost'] ? isNaN(item['sum_cost']) ? item['sum_cost'] : parseFloat(String(item['sum_cost'] * 100)).toFixed(2) + '%' : '--'
                        item['n_return'] = item['n_return'] ? isNaN(item['n_return']) ? item['n_return'] : parseFloat(item['n_return']).toFixed(2) : '--'
                        item['n_return_m'] = item['n_return_m'] ? isNaN(item['n_return_m']) ? item['n_return_m'] : parseFloat(String(item['n_return_m'] * 100)).toFixed(2) + '%' : '--'
                        item['n_total_market_value'] = item['n_total_market_value'] ? isNaN(item['n_total_market_value']) ? item['n_total_market_value'] : parseFloat(item['n_total_market_value']).toFixed(2) : '--'
                        item['market_value_ratio'] = item['market_value_ratio'] ? isNaN(item['market_value_ratio']) ? item['market_value_ratio'] : parseFloat(item['market_value_ratio']).toFixed(2) + '%' : '--'
                    })
                    this.setState({dataSource, loading: false, total: dataSource.length}, () => {
                        new iscroll('#productAssetsTable .ant-table-body', {
                            scrollbars: 'custom',
                            mouseWheel: true,
                            preventDefault: false,
                            scrollX: false,
                            scrollY: true,
                            interactiveScrollbars: true,
                            shrinkScrollbars: false,
                            bounce: false
                        })
                    })
                } else {
                    this.setState({dataSource: [], loading: false, total: 0})
                }
            } else {
                message.info('初始化方法为空')
            }
        })
    }
}
