import React, { Component } from 'react'
import { Table, message } from 'antd'
import moment from 'moment'

export default class TargetList extends Component {
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            total: 0,
            getDate: null,
            page_no: 1,
            page_size: 10,
            columns: [
                {
                    dataIndex: 'fund_name',
                    title: '产品名称',
                    key: '1'
                },
                {
                    dataIndex: 'start_date',
                    title: '认购日期',
                    key: '2'
                },
                {
                    dataIndex: 'valu_date',
                    title: '估值日期',
                    key: '3'
                },
                {
                    dataIndex: 'n_return',
                    title: '期间收益率',
                    key: '4'
                },
                {
                    dataIndex: 'n_return_a',
                    title: '年化收益率',
                    key: '5'
                },
                {
                    dataIndex: 'n_stdev_a',
                    title: '年化波动率',
                    key: '6'
                },
                {
                    dataIndex: 'n_max_drawdown',
                    title: '最大回撤',
                    key: '7'
                },
                {
                    dataIndex: 'n_drawdown',
                    title: '当前回撤',
                    key: '8'
                },
                {
                    dataIndex: 'n_sharpe_a',
                    title: '年化夏普',
                    key: '9'
                },
                {
                    dataIndex: 'n_correlation',
                    title: '与母基金相关性',
                    key: '10'
                },
            ]
        }
    }

    render () {
        return (<div className={`position_relative mt_30`}>
                <div className={`position_absolute table_total_position`}>
                    共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.getDate).format('YYYY-MM-DD')}</span>
                </div>
                <Table
                    className={`table_border_top`}
                    loading={this.state.loading}
                    columns={this.state.columns}
                    dataSource={this.state.dataSource}
                    onChange={this.tableChange.bind(this)}
                    pagination={{
                        showSizeChanger: true,
                        showQuickJumper: true,
                        current: this.state.page_no,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        pageSize: this.state.page_size,
                        total: this.state.total
                    }} />
        </div>)
    }

    componentDidMount () {}

    _initData (params) {
        let param = { page_no: this.state.page_no, page_size: this.state.page_size, ...this.state.param, ...params }
        this.setState({ param, loading: true}, async () => {
            if (this.props._initData) {
                const data = await this.props._initData(param)
                if (data && data.result) {
                let list = data.result
                list.forEach((item, index) => {
                    item.key = index
                    item[`fund_name`] = item[`fund_name`] ? item[`fund_name`] : '--'
                    item[`start_date`] = item[`start_date`] ? isNaN(item[`start_date`]) ? item[`start_date`] : parseFloat(item[`start_date`]).toFixed(2) : '--'
                    item[`valu_date`] =item[`valu_date`] ? isNaN(item[`valu_date`]) ? item[`valu_date`] : parseFloat(item[`valu_date`]).toFixed(2) : '--'
                    item[`n_return`] = isNaN(item[`n_return`]) ? item[`n_return`] : parseFloat(String(item[`n_return`] * 100)).toFixed(2) + '%'
                    item[`n_return_a`] = isNaN(item[`n_return_a`]) ? item[`n_return_a`] : parseFloat(String(item[`n_return_a`] * 100)).toFixed(2) + '%'
                    item[`n_stdev_a`] = isNaN(item[`n_stdev_a`]) ? item[`n_stdev_a`] : parseFloat(String(item[`n_stdev_a`] * 100)).toFixed(2) + '%'
                    item[`n_max_drawdown`] = isNaN(item[`n_max_drawdown`]) ? item[`n_max_drawdown`] : parseFloat(String(Math.abs(item[`n_max_drawdown`] *100))).toFixed(2) + '%'
                    item[`n_drawdown`] = isNaN(item[`n_drawdown`]) ? item[`n_drawdown`] : parseFloat(String(Math.abs(item[`n_drawdown`] * 100))).toFixed(2) + '%'
                    item[`n_sharpe_a`] = isNaN(item[`n_sharpe_a`]) ? item[`n_sharpe_a`] : parseFloat(item[`n_sharpe_a`]).toFixed(2)
                    item[`n_correlation`] = isNaN(item[`n_correlation`]) ? item[`n_correlation`] : parseFloat(item[`n_correlation`]).toFixed(4)

                })

                this.setState({dataSource: list, loading: false, total: data.sumCount, getDate: data.getDate})
                } else {
                    this.setState({ loading: false, dataSource: [], total: 0 })
                }
            } else {
                message.info('初始化方法为空')
            }
        })
    }

    tableChange (pagination, filter, sorter, e) {
        // console.log('tableChange', pagination, filter, sorter, e)
        const { pageSize, current } = pagination
        const params = {...this.state.param, ...{page_size: pageSize, page_no: current}}
        this.setState({param: params, page_size: pageSize, page_no: current}, () => {
            this._initData(params)
        })
    }

}
