import React, { Component } from 'react'
import { Table, Input } from 'antd'
import detailsStyle from "../../../detail/scences.module.sass";

function renderFunction (t, r) {
    let text = ''
    switch (r['指标']) {
        case '周期数':
            return t
        case 'Calmar比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case 'Sortino比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case '夏普比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        default:
            if (typeof t === 'object' && Array.isArray(t)) {
                t.forEach((item, index) => {
                    if (index < t.length - 1) {
                        text += `${parseFloat(String(item * 100)).toFixed(2)}% ~`
                    } else {
                        text += `${parseFloat(String(item * 100)).toFixed(2)}%`
                    }
                })
            } else {
                text = `${parseFloat(String(t * 100)).toFixed(2)}%`
            }

            return text
    }
}
const socketColumns = [
    {key: '1', dataIndex: '指标', title: '指标', width: 260,
        className:`${detailsStyle.bondTableBgc1}`,
        render: (t, r) => {
            if (t === '夏普比率') {
                const renders =  <span>
                    {t}
                    <Input className={`table_columns_rows_input`} id={`futureId`} defaultValue={'2.5'} />
                    <em className={`table_columns_percent`}>%</em>
                </span>
                return renders
            } else {
                return t
            }
        }},
    {key: '2', dataIndex: '市场上涨', title: '高波动', className:`${detailsStyle.bondTableBgc2}`, render:  (t, r) => renderFunction(t, r)},
    {key: '3', dataIndex: '市场震荡', title: '震荡',  className:`${detailsStyle.bondTableBgc3}`, render: (t, r) => renderFunction(t, r)},
    {key: '4', dataIndex: '市场下跌', title: '低波动', className:`${detailsStyle.tableBgc3}`, render:  (t, r) => renderFunction(t, r)}
]

const columnsText = {
    conf_intveral: {name: '周收益率区间范围'},
    n_stdev_a: {name: '波动率'},
    return_ratio: {name: '场景收益占比'},
    calmar: {name: 'Calmar比率'},
    max_drawdown: {name: '最大回撤'},
    n_count: {name: '周期数'},
    sharpe: {name: '夏普比率'},
    sortino: {name: 'Sortino比率'},
}

export default class FutureTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: []
        }
    }

    _initData (data) {

        if (data) {
            let dataKey = Object.keys(data)
            let dataList = []

            dataKey.forEach((item, index) => {
                if (item !== 'n_count') {
                    dataList.push({
                        key: index,
                        '指标': columnsText[item].name,
                        '市场上涨': data[item]['high'],
                        '市场震荡': data[item]['mid'],
                        '市场下跌': data[item]['low']
                    })
                } else {
                    dataList.unshift({
                        key: index,
                        '指标': columnsText[item].name,
                        '市场上涨': data[item]['high'],
                        '市场震荡': data[item]['mid'],
                        '市场下跌': data[item]['low']
                    })
                }
            })

            if (dataList.length > 0) {
                this.setState({dataSource: dataList})
            }

        }
    }

    render () {
        return <Table bordered columns={socketColumns} dataSource={this.state.dataSource} pagination={false} />
    }
}
