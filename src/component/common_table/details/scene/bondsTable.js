import React, { Component } from 'react'
import { Table, Input } from 'antd'
import detailsStyle from "../../../detail/scences.module.sass";

function renderFunction (t, r) {
    let text = ''
    switch (r['指标']) {
        case '周期数':
            return t
        case 'Calmar比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case 'Sortino比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        case '夏普比率':
            return t ? isNaN(t) ? '--' : parseFloat(t).toFixed(2) : '--'
        default:
            if (typeof t === 'object' && Array.isArray(t)) {
                t.forEach((item, index) => {
                    if (index < t.length - 1) {
                        text += `${parseFloat(item * 100).toFixed(2)}% ~`
                    } else {
                        text += `${parseFloat(String(item * 100)).toFixed(2)}%`
                    }
                })
            } else {
                text = `${parseFloat(t * 100).toFixed(2)}%`
            }

            return text
    }
}
const socketColumns = [
    {key: '1', dataIndex: '指标', title: '指标', width: 260,
        className:`${detailsStyle.bondTableBgc1}`,
        render: (t, r) => {
            if (t === '夏普比率') {
                const renders =  <span>
                    {t}
                    <Input className={`table_columns_rows_input`} id={`bondsId`} defaultValue={'2.5'} />
                    <em className={`table_columns_percent`}>%</em>
                </span>
                return renders
            } else {
                return t
            }
        }},
    {key: '2', dataIndex: 'upstream', className:`${detailsStyle.bondTableBgc2}`, title: '市场上涨', render:  (t, r) => renderFunction(t, r)},
    {key: '3', dataIndex: 'expand', className:`${detailsStyle.bondTableBgc3}`, title: '市场震荡', render: (t, r) => renderFunction(t, r)},
    {key: '4', dataIndex: 'narrowing', className:`${detailsStyle.bondTableBgc4}`, title: '市场下跌', render:  (t, r) => renderFunction(t, r)},
    {key: '5', dataIndex: 'downstream', className:`${detailsStyle.bondTableBgc5}`, title: '市场下跌', render:  (t, r) => renderFunction(t, r)}
]

const columnsText = {
    conf_intveral: {name: '周收益率区间范围'},
    n_stdev_a: {name: '波动率'},
    return_ratio: {name: '场景收益占比'},
    calmar: {name: 'Calmar比率'},
    max_drawdown: {name: '最大回撤'},
    n_count: {name: '周期数'},
    sharpe: {name: '夏普比率'},
    sortino: {name: 'Sortino比率'},
}

const dimensionDesc = [
    {
        n_long_yield: {name: '长债收益率', status: '上行'},
        n_base_rate: {name: '货币市场基准利率', status: '上行'},
        n_IVA_yoy: {name: '工业增加值当月同比', status: '上行'},
        n_PMI: {name: 'PMI', status: '上行'},
        n_CPI_yoy: {name: 'CPI当月同比', status: '上行'},
        n_PMI_yoy: {name: 'PPI当月同比', status: '上行'},
        n_M2_yoy: {name: 'M2同比', status: '上行'},
        n_M1_M2_diff: {name: 'M1-M2增速差', status: '上行'},
        n_social_finance_yoy: {name: '社融存量同比', status: '上行'},
        n_base_rate_volatility: {name: '货币市场基准利率波动', status: '扩大'},
        n_term_spread: {name: '期限利差', status: '扩大'},
        n_long_credit_spread: {name: '长期信用利差', status: '扩大'},
    },
    {
        n_long_yield: {name: '长债收益率', status: '下行'},
        n_base_rate: {name: '货币市场基准利率', status: '下行'},
        n_IVA_yoy: {name: '工业增加值当月同比', status: '下行'},
        n_PMI: {name: 'PMI', status: '下行'},
        n_CPI_yoy: {name: 'CPI当月同比', status: '下行'},
        n_PMI_yoy: {name: 'PPI当月同比', status: '下行'},
        n_M2_yoy: {name: 'M2同比', status: '下行'},
        n_M1_M2_diff: {name: 'M1-M2增速差', status: '下行'},
        n_social_finance_yoy: {name: '社融存量同比', status: '下行'},
        n_base_rate_volatility: {name: '货币市场基准利率波动', status: '收窄'},
        n_term_spread: {name: '期限利差', status: '收窄'},
        n_long_credit_spread: {name: '长期信用利差', status: '收窄'},
    }
]

export default class BondsTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: []
        }
    }

    _initData (data, dimension, lastDimension) {
        if (data) {
            let dataKey = Object.keys(data)
            let dataList = []

            if (dimension && lastDimension) {
                socketColumns[1].title = `${dimensionDesc[0][dimension].name}${dimensionDesc[0][dimension].status} & ${dimensionDesc[0][lastDimension].name}${dimensionDesc[0][lastDimension].status}`
                socketColumns[2].title = `${dimensionDesc[0][dimension].name}${dimensionDesc[0][dimension].status} & ${dimensionDesc[1][lastDimension].name}${dimensionDesc[1][lastDimension].status}`
                socketColumns[3].title = `${dimensionDesc[1][dimension].name}${dimensionDesc[1][dimension].status} & ${dimensionDesc[0][lastDimension].name}${dimensionDesc[0][lastDimension].status}`
                socketColumns[4].title = `${dimensionDesc[1][dimension].name}${dimensionDesc[1][dimension].status} & ${dimensionDesc[1][lastDimension].name}${dimensionDesc[1][lastDimension].status}`
            }

            dataKey.forEach((item, index) => {
                if (item !== 'n_count') {
                    dataList.push({
                        key: index,
                        '指标': columnsText[item].name,
                        'upstream': data[item]['1'],
                        'expand': data[item]['2'],
                        'narrowing': data[item]['3'],
                        'downstream': data[item]['4']
                    })
                } else {
                    dataList.unshift({
                        key: index,
                        '指标': columnsText[item].name,
                        'upstream': data[item]['1'],
                        'expand': data[item]['2'],
                        'narrowing': data[item]['3'],
                        'downstream': data[item]['4']
                    })
                }
            })

            if (dataList.length > 0) {
                this.setState({dataSource: dataList})
            }

        }
    }

    render () {
        return <Table bordered columns={socketColumns} dataSource={this.state.dataSource} pagination={false} />
    }
}
