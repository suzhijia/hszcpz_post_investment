import React, { Component } from 'react'
import { Table } from 'antd'
// import NeutralStyle from './style/sceneNeutral.module.sass'

let columns = [{
    title: '基差',
    children: [
        {title: '', dataIndex: 'neutralTitle', key: '1', width: 120 },
        {title: '<5', dataIndex: 'neutral_5', key: '2', width: 120 },
        {title: '5-10', dataIndex: 'neutral_10', key: '3', width: 120 },
        {title: '10-15', dataIndex: 'neutral_15', key: '4', width: 120 },
        {title: '15-20', dataIndex: 'neutral_20', key: '5', width: 120 },
        {title: '>20', dataIndex: 'neutral_latest', key: '6', width: 120 },
    ]
}
]

const columnsText = {
    n_stdev_a: {name: '年化周波动率'},
    n_return_a: {name: '年化周收益率'},
    w_rate: {name: '胜率'},
    w_l_rate: {name: '盈亏次数比'},
    n_count: {name: '场景区间期数'},
    n_change: {name: '涨跌幅'}
}

const tableTitle = {
    n_basis_cost: '年化基差成本',
    n_sse50_change: '上证50涨跌幅',
    n_hs300_change: '沪深300涨跌幅',
    n_csi500_change: '中证500涨跌幅',
    n_csi800_change: '中证800涨跌幅',
    n_sse_change: '上证指数涨跌幅',
    n_gem_change: '创业板指数涨跌幅',
    n_ls_diff: '大小市值涨跌幅之差',
    n_sse_volatility: '上证指数波动率',
    n_sse_liquidity: '上证指数流动性',
}
export default class marketBasis extends Component {
    constructor (props) {
        super(props)
        this.state = {
            columns: columns,
            dataList: []
        }
    }

    async _initData (data, type) {
        let dataList = []
        let dataKeys = []
        if (data) {
            dataKeys = Object.keys(data)

            let stdev = {}, returnn = {}
            Object.keys(data.n_stdev_a).forEach((item, index) => {
                stdev[item] = `${parseFloat(data.n_stdev_a[item] * 100).toFixed(2)}%`
                returnn[item] = `${parseFloat(data.n_return_a[item] * 100).toFixed(2)}%`
            })
            data.n_stdev_a = stdev
            data.n_return_a = returnn
            switch (type) {
                case 'n_basis_cost':
                    columns = [{
                        title: tableTitle[type],
                        children: [
                            {title: '', dataIndex: 'neutralTitle', key: '1', width: 120 },
                            {title: '<5', dataIndex: 'neutral_5', key: '2', width: 120 ,
                                render: (text, record, index) => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                } },
                            {title: '5-10', dataIndex: 'neutral_10', key: '3', width: 120, render: (text, record, index)  => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                            {title: '10-15', dataIndex: 'neutral_15', key: '4', width: 120, render: (text, record, index) => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                } },
                            {title: '15-20', dataIndex: 'neutral_20', key: '5', width: 120, render: (text, record, index)  => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                            {title: '>20', dataIndex: 'neutral_latest', key: '6', width: 120 , render: (text, record, index)  => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                        ]
                    }
                    ]

                    dataKeys.forEach((item, index) => {
                        dataList.push({
                            key: index,
                            neutralTitle: columnsText[item] ? columnsText[item].name : '',
                            neutral_5: isNaN(data[item]['<5']) ? data[item]['<5'] : data[item]['<5'],
                            neutral_10: isNaN(data[item]['5-10']) ? data[item]['5-10'] : data[item]['5-10'],
                            neutral_15: isNaN(data[item]['10-15']) ? data[item]['10-15'] : data[item]['10-15'],
                            neutral_20: isNaN(data[item]['15-20']) ? data[item]['15-20'] : data[item]['15-20'],
                            neutral_latest: isNaN(data[item]['>20']) ? data[item]['>20'] : data[item]['>20'],
                        })
                    })

                    break
                default:
                    columns = [{
                        title: tableTitle[type],
                        children: [
                            {title: '', dataIndex: 'neutralTitle', key: '1', width: 120},
                            {title: '极大值', dataIndex: 'high', key: '2', width: 120, render: (text, record, index)  => {
                                    console.log(record)
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                            {title: '正常', dataIndex: 'mid', key: '3', width: 120, render: (text, record, index)  => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                            {title: '极小值', dataIndex: 'low', key: '4', width: 120, render: (text, record, index)  => {
                                    if (record.neutralTitle === '场景区间期数') {
                                        return text
                                    } else {
                                        return isNaN(text) ? text : parseFloat(text).toFixed(2)
                                    }
                                }},
                        ]
                    }
                    ]

                    dataKeys.forEach((item, index) => {
                        dataList.push({
                            key: index,
                            neutralTitle: columnsText[item] ? columnsText[item].name : '',
                            high: isNaN(data[item]['高']) ? data[item]['高'] : data[item]['高'],
                            mid: isNaN(data[item]['中']) ? data[item]['中'] : data[item]['中'],
                            low: isNaN(data[item]['低']) ? data[item]['低'] : data[item]['低']
                        })
                    })
                    break
            }

            await this.setState({columns: columns, dataList: dataList})
        }
    }

    render () {
        return <Table className={``} bordered columns={this.state.columns} dataSource={this.state.dataList} pagination={false} />
    }
}
