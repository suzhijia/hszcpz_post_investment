import React, { Component } from 'react'
import { Table } from 'antd'
import moment from 'moment'
import iscroll from 'iscroll'

export default class NavTable extends Component{
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            dataSource: [],
            total: 0,
            page_size: 10,
            page_no: 1,
            baseParams: {
                currentPage: 1,
                sumCount: '',
                pageSize: 10,
                token: '123456'
            },
            columns: [
                {
                    dataIndex: 'nav_date',
                    title: '净值日期',
                    key: '1',
                    width: 140,
                    ellipsis: true
                },
                {
                    dataIndex: 'nav_value',
                    title: '母基金单位净值',
                    key: '2',
                    ellipsis: true,
                    render: text => text ? parseFloat(text).toFixed(4) : '--'
                }
            ]
        }
    }

    async componentDidMount() {
        const eleTable = document.getElementById('productNavTable').querySelector('.ant-table-body')
        eleTable.style.overflow = 'hidden'
    }
    async initData(params) {
        params = {page_no: this.state.page_no, page_size: this.state.page_size,  ...this.state.navParams, ...params}
        await this.setState({loading: true, navParams: params})
        if (this.props._getUnitList) {
            const data = await this.props._getUnitList(params);
            if (data) {
                let dataSource = data.data
                const names = data.n_name
                dataSource.forEach((item, index) => item.key = index)
                let cl = this.state.columns
                if (cl.length < names.length) {
                    names.forEach((item, index) => {
                        if (index !== 0) {
                            cl.push({
                                title: item,
                                key: index + 2,
                                dataIndex: `product_${index}`,
                                width: 100,
                                ellipsis: true,
                                render: text => text ? parseFloat(text).toFixed(4) : '--'
                            })
                        }
                    })
                }
                this.setState({
                    columns: cl,
                    dataSource: dataSource,
                    sumCount: data.pageNo,
                    currentPage: data.current,
                    total: data.sum_count
                }, () => {
                    new iscroll('#productNavTable .ant-table-body', {
                        scrollbars: 'custom',
                        mouseWheel: true,
                        preventDefault: false,
                        scrollX: true,
                        scrollY: false,
                        interactiveScrollbars: true,
                        shrinkScrollbars: false,
                        bounce: false
                    })
                })
            }
            await this.setState({loading: false})
        }
    }

    tableChange = (pagination, filter, sorter, e) => {
        // console.log('tableChange', pagination, filter, sorter, e)
        const { current, pageSize } = pagination
        const params = { ...this.state.navParams, page_no: pagination.current, page_size: pagination.pageSize }
        this.setState({navParams: params, page_no: current, page_size: pageSize})
        this.initData(params)
    }

    render() {
        const { dataSource, loading } = this.state
        return (
        <div className={`position_relative mt_30`}>
            <div className={`position_absolute table_total_position`}>
                共 <span className={`color_number_ff37`}>{this.state.total}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
            </div>
            <Table
                id={`productNavTable`}
                className={`table_border_top`}
                dataSource={dataSource}
                columns={this.state.columns}
                loading={loading}
                onChange={this.tableChange}
                scroll={{x: 100 * dataSource.length}}
                pagination={{
                    showSizeChanger: true,
                    showQuickJumper: true,
                    current: this.state.page_no,
                    pageSizeOptions: ['10', '20', '50', '100'],
                    pageSize: this.state.page_size,
                    total: this.state.total,
                    className: 'add_modal_pagination' }} />
            </div> )
    }
}
