import React, { Component } from 'react'
import NoneTitle from '../../common_title/none_border_header'
import { Table, message } from 'antd'
import { _getRedemptionList } from '../../../apis/details/index'
import url from 'url'
import moment from 'moment'
const { query } = url.parse(window.location.href, true)
export default class ListTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: [],
            baseParams: {
                page_no: 1,
                sumCount: 0,
                page_size: 10,
                fund_id: query.id,
                token: '123456'
            },
            loading: true,
            takeDate: null,
            total: 0,
            columns: [
                {
                    title: '产品名称',
                    key: '1',
                    dataIndex: 'fundName',
                    render: text => text ? text : '--'
                },
                {
                    title: '认购日期',
                    key: '2',
                    dataIndex: 'tDate',
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    title: '确认时间',
                    key: '3',
                    dataIndex: 'confirmedDate',
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    title: '申赎类型',
                    key: '4',
                    dataIndex: 'changeType',
                    render: text => text ? text : '--'
                },
                {
                    title: '申赎信息',
                    key: '5',
                    dataIndex: 'changeInfo',
                    render: text => text ? text : '--'
                },
                {
                    title: '单位净值',
                    key: '6',
                    dataIndex: 'nav',
                    render: text => text ? parseFloat(text).toFixed(4) : '--'
                },
                {
                    title: '申赎金额',
                    key: '7',
                    dataIndex: 'changeValue'
                },
                {
                    title: '申赎份额',
                    key: '8',
                    dataIndex: 'sumNum'
                },
                {
                    title: '手续费',
                    key: '9',
                    dataIndex: 'feeAmt'
                },
                {
                    title: '客户名称',
                    key: '10',
                    dataIndex: 'customerName',
                    render: text => text ? text : '--'
                }
            ]
        }
    }

    async componentDidMount() {
        await this._getData({ fund_id: query.id, page_no: 1, page_size: 10 })
    }

    async _getData(params) {
        let param = { token: '123456', ...params}
        const data = await _getRedemptionList(param)
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        let result = data.result

        this.setState({loading: false})
        if (data.status) {
            result.publishList.forEach((item, index) => item.key = index)
            this.setState({
                dataSource: result.publishList,
                sumCount: result.sumCount,
                currentPage: result.current,
                takeDate: result.getDate
           })
        } else {
            message.info(data.desc)
            return
        }

    }

    tableChange = (pagination) => {
        const params = { ...this.state.baseParams,  page_no: pagination.current, page_size: pagination.pageSize }

        this.setState({baseParams: params, loading: true}, () => {
            this._getData(this.state.baseParams)
        })

    }

    render() {
        const { columns, dataSource, baseParams, loading } = this.state
        const { page_no, page_size } = baseParams
        return (
            <div className={`mt_20`}>
            <NoneTitle title={`申赎记录`} iconStatus={true} />
            <div className={`position_relative mt_30`}>
                <div className={`position_absolute table_total_position`}>
                    共 <span className={`color_number_ff37`}>{this.state.sumCount}</span> 条数据 <span className={`pl_10`}>更新日期：{moment(this.state.takeDate).format('YYYY-MM-DD')}</span>
                </div>
                <Table
                    className={`table_border_top`}
                    dataSource={dataSource}
                    columns={columns}
                    loading={loading}
                    onChange={this.tableChange}
                    pagination={{ showSizeChanger: true,
                        showQuickJumper: true,
                        current: page_no,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        pageSize: page_size,
                        total: this.state.sumCount,
                        className: 'add_modal_pagination'}} />
            </div>
        </div>)
    }
}
