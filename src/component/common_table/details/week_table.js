import React, { Component } from 'react'
import { Table, message } from 'antd'
import moment from 'moment'
import iscroll from 'iscroll'

export default class WeekTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            dataSource: [],
            columns: [
                {
                    dataIndex: 'date',
                    title: '日期',
                    key: '1',
                    width: 130,
                    render: text => text ? moment(text).format('YYYY-MM-DD') : '--'
                },
                {
                    dataIndex: 'nav',
                    title: '单位净值',
                    key: '2'
                },
                {
                    dataIndex: 'return',
                    title: '收益率',
                    key: '3'
                },
                {
                    dataIndex: 'return_a',
                    title: '累计年化收益率',
                    key: '4'
                },
                {
                    dataIndex: 'drawdown',
                    title: '动态回撤',
                    key: '5'
                },
            ]
        }
    }

    render () {
        return <Table
         className={`border_middle_table table_none_pagination`}
         id={`productWeekTable`}
         loading={this.state.loading}
         columns={this.state.columns}
         dataSource={this.state.dataSource}
         onChange={this._tableChange.bind(this)}
         pagination={this.state.dataSource.length > 10 ? {
             pageSize: this.state.dataSource.length
         } : false} scroll={{y: 400}} />
    }

    componentDidMount () {
        const eleTable = document.getElementById('productWeekTable').querySelector('.ant-table-body')
        eleTable.style.overflow = 'hidden'
        document.getElementById('productWeekTable').querySelector('.ant-table-header').style.overflow = 'hidden'
        document.getElementById('productWeekTable').querySelector('.ant-table-header').style.marginBottom = '0'
    }
    _tableChange (e) {
        console.log(e)
    }
    _initData (params) {
        this.setState({loading: true})
        if (this.props._initData) {
            let param = {...this.state.param, ...params}
            this.setState({param}, async () => {
                const data = await this.props._initData(param)
                let dataSource = []

                if (data && data.length > 0) {
                    const list = data
                    list.forEach((item, index) => {
                        dataSource.push({
                            key: index,
                            date: item.date,
                            nav: isNaN(item.nav) ? item.nav : `${parseFloat(item.nav).toFixed(4)}`,
                            return: isNaN(item.return) ? item.return : `${parseFloat(String(item.return * 100)).toFixed(2)}%`,
                            return_a: isNaN(item.return_a) ? item.return_a : `${parseFloat(String(item.return_a * 100)).toFixed(2)}%`,
                            drawdown: isNaN(item.drawdown) ? item.drawdown : `${parseFloat(String(Math.abs(item.drawdown * 100))).toFixed(2)}%`,
                        })
                    })

                    this.setState({dataSource, loading: false}, () => {
                        new iscroll('#productWeekTable .ant-table-body', {
                            scrollbars: 'custom',
                            mouseWheel: true,
                            preventDefault: false,
                            scrollX: false,
                            scrollY: true,
                            interactiveScrollbars: true,
                            shrinkScrollbars: false,
                            bounce: false
                        })
                    })
                } else {
                    this.setState({dataSource, loading: false})
                }
            })
        } else {
            message.info('初始化方法为空')
        }
    }
}
