import React, { Component } from 'react'
import { Table } from 'antd'
import moment from 'moment'

export default class ProfileTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            columns: [
                {
                    title: '序号',
                    dataIndex: 'series',
                    key: '1',
                    width: 80,
                    ellipsis: true
                },
                {
                    title: '产品名称',
                    dataIndex: 'fundName',
                    key: '2',
                    ellipsis: true,
                    width: 140,
                    render: (text, recoder) => {
                        return text ?  <a href={`/details?id=${recoder.fundId}`}>{text ? text : '--'}</a> : '--'
                    }
                },
                {
                    title: '投资顾问',
                    dataIndex: 'fundConsultant',
                    key: '3',
                    ellipsis: true,
                    width: 120,
                    render: (text, recoder) => {
                        return text ? <a href={`${process.env.REACT_APP_LOGIN}advisor?id=${recoder.orgId}`}>{text ? text : '--'}</a> : '--'
                    }
                },
                {
                    title: '投资经理',
                    dataIndex: 'fundManager',
                    key: '4',
                    ellipsis: true,
                    render : (text, recoder) => {
                        return text ? <a href={`${process.env.REACT_APP_LOGIN}manager?id=${recoder.fundManagerId}`}>{text ? text : '--'}</a> : '--'
                    }
                },
                {
                    title: '投资策略',
                    dataIndex: 'strategyType',
                    width: 120,
                    ellipsis: true,
                    key: '5',
                    render: text => text ? text : '--'
                },
                {
                    title: '成立日期',
                    dataIndex: 'startDate',
                    key: '6',
                    width: 110,
                    render: (text, record) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    title: '净值日期',
                    dataIndex: 'newNavDate',
                    key: '7',
                    width: 110,
                    render: (text, record) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    title: '单位净值',
                    dataIndex: 'fundAddedNav',
                    key: '8',
                    render: (text, record) => {
                        return text ? parseFloat(text).toFixed(4) : '--'
                    }
                },
                {
                    title: '产品类型',
                    dataIndex: 'assetType',
                    key: '9',
                    ellipsis: true,
                    render: text => text ? text : '--'
                },
                {
                    title: '星级评价',
                    dataIndex: 'fundStar',
                    key: '10',
                    render: (text, record) => {
                        return text ? `${parseFloat(text * 100).toFixed(2)}%` : '--'
                    }
                },
                {
                    title: '收益率（2019）',
                    dataIndex: 'nReturn',
                    key: '11',
                    render: (text, record) => {
                        return text ? `${parseFloat(text * 100).toFixed(2)}%` : '--'
                    }
                },
                {
                    title: '同策略排名（2019）',
                    dataIndex: 'cRank',
                    key: '12',
                    render: (text) => {
                        return text ? `${text}` : '--'
                    }
                },
                {
                    title: '最大回撤（2019）',
                    dataIndex: 'maxDrawdown',
                    key: '13',
                    render: (text, record) => {
                        return text ? `${parseFloat(text * 100).toFixed(2)}%` : '--'
                    }
                }
            ],
            dataSource: [],
            loading: false,
            pageNo: 1,
            pageSize: 10,
            pageCount: 0,
            total: 0
        }
    }

    render () {
        return (<div className={`mt_10 position_relative`}>
            <div className={`position_absolute`}>
                共<span className={`color_number_red`}>{this.state.total}</span>条数据 更新日期： 2020-04-02
            </div>
            <Table
                loading={this.state.loading}
                columns={this.state.columns}
                dataSource={this.state.dataSource}
                onChange={this.tableChange.bind(this)}
                pagination={this.state.pageCount > 0 ? {
                    showSizeChanger: true,
                    showQuickJumper: true,
                    current: this.state.pageNo,
                    pageSizeOptions: ['10', '20', '50', '100'],
                    pageSize: this.state.pageSize,
                    total: this.state.total
                    } : false}
            />
        </div>)
    }

    async componentDidMount () {
        this._initData({token: '8ed5d194-dce8-41c5-84c0-37e75b825c4d', page_no: this.state.pageNo, page_size:  this.state.pageSize})
    }

    // 表格改变
    tableChange (e) {
        const { current, pageSize } = e
        this.setState({pageSize: pageSize, pageNo: current}, async () => {
            const param = { page_size: this.state.pageSize, page_no: this.state.pageNo, }
            await this._initData(param)
        })
    }

    async _initData (params) {
        this.setState({loading: true}, async () => {
            if (this.props._initDate) {
                params = { page_no: this.state.pageNo, page_size: this.state.pageSize, ...params}
                const data = await this.props._initDate(params)
                const { page_size, page_no } = params
                if (data) {
                    const list = []
                    data.targetList.forEach((item, index) => {
                        item.key = index
                        item.series = index + ((page_no -1) * page_size) + 1
                        list.push(item)
                    })
                    console.log()
                    this.setState({dataSource: data.targetList, pageCount: data.pageCount, total: data.sumCount, loading: false})
                }
            }
        })
    }

}
