import React, { Component } from 'react'
import { Table } from 'antd'
import moment from 'moment'

export default class ProfileTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            getParentDate: null,
            columns: [
                {
                    title: '序号',
                    dataIndex: 'series',
                    key: '1',
                    width: 80,
                    ellipsis: true
                },
                {
                    title: '产品名称',
                    dataIndex: 'fundName',
                    key: '2',
                    ellipsis: true,
                    width: 164,
                    render: (text, recoder) => {
                        return text ?  <a href={`/details?id=${recoder.fundId}`} data-producttype={Number(recoder[`isParent`]) === 1 ? '母基金' : '子基金'} className={`position_relative ${Number(recoder[`isParent`]) === 1 ? 'product_tag_type' : 'product_child_type'}`} target={`_blank`}><span className={`tag_content`}>{text ? text : '--'}</span></a> : '--'
                    }
                },
                {
                    title: '投资顾问',
                    dataIndex: 'fundConsultant',
                    key: '3',
                    ellipsis: true,
                    width: 120,
                    render: (text, recoder) => {
                        return text ? <a href={`${process.env.REACT_APP_LOGIN}advisor?id=${recoder.orgId}&name=${text}`} target={`_blank`}>{text ? text : '--'}</a> : '--'
                    }
                },
                {
                    title: '投资经理',
                    dataIndex: 'fundManager',
                    key: '4',
                    ellipsis: true,
                    render : (text, recoder) => {
                        const managerArr = text ? text.split(',') : []
                        return text ? managerArr.map((item, index) => <a key={index} href={`${process.env.REACT_APP_LOGIN}manager?id=${recoder.fundManagerId.split(',')[index]}`} target={`_blank`}>{item ? item : '--'}{index < managerArr.length - 1 ? ',' : '' }</a>) : '--'
                    }
                },
                {
                    title: '投资策略',
                    dataIndex: 'strategyType',
                    width: 120,
                    ellipsis: true,
                    key: '5',
                    render: text => text ? text : '--'
                },
                {
                    title: '成立日期',
                    dataIndex: 'fundFoundationDate',
                    key: '6',
                    width: 110,
                    render: (text, record) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    title: '净值日期',
                    dataIndex: 'newNavDate',
                    key: '7',
                    width: 110,
                    render: (text, record) => {
                        return text ? moment(text).format('YYYY-MM-DD') : '--'
                    }
                },
                {
                    title: '单位净值',
                    dataIndex: 'fundNav',
                    key: '8',
                    render: (text, record) => {
                        return text ? parseFloat(text).toFixed(4) : '--'
                    }
                },
                {
                    title: '产品类型',
                    dataIndex: 'product_type',
                    key: '9',
                    ellipsis: true,
                    render: text => text ? text : '--'
                },
                {
                    title: '星级评价',
                    dataIndex: 'fundStar',
                    key: '10',
                    render: (text, record) => {
                        return text ? <span>
                        <em className={`iconfont iconxingji ${text > 0 ? 'icon_color_star' : 'icon_star_gray'}`}></em>
                        <em className={`iconfont iconxingji ${text > 1 ? 'icon_color_star' : 'icon_star_gray'}`}></em>
                        <em className={`iconfont iconxingji ${text > 2 ? 'icon_color_star' : 'icon_star_gray'}`}></em>
                        <em className={`iconfont iconxingji ${text > 3 ? 'icon_color_star' : 'icon_star_gray'}`}></em>
                        <em className={`iconfont iconxingji ${text > 4 ? 'icon_color_star' : 'icon_star_gray'}`}></em>
                        </span> : '--'
                    }
                },
                {
                    title: () =>  `收益率（${moment(this.props._stateDate ? this.props._stateDate : new Date()).format('YYYY')}）`,
                    dataIndex: 'nReturn',
                    key: '11',
                    render: (text, record) => {
                        return text ? <span className={`${text > 0 ? 'color_number_red' : 'color_number_green'}`}>{parseFloat(text * 100).toFixed(2)}%</span> : '--'
                    }
                },
                {
                    title: () => `同策略排名（${moment(this.props._stateDate ? this.props._stateDate : new Date()).format('YYYY')}）`,
                    dataIndex: 'cRank',
                    key: '12',
                    render: (text) => {
                        return text ? `${text}` : '--'
                    }
                },
                {
                    title: `最大回撤（${moment(this.props._stateDate ? this.props._stateDate : new Date()).format('YYYY')}）`,
                    dataIndex: 'maxDrawdown',
                    key: '13',
                    render: (text, record) => {
                        return text ? `${parseFloat(String(Math.abs(text * 100))).toFixed(2)}%` : '--'
                    }
                }
            ],
            dataSource: [],
            loading: false,
            pageNo: 1,
            pageSize: 10,
            pageCount: 0,
            getDate: null,
            total: 0,
            baseParams: {}
        }
    }

    render () {
        return (<div className={`mt_15 position_relative`}>
            <div className={`position_absolute table_total_position`}>
                共<span className={`color_number_ff37`}>{this.state.total}</span>条数据 <span className={`pl_10`}>更新日期：{this.state.getDate}</span>
            </div>
            <Table
                className={`table_border_top`}
                loading={this.state.loading}
                columns={this.state.columns}
                dataSource={this.state.dataSource}
                onChange={this.tableChange.bind(this)}
                pagination={this.state.pageCount > 0 ? {
                    showSizeChanger: true,
                    showQuickJumper: true,
                    current: this.state.pageNo,
                    pageSizeOptions: ['10', '20', '50', '100'],
                    pageSize: this.state.pageSize,
                    total: this.state.total
                    } : false}
            />
        </div>)
    }

    async componentDidMount () {
        this._initData({token: '8ed5d194-dce8-41c5-84c0-37e75b825c4d', page_no: this.state.pageNo, page_size:  this.state.pageSize})
    }

    // 表格改变
    tableChange (e) {
        const { current, pageSize } = e
        this.setState({pageSize: pageSize, pageNo: current}, async () => {
            const param = { page_size: this.state.pageSize, page_no: this.state.pageNo, }
            await this._initData(param)
        })
    }

    async _initData (params) {
        params = {...this.state.baseParams, ...params}
        this.setState({loading: true, baseParams: params}, async () => {
            if (this.props._initDate) {
                params = { page_no: this.state.pageNo, page_size: this.state.pageSize, ...params}
                const data = await this.props._initDate(params)
                const { page_size, page_no } = params
                if (data && data.targetList) {
                    const list = []
                    data.targetList.forEach((item, index) => {
                        item.key = index
                        item.series = index + ((page_no -1) * page_size) + 1
                        list.push(item)
                    })

                    this.setState({getDate: data.getDate, dataSource: data.targetList, pageCount: data.pageCount, total: data.sumCount, loading: false})
                } else {
                    this.setState({dataSource: [], loading: false})
                }
            }
        })
    }

}
