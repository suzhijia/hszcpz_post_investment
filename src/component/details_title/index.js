import React, { Component } from 'react'
import titleStyle from './details_title.module.sass'
import { _getInfo } from '../../apis/details/index'
import url from 'url'
import moment from 'moment'
export default class DetailsTitle extends Component {
    constructor (props) {
        super(props)
        this.state = {
            productInfo: {},
            navInfo: {}
        }
    }

    render () {
        return (
        <div className={`${titleStyle.product_info_wrapper}`}>
            <div className={`${titleStyle.info_top} clear-fix`}>
                <h2 className={titleStyle.product_name} data-status={`[${this.state.productInfo ? Number(this.state.productInfo[`product_tag`]) === 1 ? '母基金' : '子基金' : '--'}]`}>{this.state.productInfo[`product_name`] || '--'}</h2>
            </div>
            <div className={`${titleStyle.bottom_content}`}>

                <dl className={titleStyle.bottom_middle_items}>
                    <dt>净值日期：<span>{this.state.productInfo['newNavDate'] ? moment(this.state.productInfo['newNavDate']).format('YYYY-MM-DD') : '--'}</span></dt>
                    <dd>
                        <div>
                            <span>单位净值</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['fundNav'] ? '--' : parseFloat(this.state.productInfo['fundNav']).toFixed(4) : '--'}</strong>
                        </div>
                        <div>
                            <span>累计净值</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['fundAddedNav'] ? '--' : parseFloat(this.state.productInfo['fundAddedNav']).toFixed(4) : '--'}</strong>
                        </div>
                        <div>
                            <span>复权累计净值</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['fundSwanav'] ? '--' : parseFloat(this.state.productInfo['fundSwanav']).toFixed(4) : '--'}</strong>
                        </div>
                    </dd>
                </dl>

                <dl className={titleStyle.bottom_middle_items}>
                    <dt>（成立以来）指标日期：<span>{this.state.productInfo['tDate'] ? moment(this.state.productInfo['tDate']).format('YYYY-MM-DD') : '--'}</span></dt>
                    <dd>
                        <div>
                            <span>收益率</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['nReturn'] ? '--': parseFloat(this.state.productInfo['nReturn']*100).toFixed(2) : '--'}<em>%</em></strong>
                        </div>
                        <div>
                            <span>最大回撤</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['maxDrawdown'] ? '--' : parseFloat(this.state.productInfo['maxDrawdown']*100).toFixed(2) : '--'}<em>%</em></strong>
                        </div>
                        <div>
                            <span>夏普比</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? !this.state.productInfo['sharpeA'] ? '--' : parseFloat(this.state.productInfo['sharpeA']).toFixed(2) : '--'}</strong>
                        </div>
                    </dd>
                </dl>

                <dl className={titleStyle.bottom_middle_items}>
                    <dt>产品星级：
                        {
                            this.state.productInfo['start'] ?
                            <span>
                                <em className={`iconfont iconxingji ${this.state.productInfo && this.state.productInfo[`star`] > 0 ? 'icon_color_star' : 'star_color_gray'}`}></em>
                                <em className={`iconfont iconxingji ${this.state.productInfo && this.state.productInfo[`star`] > 1 ? 'icon_color_star' : 'star_color_gray'}`}></em>
                                <em className={`iconfont iconxingji ${this.state.productInfo && this.state.productInfo[`star`] > 2 ? 'icon_color_star' : 'star_color_gray'}`}></em>
                                <em className={`iconfont iconxingji ${this.state.productInfo && this.state.productInfo[`star`] > 3 ? 'icon_color_star' : 'star_color_gray'}`}></em>
                                <em className={`iconfont iconxingji ${this.state.productInfo && this.state.productInfo[`star`] > 4 ? 'icon_color_star' : 'star_color_gray'}`}></em>
                            </span>
                            : <span>--</span>}
                        </dt>
                    <dd>
                        <div>
                            <span>实收资本</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? this.state.productInfo[`capital`] ? this.state.productInfo[`capital`] : '--'  : '--'}<em>万元</em></strong>

                        </div>
                        <div>
                            <span>最新市值</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? this.state.productInfo[`marketValue`] ? this.state.productInfo[`marketValue`] : '--'  : '--'}<em>万元</em></strong>

                        </div>
                        <div>
                            <span>总收益</span>
                            <strong className={`color_number_red`}>{this.state.productInfo ? this.state.productInfo[`revenue`] ? this.state.productInfo[`revenue`]  : '--' : '--'}<em>万元</em></strong>

                        </div>
                    </dd>
                </dl>

            </div>
        </div>
        )
    }

    async componentDidMount () {
        // await this._initTitle({})
    }

    async _initTitle (params) {
        const { query } = url.parse(window.location.href, true)
        const param = {token: '123456', fund_id: query.id, ...params}
        const data = await _getInfo(param)
        if (data) {
            this.setState({productInfo: data})
        }
    }
}
