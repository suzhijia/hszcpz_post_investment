import React, { Component } from 'react'
import { Checkbox } from 'antd'

export default class HeaderTitle extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            isChecked: true,
            checkStatus: this.props.checkStatus || false,
            isBorder: this.props.isBorder || false
        }
    }
    
    changeCheckStatus = (e) => {
        this.setState({
            isChecked: e.target.checked
        })
        if (this.props.checkStatus) {
            this.props.changeStatus(e.target.checked)
        }
    }

    exportFiles = (title) => {
        console.log('title', title)
        this.props.exportFiles(title)
    }
    render () {
        const { title, iconStatus } = this.props
        const { isChecked, checkStatus, isBorder} = this.state;
        const showIcon = iconStatus ? true : false

        return <div className='clear-fix' style={{paddingBottom: 10, borderBottom: !isBorder ? '2px solid #dcdcdc' : 'none'}}>
        <div className='float_left'>
            <strong className={`headers_title common_item_title common_title_manger`}>{title}</strong>
                {checkStatus && <Checkbox checked={isChecked} onChange={this.changeCheckStatus}>起点对齐</Checkbox>}
        </div>

        <div className='float_right'>
            {showIcon ? '' :<span className='iconfont echarts_exports iconCombinedShape' onClick={this.exportFiles.bind(this, title)}></span>}
        </div>
        {this.props.children}
    </div>
    }
}