import React, { Component } from 'react'

export default class NoneTitle extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }
    
    render () {
        const { title, iconStatus } = this.props

        const showIcon = iconStatus ? true : false

        return <div className='clear-fix common_header_none_border'>
        <div className='float_left'>
            <strong className={`headers_title common_item_title common_title_manger`}>{title}</strong>
        </div>

        <div className='float_right'>
            {showIcon ? '' :<span className='iconfont echarts_exports iconCombinedShape'></span>}
        </div>
        {this.props.children}
    </div>
    }
}