import React, { Component } from 'react'
import {Button, Layout} from 'antd'
import DevStyle from './dev.module.sass'
import PersonImg from '../../static/img/person.png'

const { Content } = Layout
export default class Developing extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    render () {
        return (
            <div className={`mt_20 manager_wrapper`}>
                <Layout>
                    <Content className={`${DevStyle.dev_container}`}>
                        <div className={`${DevStyle.flex_content}`}>
                            <div className={`${DevStyle.flex_content_left}`}>
                                <img src={PersonImg} alt={`person`} />
                            </div>
                            <div className={`${DevStyle.flex_content_right}`}>
                                <div className={`${DevStyle.flex_img}`}>
                                    <h2 className={`${DevStyle.dev_tips}`}>系统正在开发中。。。</h2>
                                </div>
                                <p>系统该模块，程序员正在开发中。。。</p>
                                <Button className={`${DevStyle.back_button}`} onClick={this.goHome.bind(this)}>返回首页</Button>
                            </div>
                        </div>
                    </Content>
                </Layout>
            </div>)
    }
    async componentDidMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'manager_wrapper')
    }
    goHome () {
        window.location.href = '/home'
    }
}
