import React, { Component } from 'react'
import DefaultLayout from '../../layout/layout'
import { Tabs } from 'antd'
import DetailsTitle from '../../component/details_title'
import ProductAnalysis from '../../component/detail/product_analysis'
import SubProduct from '../../component/detail/sub_product'
import ProductProfile from '../../component/detail/product_profile'
import Performance from '../../component/detail/performance'
import PositionsInfo from '../../component/detail/positions_info'
import ScencesAnalysis from '../../component/detail/scences_analysis'
import Developing from "../../component/developing";

export default class ProductDetail extends Component {
    constructor (props) {
        super(props)
        this.state = {
            tagType: 1
        }
    }

    render () {
        return (<DefaultLayout>
            <DetailsTitle ref={`titleContent`} />

            <Tabs defaultActiveKey="1" className={`tabs_items`}>
            <Tabs.TabPane className={`tabs_item`} tab="产品概况" key="1">
                <ProductProfile />
            </Tabs.TabPane>
                {this.state.tagType === 1 ?
                    <Tabs.TabPane className={`tabs_item`} tab="产品分析" key="2" style={{display: this.state.tagType === 1 ? 'block' : 'none'}}>
                        <ProductAnalysis />
                    </Tabs.TabPane>
                : ''}

                {this.state.tagType === 1 ?
                    <Tabs.TabPane className={`tabs_item`} tab="子产品信息" key="3" style={{display: this.state.tagType === 1 ? 'block' : 'none'}}>
                        <SubProduct />
                    </Tabs.TabPane>
                : ''}

            <Tabs.TabPane className={`tabs_item`} tab="业绩表现" key="4">
                <Performance />
            </Tabs.TabPane>
            <Tabs.TabPane className={`tabs_item`} tab="持仓信息" key="5">
                <PositionsInfo />
            </Tabs.TabPane>
            <Tabs.TabPane className={`tabs_item`} tab="归因分析" key="6">
                <Developing />
            </Tabs.TabPane>
            <Tabs.TabPane className={`tabs_item`} tab="场景分析" key="7">
                <ScencesAnalysis />
            </Tabs.TabPane>
        </Tabs>
        </DefaultLayout>)
    }

    async componentDidMount() {
        await this.refs.titleContent._initTitle({})
        if (this.refs.titleContent.state.productInfo) {
            this.setState({tagType: this.refs.titleContent.state.productInfo.product_tag})
        }
    }
}
