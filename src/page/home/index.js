import React, { Component } from 'react'
import DefaultLayout from '../../layout/layout'
import { Tabs } from 'antd'
import Profile from '../../component/home/product_profile'
import SelfAnalysis from '../../component/home/self_analysis'
import ReportManage from '../../component/home/report_manage'
import Developing from "../../component/developing";

export default class Home extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: []
        }
    }
    async componentDidMount() {

    }

    render () {
        return (
            <DefaultLayout>
                <Tabs defaultActiveKey="2" className={`tabs_items`}>
                    <Tabs.TabPane className={`tabs_item`} tab="产品总览" key="1">
                        <Profile />
                    </Tabs.TabPane>
                    <Tabs.TabPane className={`tabs_item`} tab="自营分析" key="2">
                        <SelfAnalysis />
                    </Tabs.TabPane>
                    <Tabs.TabPane className={`tabs_item`} tab="风控管理" key="3">
                        <Developing />
                    </Tabs.TabPane>
                    <Tabs.TabPane className={`tabs_item`} tab="报告管理" key="4">
                        <ReportManage />
                    </Tabs.TabPane>
                </Tabs>
            </DefaultLayout>
        )
    }
}
