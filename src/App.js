import React, { Component } from 'react'
import { BrowserRouter as Routers,  Route, Switch} from 'react-router-dom'

import routes from './router/route.js'

class App extends Component {
  render() {
    return (
      <Routers>
        <Switch >
          {
            routes.map(({ name, path, exact = true, component }) => (
              <Route path = {path} exact={exact} component={component} key={name} />
            ))
          }
        </Switch> 
      </Routers>
    );
  }
}

export default App;
