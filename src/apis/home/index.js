import axios from '../../Utils/axios'
import { message } from 'antd'

const _getProfileInit = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proInfo/productInit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

const _getProfileTable = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proInfo/productList',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析- 策略收益表
const _getPloTable = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/strategyRevenue',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—规模收益总览-自营资产总规模 按资金结构划分
const _getCapitalScale = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/capitalScale',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—规模收益总览-自营资产总规模 按大类策略划分
const _getCategoryScale = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/categoryScale',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status &&  data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—自营资产总收益 按资金结构划分
const _getCapitalProfit = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/capitalProfit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data&&data.result) {
        if (data.status) {
            return data
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—自营资产总收益 按大类资产划分
const _getCategoryProfit = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/categoryProfit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—后台获取默认时间
const _getInitDate = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/initDate',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—规模分布变化
const _getScaleDistribution = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/scaleDistribution',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—总收益率变化
const _getAllprofitChange = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/allprofitChange',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—投出产品数量变化
const _getProductCount = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/productCount',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—投出策略数量变化
const _getStrategyCount = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/strategyCount',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—规模收益表-规模收益表
const _getScaleList = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/scaleSummary',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data.result
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—规模收益表-收益汇总表
const _getScaleRateList = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/profitSummary',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览
const _getAllPloy = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/strategyOverview',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-自营策略收益分布
const _getAllPloyRate = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/earingSelfStrategyProfit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-自营策略收益分布(按月)
const _getAllPloyRateMonth = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/earingSelfStrategyProfitMonth',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-自营策略资金分布
const _getAllPloyCapital = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/earingSelfStrategyCapital',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-自营策略资金分布(按月)
const _getAllPloyCapitalMonth = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/earingSelfStrategyCapitalMonth',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-自营策略资金分布(按月)
const _getAllPloyMonitor = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsth/earning/orgMonitor',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 自营分析—策略情况总览-投出策略穿透清单
const _getAllPloyThrowStrategy = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/earning/throwStrategyPenetration',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status && data.result) {
            return data
        } else {
            message.error(data.desc)
            return
        }
    } else {
        return { message: 'error', status: false}
    }
}

export {
    _getProfileInit,
    _getProfileTable,
    _getPloTable,
    _getCapitalScale,
    _getCategoryScale,
    _getCapitalProfit,
    _getCategoryProfit,
    _getInitDate,
    _getScaleDistribution,
    _getAllprofitChange,
    _getProductCount,
    _getStrategyCount,
    _getScaleList,
    _getScaleRateList,
    _getAllPloy,
    _getAllPloyRate,
    _getAllPloyThrowStrategy,
    _getAllPloyRateMonth,
    _getAllPloyCapital,
    _getAllPloyCapitalMonth,
    _getAllPloyMonitor
}
