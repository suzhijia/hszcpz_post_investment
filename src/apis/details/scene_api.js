import axios from '../../Utils/axios'
import { message } from 'antd'
// 市场中性，雷达图
const _getRadar = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/getKeyMarketVariable',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，股票多头
const _getSocketMarket = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/getIssueRightTotalNav',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，市场分阶段表现，图和表
const _sceneMarketNeutral = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/getMarketPerformanceNeutral',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，债券模块-市场分阶段表现
const _getBondsMarket = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/getMarketPerformanceBond',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，期货模块-市场分阶段表现
const _getFutureMarket = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/getMarketPerformanceFuture',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，特殊场景，收益走势图
const _getSpecialLine = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundReturnDrawdown',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 场景分析，特殊场景，风险收益列表
const _getSpecialRisk = async params => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundIndexPerformance',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

const _getSceneList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/findSceneByDateRange',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false }
    }
}


export {
    _getRadar,
    _getSocketMarket,
    _getBondsMarket,
    _getFutureMarket,
    _sceneMarketNeutral,
    _getSpecialLine,
    _getSpecialRisk,
    _getSceneList
}
