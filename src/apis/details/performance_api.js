import axios from '../../Utils/axios'
import { message } from 'antd'

// 业绩表现——添加对比产品-列表初始化
const _getAddCompanyInitTableList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/index/selectInit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data) {
      return data
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现-添加对比产品- 内容查询
const _getAddCompanyInput = async (params) => {
  let data = null
  if (params) {
      data = await axios({
          url: '/hsApi//hszcpzindex/selectByLike',
          method: 'POST',
          data: JSON.stringify({...params})
      })
  } else {
      message.error("params is null")
  }
  if (data) {
    return data
  } else {
      return { message: 'error', status: false}
  }
}

// 业绩表现——添加对比产品-列表
const _getAddCompanyTable = async (params) => {
  let data = null
  if (params) {
      data = await axios({
          url: '/hsApi/hszcpz/index/selectByScreen',
          method: 'POST',
          data: JSON.stringify({...params})
      })
  } else {
      message.error("params is null")
  }
  if (data) {
    return data
  } else {
      return { message: 'error', status: false}
  }
}

// 业绩表现-收益,回撤走势图
const _getReturnOrg = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundReturnDrawdown',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
  }

  // 业绩表现-收益-风险指标
const _getCompareRateRisk = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundIndexPerformance',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
  }

// 业绩表现 - 业绩基准
  const _cascaderTags = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/findAllProduct',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.product
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现-月度收益率
  const _historyRate = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/monthReturn',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.product
    } else {
        return { message: 'error', status: false}
    }
  }

 // 业绩表现-月度收益率
 const _getWeekRate = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundWeekReturnDistribution',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
}

 // 业绩表现-产品相关性柱状图
 const _getProCorrelation = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/productCorrelation',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data) {
      return data
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现-产品相关性select查询
const _getCompareList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/findCompareRules',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现 - 产品线相关性 - table
const _getCorrelationTable = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/fundCorrelation',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现 - 历史投资能力
const _getHistoryInvest = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/findHistoryInvestAbility',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.result) {
      return data.result
    } else {
        return { message: 'error', status: false}
    }
}

// 业绩表现 - 历年排名
const _getRankingData = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/yearRankings',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }
    if (data.yearRankings) {
      return data.yearRankings
    } else {
        return { message: 'error', status: false}
    }
}

export {
  _getAddCompanyInitTableList,
  _getAddCompanyInput,
  _getAddCompanyTable,
  _getReturnOrg,
  _getCompareRateRisk,
  _cascaderTags,
  _historyRate,
  _getWeekRate,
  _getProCorrelation,
  _getCompareList,
  _getCorrelationTable,
  _getHistoryInvest,
  _getRankingData
};
