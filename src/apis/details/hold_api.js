import axios from '../../Utils/axios'
import { message } from 'antd'
// 持仓，股票多头，持仓信息
const _getholderStack = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/inventoryOverview',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}
// 持仓，股票多头，板块配置
const _getConcentration = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/tectonic',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}
// 持仓，股票多头，行业集中度
const _getIndustry = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/industryConcentration',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}
// 持仓，股票多头，行业配置
const _getIndustrySetting = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/industryConf',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}
// 持仓获取固定时间接口
const _getStaticDate = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/statisticalDate',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 持仓信息，期货模块，仓位总览
const _getFutureHold = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/forwardInventoryOverview',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 持仓信息，期货模块，板块配置
const _getFutureIndustry = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/tectonicFutures',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 持仓信息，期货模块，资产配比饼图
const _getFuturePie = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/varietyPie',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 持仓信息，期货模块，资产配比-柱状图
const _getFutureBar = async (params) => {
    let data = null
    message.destroy()
    if (params) {
        data = await axios({
            url: '/hsApi/hszcpz/product/varietyBar',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

export {
    _getholderStack,
    _getConcentration,
    _getIndustry,
    _getIndustrySetting,
    _getStaticDate,
    _getFutureHold,
    _getFutureIndustry,
    _getFuturePie,
    _getFutureBar
}
