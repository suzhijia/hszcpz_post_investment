import axios from '../../Utils/axios'
import { message } from 'antd'

// 产品分析，周收益列表
const _getWeekList = async (params) => {
    let data = null

    if (params) {
        data = await axios({
            url: '/hsth/product/weekProfit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.destroy()
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 产品分析-资产收益列表
const _getAssetsRateList = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/assetsProfit',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.error(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 产品分析，产品指标清单
const _getTargetList = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/fundIndexList',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.error(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 子产品信息，子产品净值模拟
const _getMockNav = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/subPro/subFundNav',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data.status) {
        return data
    } else {
        message.error(data.desc)
        return { message: 'error', status: false}
    }
}

// 产品页面初始化，产品信息
const _getInfo = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proInfo/init',
            method: 'POST',
            data: JSON.stringify({...params})
        })
    } else {
        message.error("params is null")
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.destroy()
            message.info(data.desc)
        }
    } else {
        return { message: 'error', status: false}
    }
}

// 产品概况， 申赎记录
const _getRedemptionList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proOverview/changeRecord',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 产品概况， 产品公告
const _getPublishList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proOverview/announcement',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 产品概况， 分红记录
const _getBonusList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proOverview/bonusRecord',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 子产品信息-单位净值序列
const _getUnitList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url:'/hsth/subPro/navSequence',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false }
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false }
    }
}

// 子产品信息-规模序列
const _getScaleList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url:'/hsth/subPro/scaleSequence',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false }
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false }
    }
}

// 產品頁，產品分析，母基金净值圖
const _getSubProductNav = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/fundNav',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品概况，合同总览
const _getContractList = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/proOverview/contractDesc',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data.result
        } else {
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品分析，资产分配比例
const _getAssetsPercenter = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/assetAllocation',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品分析，子投顾累计收益率
const _getSubChart = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/subOrgProfit',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品分析，分类收益贡献
const _getRateChart = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/profitContribution',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品分析，子基金资产净值分布图
const _getSubReturn = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/subFundValue',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品页，产品分析，子投顾周/月
const _getSubRate = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/product/subOrgReturn',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品详情，子产品信息-过滤条件
const _getSubSearch = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/subPro/loadScreen',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品详情，子产品信息-规模占比走势
const _getSubScale = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/subPro/scaleProportion',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

// 产品详情，子产品信息-净值走势
const _getSubNav = async params => {
    let data = null
    if (params) {
        data = await axios({
            url: '/hsth/subPro/navTrend',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        if (data.status) {
            return data
        } else {
            message.destroy()
            message.info(data.desc)
        }

    } else {
        return { message: 'error', status: false}
    }
}

export {
    _getWeekList,
    _getAssetsRateList,
    _getTargetList,
    _getMockNav,
    _getInfo,
    _getRedemptionList,
    _getPublishList,
    _getBonusList,
    _getUnitList,
    _getSubProductNav,
    _getContractList,
    _getAssetsPercenter,
    _getSubChart,
    _getRateChart,
    _getSubReturn,
    _getSubRate,
    _getSubSearch,
    _getScaleList,
    _getSubScale,
    _getSubNav
}
