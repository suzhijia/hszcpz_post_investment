const proxy = require('http-proxy-middleware')

module.exports = function(app) {
 app.use(proxy('/api',
   {
    //  target: "http://192.168.1.51:8090/netFilter/",
     target: "https://www.fastmock.site/mock/6c4fa966509b8d1703949d30ff19a8c7/postWeb",
     changeOrigin: true,
     "secure": false,
     pathRewrite: {
                 "^/api": "/"
             }
   }),
    proxy('/hsth',
     {
          target: "http://192.168.1.51:9990/hsth",
         changeOrigin: true,
         "secure": false,
         pathRewrite: {
             "^/hsth": "/"
         }
     }),
     proxy('/hsApi',
         {
             target: "http://192.168.1.51:9900",
             changeOrigin: true,
             "secure": false,
             pathRewrite: {
                 "^/hsApi": "/api"
             }
         })
 )}
