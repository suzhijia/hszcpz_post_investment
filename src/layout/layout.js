import React, { Component } from 'react'
import { Layout, Menu, Icon, Dropdown, Avatar, Badge } from 'antd'
import '../static/style/common/common.sass'
import '../static/style/layout/index.sass'
import LayoutStyle from '../static/style/layout/index.module.sass'
import Logo from '../static/img/logo.svg'
import cookie from 'js-cookie'
import url from 'url'

const { Header, Sider, Content } = Layout
const { query } = url.parse(window.location.href, true)
export default class DefaultLayout extends Component {
    constructor (props) {
        super(props)
        this.state = {
            collapsed: true,
            pageName: '资产研究配置',
            navList: [
                {
                    name: '资产策略研究',
                    link: `${process.env.REACT_APP_PUBLIC_IP}:8094`,
                    iconClass: 'iconfont assets iconzichan',
                    subMenu: []
                },
                {
                    name: '投顾产品挖掘',
                    link: `${process.env.REACT_APP_PUBLIC_IP}`,
                    iconClass: 'iconfont advisor iconzichanshaixuan',
                    subMenu: []
                },
                {
                    name: '组合配置策略',
                    link: `${process.env.REACT_APP_PUBLIC_IP}:8183`,
                    iconClass: 'iconfont group iconjuecebiao',
                    subMenu: [

                    ]
                },
                {
                    name: '投后管理监控',
                    link: '/',
                    iconClass: 'iconfont abserve iconjianguan1',
                    subMenu: [

                    ]
                },
                {
                    name: '配置流程管理',
                    link: '/',
                    iconClass: 'iconfont reset iconliuchengguanli',
                    subMenu: [

                    ]
                }
            ],
            defaultKey: '3',
            subKey: 0
        }
    }

    componentDidMount () {
        document.body.setAttribute('id', 'layout_wrapper')
        cookie.set('token', query.token)

    }

    _toggle () {
        // 左边菜单收起展开功能
        // this.setState({ collapsed: !this.state.collapsed })
    }

    _collapse (collapse, type) {
        // console.log(collapse, type)
    }
    _goUser () {
        window.location.href=`/users?token=${query.token || ''}&key=2`
    }
    _goRole() {
        window.location.href=`/roles?token=${query.token || ''}&key=3`
    }

    _goUrl (url, key) {
        console.log(url, key)
        if (url) {
            if (url === '/') {
                window.location.href = `${url}`
            } else {
                window.open(`${url}/home?token=${window.decodeURI(cookie.get('token'))}`, '_blank')
            }
        }
    }

    render () {
        const menuList = this.props.navList || this.state.navList
        const userInfo = JSON.parse(window.localStorage.getItem('userInfo'))
        const menu = (
            <Menu>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="/">
                    消息1
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="/">
                    消息2
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="/">
                    消息3
                </a>
              </Menu.Item>
            </Menu>
          )

          const userMenu = (
            <Menu>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="/">
                    个人中心
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href={`/system-list`}>
                    登录中心
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href={`${process.env.REACT_APP_BACK_URL}?token=${cookie.get('token')}`}>
                    后台管理
                </a>
              </Menu.Item>
              <Menu.Item>
                <span rel="noopener noreferrer" >
                    退出
                </span>
              </Menu.Item>
            </Menu>
          )


        return (
            <Layout>
                <Header className={`${LayoutStyle.project_header}`} onClick={this._toggle.bind(this)}>
                    <div className={`${LayoutStyle.project_logo} float_left`}>
                        <div className={`${LayoutStyle.logo_wrapper} float_left`}><img src={Logo} alt={`logo`} /></div>
                        <div className={`${LayoutStyle.logo_title} float_left`}>
                            <h1>{this.state.pageName}</h1>
                        </div>
                    </div>
                    <Menu
                        theme='customer'
                        onClick={this.handlerClick}
                        mode='horizontal'
                        className={`${LayoutStyle.header_menu_items} layout_header_menu float_left`}
                        selectedKeys={[this.state.defaultKey]}>
                            {
                                menuList.map((item, index) => (
                                    <Menu.Item key={index} className={LayoutStyle.position_relative} onClick={() => {this._goUrl(item.link)}}>
                                        <span className={`${item.iconClass}`}></span>
                                        <span>{item.name}</span>
                                        <div style={{display: item.subMenu.length > 0 ? 'block' : 'none'}} className={`position_sub_menu`}>
                                            {
                                                item.subMenu.map((itm, ix) => (<span key={ix}>{itm.name}</span>))
                                            }
                                        </div>
                                    </Menu.Item>
                                ))
                            }

                        </Menu>
                    <div className={`${LayoutStyle.header_avatar} float_right`}>
                        <Dropdown overlay={menu} placement="bottomCenter">
                            <Badge dot count={this.state.messageCount} className={LayoutStyle.header_message}>
                                <span className={`iconfont iconxiaoxi1 ${LayoutStyle.icon_bell}`}></span>
                            </Badge>
                        </Dropdown>
                        <Dropdown overlay={userMenu} placement="bottomCenter">
                            <Avatar className={LayoutStyle.user_avart} size='large' src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"></Avatar>
                        </Dropdown>
                        <span className={LayoutStyle.header_username}>{userInfo ? userInfo.userName ? userInfo.userName: '游客您好' : '游客您好'}</span>
                        {/* <Icon className={Inner.header_change_menu} type='swap' /> */}
                    </div>
                </Header>
                <Layout>
                <Sider breakpoint='lg' collapsedWidth='0' theme={'customer'} trigger={null} collapsed={this.state.collapsed} onCollapse={this._collapse.bind(this)}>
                    <Menu theme="customer" mode="inline" defaultOpenKeys={['sub1']} defaultSelectedKeys={[`${query.key || '1'}`]}>
                        <Menu.Item key="1">
                        <Icon type="home" />
                        <span className="nav-text">首页</span>
                        </Menu.Item>

                        {/* <SubMenu
                        key="sub1"
                        title={
                            <span>
                            <Icon type="setting" />
                            <span>权限管理</span>
                            </span>
                        }
                        >
                        <Menu.Item key="4" onClick={this._goUrl.bind(this, '/setting-list', '4')}>权限列表</Menu.Item>
                        <Menu.Item key="5" onClick={this._goUrl.bind(this, '/setting-role', '5')}>用户角色分配</Menu.Item>
                        <Menu.Item key="6" onClick={this._goUrl.bind(this, '/setting-permission', '6')}>角色权限设置</Menu.Item>
                        </SubMenu> */}

                    </Menu>
                </Sider>
                <Layout>
                    <Content className={`${LayoutStyle.project_container}`}>{this.props.children}</Content>
                </Layout>
                </Layout>
            </Layout>
        )
    }
}
